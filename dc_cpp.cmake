FILE(GLOB_RECURSE LibFiles "include/*.h")

option(BUILD_64_BIT "Build 64 bit library" ON)
option(USE_LOGGING "Provide fine grained logging output" OFF)

if (BUILD_64_BIT)
    message(STATUS "Building for 64 bit")
    set(BOOST_ADDRESS_MODEL 64)
else ()
    message(STATUS "Building for 32 bit")
    set(BOOST_ADDRESS_MODEL 32)
endif (BUILD_64_BIT)

if(USE_LOGGING)
add_definitions(-DUSE_LOGGING)
endif()

# --------------------------------- Pin related search map ---------------------------------------- #
find_path(PIN_ROOT_DIR
    NAMES source/include/pin/pin.H
    PATHS $ENV{PIN_ROOT_DIR}
    DOC "Pin's base directory"
    )
if (NOT PIN_ROOT_DIR)
    message(FATAL_ERROR
        "\nPin not found!\n"
        "Please set the environment variable PIN_ROOT_DIR to the base directory"
        " of the Pin.\n Please make sure the same pin installation is in USER or SYSTEM PATH as well"
        )
endif (NOT PIN_ROOT_DIR)
message(STATUS "PIN_ROOT_DIR: ${PIN_ROOT_DIR}")

if (BUILD_64_BIT)
    set(PIN_INCLUDE_DIRS
        ${PIN_ROOT_DIR}/extras/xed-intel64/include
        ${PIN_ROOT_DIR}/source/include/pin
        ${PIN_ROOT_DIR}/source/include/pin/gen
        ${PIN_ROOT_DIR}/extras/components/include
        ${PIN_ROOT_DIR}/source/tools/InstLib
        )
    set(PIN_LIBRARY_DIRS
        ${PIN_ROOT_DIR}/extras/components/lib/intel64
        ${PIN_ROOT_DIR}/extras/xed-intel64/lib
        ${PIN_ROOT_DIR}/intel64/lib
        ${PIN_ROOT_DIR}/intel64/lib-ext
        )
    set(BOOST_ADDRESS_MODEL 64)
    if (MSVC)
        set(LD_LIBDWARF_FLAGS "")
        set(ELF_FLAG "")
    else()
        set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -O0 -g")
        set(CMAKE_C_FLAGS_DEBUG "${CMAKE_C_FLAGS_DEBUG} -O0 -g")
        set(CMAKE_CXX_FLAGS_RELWITHDEBINFO "${CMAKE_CXX_FLAGS_RELWITHDEBINFO} -O2 -g -fno-omit-frame-pointer")
        set(CMAKE_C_FLAGS_RELWITHDEBINFO "${CMAKE_C_FLAGS_RELWITHDEBINFO} -O2 -g -fno-omit-frame-pointer")
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O2 -g -fno-omit-frame-pointer")
        set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -O2 -g -fno-omit-frame-pointer")
        set(LD_LIBDWARF_FLAGS "-O2 -L${CMAKE_CURRENT_BINARY_DIR}/libelf/lib/") # -I${CMAKE_CURRENT_BINARY_DIR}/libelf/lib/")
        set(ELF_FLAG "")
    endif()
else ()
    set(PIN_INCLUDE_DIRS
        ${PIN_ROOT_DIR}/extras/xed-ia32/include
        ${PIN_ROOT_DIR}/source/include/pin
        ${PIN_ROOT_DIR}/source/include/pin/gen
        ${PIN_ROOT_DIR}/extras/components/include
        ${PIN_ROOT_DIR}/source/tools/InstLib
        )
    set(PIN_LIBRARY_DIRS
        ${PIN_ROOT_DIR}/extras/components/lib/ia32
        ${PIN_ROOT_DIR}/extras/xed-ia32/lib
        ${PIN_ROOT_DIR}/ia32/lib
        ${PIN_ROOT_DIR}/ia32/lib-ext
        )

    set(BOOST_ADDRESS_MODEL 32)
    if (MSVC)
        set(LD_LIBDWARF_FLAGS "")
        set(ELF_FLAG "")
        SET(CMAKE_EXE_LINKER_FLAGS_DEBUG "${CMAKE_EXE_LINKER_FLAGS_DEBUG} /SAFESEH:NO")
        SET(CMAKE_EXE_LINKER_FLAGS_MINSIZEREL "${CMAKE_EXE_LINKER_FLAGS_MINSIZEREL} /SAFESEH:NO")
        SET(CMAKE_EXE_LINKER_FLAGS_RELEASE "${CMAKE_EXE_LINKER_FLAGS_RELEASE} /SAFESEH:NO")
        SET(CMAKE_EXE_LINKER_FLAGS_RELWITHDEBINFO "${CMAKE_EXE_LINKER_FLAGS_RELWITHDEBINFO} /SAFESEH:NO")
        SET(CMAKE_SHARED_LINKER_FLAGS_DEBUG "${CMAKE_SHARED_LINKER_FLAGS_DEBUG} /SAFESEH:NO")
        SET(CMAKE_SHARED_LINKER_FLAGS_MINSIZEREL "${CMAKE_SHARED_LINKER_FLAGS_MINSIZEREL} /SAFESEH:NO")
        SET(CMAKE_SHARED_LINKER_FLAGS_RELEASE "${CMAKE_SHARED_LINKER_FLAGS_RELEASE} /SAFESEH:NO")
        SET(CMAKE_SHARED_LINKER_FLAGS_RELWITHDEBINFO "${CMAKE_SHARED_LINKER_FLAGS_RELWITHDEBINFO} /SAFESEH:NO")
        SET(CMAKE_MODULE_LINKER_FLAGS_DEBUG "${CMAKE_MODULE_LINKER_FLAGS_DEBUG} /SAFESEH:NO")
        SET(CMAKE_MODULE_LINKER_FLAGS_MINSIZEREL "${CMAKE_MODULE_LINKER_FLAGS_MINSIZEREL} /SAFESEH:NO")
        SET(CMAKE_MODULE_LINKER_FLAGS_RELEASE "${CMAKE_MODULE_LINKER_FLAGS_RELEASE} /SAFESEH:NO")
        SET(CMAKE_MODULE_LINKER_FLAGS_RELWITHDEBINFO "${CMAKE_MODULE_LINKER_FLAGS_RELWITHDEBINFO} /SAFESEH:NO")
     else()
        set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -O0 -g -m32")
        set(CMAKE_C_FLAGS_DEBUG "${CMAKE_C_FLAGS_DEBUG}-O0 -g -m32")
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O2 -m32 -fno-omit-frame-pointer")
        set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -O2 -m32 -fno-omit-frame-pointer")
        set(LD_LIBDWARF_FLAGS "-O2 -m32 -L${CMAKE_CURRENT_BINARY_DIR}/libelf/lib/") # -I${CMAKE_CURRENT_BINARY_DIR}/libelf/lib/")
        set(ELF_FLAG "--disable-elf64")
    endif()
endif (BUILD_64_BIT)

add_subdirectory(${CMAKE_CURRENT_LIST_DIR}/src)
add_subdirectory(${CMAKE_CURRENT_LIST_DIR}/symbols)
