/* Copyright 2014 Siemens Technology and Services*/

#ifdef _WIN32
#include "PinToolInstrumentWin32ThreadLibrary.h"

#include <algorithm>
#include <string>

#include "DebugOptions.h"

namespace pcv {

PinToolInstrumentWin32ThreadLibrary::PinToolInstrumentWin32ThreadLibrary(void)
{
  pinToolAnalyzeWin32ThreadLibraryObject = new PinToolAnalyzeWin32ThreadLibrary();
}

PinToolInstrumentWin32ThreadLibrary::~PinToolInstrumentWin32ThreadLibrary(void) {}

void PinToolInstrumentWin32ThreadLibrary::setQuery(Query *query)
{
  this->pinToolAnalyzeWin32ThreadLibraryObject->setQuery(query);
}

void PinToolInstrumentWin32ThreadLibrary::setThreadDataNodeList(ThreadDataNode **threadDataNodeList)
{
  this->pinToolAnalyzeWin32ThreadLibraryObject->setThreadDataNodeList(threadDataNodeList);
}

void PinToolInstrumentWin32ThreadLibrary::setThreadAnalyzeObject(ThreadAnalyze *threadAnalyzeObject)
{
  this->pinToolAnalyzeWin32ThreadLibraryObject->setThreadAnalyzeObject(threadAnalyzeObject);
}

void PinToolInstrumentWin32ThreadLibrary::setFilterObject(FilterInstrumentation *filterObject)
{
  this->pinToolAnalyzeWin32ThreadLibraryObject->setFilterObject(filterObject);
}

void instrumentWin32ThreadLibrary(IMG img, VOID *v)
{
#if defined NORMALMODE || defined ANALYZETHREADLIBRARY
  if (!IMG_Valid(img)) return;

  PinToolInstrumentWin32ThreadLibrary *threadLibraryInstrumentObject =
    static_cast<PinToolInstrumentWin32ThreadLibrary *>(v);

  PinToolAnalyzeWin32ThreadLibrary *threadLibraryAnalysisObject =
    threadLibraryInstrumentObject->pinToolAnalyzeWin32ThreadLibraryObject;

  std::string imageName = IMG_Name(img);
  int pos               = static_cast<int>(imageName.find_last_of('\\'));
  std::string dllName   = imageName.substr(pos + 1, imageName.length() - (pos + 1));

  std::transform(dllName.begin(), dllName.end(), dllName.begin(), ::tolower);

  if (dllName == "kernelbase.dll") {
    // Instrumenting CreateMutexA routine
    RTN routineOfInterest = RTN_FindByName(img, "CreateMutexA");
    if (RTN_Valid(routineOfInterest)) {
      RTN_Open(routineOfInterest);
      // RTN_InsertCall(routineOfInterest, IPOINT_BEFORE, (AFUNPTR)
      // beforeCreateMutexA,
      //     IARG_THREAD_ID, IARG_RETURN_IP, IARG_END);
      RTN_InsertCall(routineOfInterest,
                     IPOINT_AFTER,
                     (AFUNPTR)afterCreateMutexA,
                     IARG_PTR,
                     threadLibraryAnalysisObject,
                     IARG_THREAD_ID,
                     IARG_RETURN_IP,
                     IARG_FUNCARG_ENTRYPOINT_VALUE,
                     1,
                     IARG_FUNCARG_ENTRYPOINT_VALUE,
                     2,
                     IARG_FUNCRET_EXITPOINT_REFERENCE,
                     IARG_RETURN_IP,
                     IARG_END);
      RTN_Close(routineOfInterest);
#if defined TRACING_LEVEL1 || defined TRACING_LEVEL2
      BOOST_LOG_TRIVIAL(trace) << "[Trace][Lock] Instrumenting CreateMutexA";
#endif
    }

    // Instrumenting CreateMutexW routine
    routineOfInterest = RTN_FindByName(img, "CreateMutexW");
    if (RTN_Valid(routineOfInterest)) {
      RTN_Open(routineOfInterest);
      // RTN_InsertCall(routineOfInterest, IPOINT_BEFORE, (AFUNPTR)
      // BeforeCreateMutexW,
      //     IARG_THREAD_ID, IARG_RETURN_IP, IARG_END);
      RTN_InsertCall(routineOfInterest,
                     IPOINT_AFTER,
                     (AFUNPTR)afterCreateMutexW,
                     IARG_PTR,
                     threadLibraryAnalysisObject,
                     IARG_THREAD_ID,
                     IARG_RETURN_IP,
                     IARG_FUNCARG_ENTRYPOINT_VALUE,
                     1,
                     IARG_FUNCARG_ENTRYPOINT_VALUE,
                     2,
                     IARG_FUNCRET_EXITPOINT_REFERENCE,
                     IARG_RETURN_IP,
                     IARG_END);
      RTN_Close(routineOfInterest);
#if defined TRACING_LEVEL1 || defined TRACING_LEVEL2
      BOOST_LOG_TRIVIAL(trace) << "[Trace][Lock] Instrumenting CreateMutexW";
#endif
    }

    // Instrumenting CreateMutexExA routine
    routineOfInterest = RTN_FindByName(img, "CreateMutexExA");
    if (RTN_Valid(routineOfInterest)) {
      RTN_Open(routineOfInterest);
      // RTN_InsertCall(routineOfInterest, IPOINT_BEFORE,
      //     (AFUNPTR) BeforeCreateMutexExA,
      //     IARG_THREAD_ID, IARG_RETURN_IP, IARG_END);
      RTN_InsertCall(routineOfInterest,
                     IPOINT_AFTER,
                     (AFUNPTR)afterCreateMutexExA,
                     IARG_PTR,
                     threadLibraryAnalysisObject,
                     IARG_THREAD_ID,
                     IARG_RETURN_IP,
                     IARG_FUNCARG_ENTRYPOINT_VALUE,
                     1,
                     IARG_FUNCRET_EXITPOINT_REFERENCE,
                     IARG_END);
      RTN_Close(routineOfInterest);
#if defined TRACING_LEVEL1 || defined TRACING_LEVEL2
      BOOST_LOG_TRIVIAL(trace) << "[Trace][Lock] Instrumenting CreateMutexExA";
#endif
    }

    // Instrumenting CreateMutexExW routine
    routineOfInterest = RTN_FindByName(img, "CreateMutexExW");
    if (RTN_Valid(routineOfInterest)) {
      RTN_Open(routineOfInterest);
      // RTN_InsertCall(routineOfInterest, IPOINT_BEFORE,
      //     (AFUNPTR) BeforeCreateMutexExW,
      //     IARG_THREAD_ID, IARG_RETURN_IP, IARG_END);
      RTN_InsertCall(routineOfInterest,
                     IPOINT_AFTER,
                     (AFUNPTR)afterCreateMutexExW,
                     IARG_PTR,
                     threadLibraryAnalysisObject,
                     IARG_THREAD_ID,
                     IARG_RETURN_IP,
                     IARG_FUNCARG_ENTRYPOINT_VALUE,
                     1,
                     IARG_FUNCRET_EXITPOINT_REFERENCE,
                     IARG_END);
      RTN_Close(routineOfInterest);
#if defined TRACING_LEVEL1 || defined TRACING_LEVEL2
      BOOST_LOG_TRIVIAL(trace) << "[Trace][Lock] Instrumenting CreateMutexExW";
#endif
    }

    // Instrumenting ReleaseMutex routine
    routineOfInterest = RTN_FindByName(img, "ReleaseMutex");
    if (RTN_Valid(routineOfInterest)) {
      RTN_Open(routineOfInterest);
      RTN_InsertCall(routineOfInterest,
                     IPOINT_BEFORE,
                     (AFUNPTR)beforeReleaseMutex,
                     IARG_PTR,
                     threadLibraryAnalysisObject,
                     IARG_THREAD_ID,
                     IARG_FUNCARG_ENTRYPOINT_REFERENCE,
                     0,
                     IARG_RETURN_IP,
                     IARG_END);
      RTN_InsertCall(routineOfInterest,
                     IPOINT_AFTER,
                     (AFUNPTR)afterReleaseMutex,
                     IARG_PTR,
                     threadLibraryAnalysisObject,
                     IARG_THREAD_ID,
                     IARG_FUNCRET_EXITPOINT_VALUE,
                     IARG_RETURN_IP,
                     IARG_END);
      RTN_Close(routineOfInterest);
#if defined TRACING_LEVEL1 || defined TRACING_LEVEL2
      BOOST_LOG_TRIVIAL(trace) << "[Trace][Lock] Instrumenting ReleaseMutexA";
#endif
    }

    // Instrumenting CloseHandle routine
    routineOfInterest = RTN_FindByName(img, "CloseHandle");
    if (RTN_Valid(routineOfInterest)) {
      RTN_Open(routineOfInterest);
      RTN_InsertCall(routineOfInterest,
                     IPOINT_BEFORE,
                     (AFUNPTR)beforeCloseHandle,
                     IARG_PTR,
                     threadLibraryAnalysisObject,
                     IARG_THREAD_ID,
                     IARG_FUNCARG_ENTRYPOINT_REFERENCE,
                     0,
                     IARG_RETURN_IP,
                     IARG_END);
      RTN_InsertCall(routineOfInterest,
                     IPOINT_AFTER,
                     (AFUNPTR)afterCloseHandle,
                     IARG_PTR,
                     threadLibraryAnalysisObject,
                     IARG_THREAD_ID,
                     IARG_FUNCRET_EXITPOINT_VALUE,
                     IARG_RETURN_IP,
                     IARG_END);
      RTN_Close(routineOfInterest);
#if defined TRACING_LEVEL1 || defined TRACING_LEVEL2
      BOOST_LOG_TRIVIAL(trace) << "[Trace][Lock] Instrumenting CloseHandle";
#endif
    }

    // Instrumenting InitializeCriticalSectionAndSpinCount routine
    routineOfInterest = RTN_FindByName(img, "InitializeCriticalSectionAndSpinCount");
    if (RTN_Valid(routineOfInterest)) {
      RTN_Open(routineOfInterest);
      RTN_InsertCall(routineOfInterest,
                     IPOINT_BEFORE,
                     (AFUNPTR)beforeInitializeCriticalSectionAndSpinCount,
                     IARG_PTR,
                     threadLibraryAnalysisObject,
                     IARG_THREAD_ID,
                     IARG_FUNCARG_ENTRYPOINT_REFERENCE,
                     0,
                     IARG_RETURN_IP,
                     IARG_END);
      RTN_InsertCall(routineOfInterest,
                     IPOINT_AFTER,
                     (AFUNPTR)afterInitializeCriticalSectionAndSpinCount,
                     IARG_PTR,
                     threadLibraryAnalysisObject,
                     IARG_THREAD_ID,
                     IARG_RETURN_IP,
                     IARG_END);
      RTN_Close(routineOfInterest);

#if defined TRACING_LEVEL1 || defined TRACING_LEVEL2
      BOOST_LOG_TRIVIAL(trace) << "[Trace][Lock] Instrumenting Initialize Critical Section "
                               << "with Spin Count";
#endif
    }
  } else if (dllName == "kernel32.dll") {
    // Replacing WaitForSingleObject routine
    RTN routineOfInterest = RTN_FindByName(img, "WaitForSingleObject");
    if (RTN_Valid(routineOfInterest)) {
      PROTO protoWaitForSingleObject = PROTO_Allocate(PIN_PARG(WINDOWS::DWORD),
                                                      CALLINGSTD_STDCALL,
                                                      "WaitForSingleObject",
                                                      PIN_PARG(WINDOWS::HANDLE),
                                                      PIN_PARG(WINDOWS::DWORD),
                                                      PIN_PARG_END());
      RTN_ReplaceSignature(routineOfInterest,
                           (AFUNPTR)waitForSingleObjectWrapper,
                           IARG_PROTOTYPE,
                           protoWaitForSingleObject,
                           IARG_PTR,
                           threadLibraryAnalysisObject,
                           IARG_CONST_CONTEXT,
                           IARG_ORIG_FUNCPTR,
                           IARG_THREAD_ID,
                           IARG_FUNCARG_ENTRYPOINT_VALUE,
                           0,
                           IARG_FUNCARG_ENTRYPOINT_VALUE,
                           1,
                           IARG_RETURN_IP,
                           IARG_END);
#if defined TRACING_LEVEL1 || defined TRACING_LEVEL2
      BOOST_LOG_TRIVIAL(trace) << "[Trace][Lock] Instrumenting WaitForSingleObject";
#endif
    }

    // Instrumenting WaitForMultipleObjects routine
    routineOfInterest = RTN_FindByName(img, "WaitForMultipleObjects");
    if (RTN_Valid(routineOfInterest)) {
      RTN_Open(routineOfInterest);
      RTN_InsertCall(routineOfInterest,
                     IPOINT_BEFORE,
                     AFUNPTR(beforeWaitForMultipleObjects),
                     IARG_PTR,
                     threadLibraryAnalysisObject,
                     IARG_THREAD_ID,
                     IARG_FUNCARG_ENTRYPOINT_VALUE,
                     0,
                     IARG_FUNCARG_ENTRYPOINT_REFERENCE,
                     1,
                     IARG_FUNCARG_ENTRYPOINT_VALUE,
                     2,
                     IARG_RETURN_IP,
                     IARG_END);
      RTN_InsertCall(routineOfInterest,
                     IPOINT_AFTER,
                     AFUNPTR(afterWaitForMultipleObjects),
                     IARG_PTR,
                     threadLibraryAnalysisObject,
                     IARG_THREAD_ID,
                     IARG_FUNCRET_EXITPOINT_VALUE,
                     IARG_RETURN_IP,
                     IARG_END);
      RTN_Close(routineOfInterest);
#if defined TRACING_LEVEL1 || defined TRACING_LEVEL2
      BOOST_LOG_TRIVIAL(trace) << "[Trace][Lock] Instrumenting WaitForMultipleObject";
#endif
    }

    // Instrumenting CreateThread routine
    routineOfInterest = RTN_FindByName(img, "CreateThread");
    if (RTN_Valid(routineOfInterest)) {
      RTN_Open(routineOfInterest);
      RTN_InsertCall(routineOfInterest,
                     IPOINT_AFTER,
                     AFUNPTR(afterCreateThread),
                     IARG_PTR,
                     threadLibraryAnalysisObject,
                     IARG_THREAD_ID,
                     IARG_FUNCRET_EXITPOINT_VALUE,
                     IARG_END);
      RTN_Close(routineOfInterest);
#if defined TRACING_LEVEL1 || defined TRACING_LEVEL2
      BOOST_LOG_TRIVIAL(trace) << "[Trace][Thread] Instrumenting CreateThread";
#endif
    }
  } else if (dllName == "ntdll.dll") {
    // Instrumenting RtlInitializeCriticalSection routine
    RTN routineOfInterest = RTN_FindByName(img, "RtlInitializeCriticalSection");
    if (RTN_Valid(routineOfInterest)) {
      RTN_Open(routineOfInterest);
      RTN_InsertCall(routineOfInterest,
                     IPOINT_BEFORE,
                     (AFUNPTR)beforeInitializeCriticalSection,
                     IARG_PTR,
                     threadLibraryAnalysisObject,
                     IARG_THREAD_ID,
                     IARG_FUNCARG_ENTRYPOINT_REFERENCE,
                     0,
                     IARG_RETURN_IP,
                     IARG_END);
      RTN_InsertCall(routineOfInterest,
                     IPOINT_AFTER,
                     (AFUNPTR)afterInitializeCriticalSection,
                     IARG_PTR,
                     threadLibraryAnalysisObject,
                     IARG_THREAD_ID,
                     IARG_RETURN_IP,
                     IARG_END);
      RTN_Close(routineOfInterest);

#if defined TRACING_LEVEL1 || defined TRACING_LEVEL2
      BOOST_LOG_TRIVIAL(trace) << "[Trace][Lock] Instrumenting Initialize Critical Section";
#endif
    }

    // Instrumenting RtlEnterCriticalSection routine
    routineOfInterest = RTN_FindByName(img, "RtlEnterCriticalSection");
    if (RTN_Valid(routineOfInterest)) {
      RTN_Open(routineOfInterest);
      RTN_InsertCall(routineOfInterest,
                     IPOINT_BEFORE,
                     (AFUNPTR)beforeEnterCriticalSection,
                     IARG_PTR,
                     threadLibraryAnalysisObject,
                     IARG_THREAD_ID,
                     IARG_FUNCARG_ENTRYPOINT_REFERENCE,
                     0,
                     IARG_RETURN_IP,
                     IARG_END);
      RTN_InsertCall(routineOfInterest,
                     IPOINT_AFTER,
                     (AFUNPTR)afterEnterCriticalSection,
                     IARG_PTR,
                     threadLibraryAnalysisObject,
                     IARG_THREAD_ID,
                     IARG_RETURN_IP,
                     IARG_END);
      RTN_Close(routineOfInterest);
#if defined TRACING_LEVEL1 || defined TRACING_LEVEL2
      BOOST_LOG_TRIVIAL(trace) << "[Trace][Lock] Instrumenting RTLEnterCriticalSection";
#endif
    }

    // Instrumenting RtlTryEnterCriticalSection routine
    routineOfInterest = RTN_FindByName(img, "RtlTryEnterCriticalSection");
    if (RTN_Valid(routineOfInterest)) {
      RTN_Open(routineOfInterest);
      RTN_InsertCall(routineOfInterest,
                     IPOINT_BEFORE,
                     (AFUNPTR)beforeTryEnterCriticalSection,
                     IARG_PTR,
                     threadLibraryAnalysisObject,
                     IARG_THREAD_ID,
                     IARG_FUNCARG_ENTRYPOINT_REFERENCE,
                     0,
                     IARG_RETURN_IP,
                     IARG_END);
      RTN_InsertCall(routineOfInterest,
                     IPOINT_AFTER,
                     (AFUNPTR)afterTryEnterCriticalSection,
                     IARG_PTR,
                     threadLibraryAnalysisObject,
                     IARG_THREAD_ID,
                     IARG_FUNCRET_EXITPOINT_VALUE,
                     IARG_RETURN_IP,
                     IARG_END);
      RTN_Close(routineOfInterest);
#if defined TRACING_LEVEL1 || defined TRACING_LEVEL2
      BOOST_LOG_TRIVIAL(trace) << "[Trace][Lock] Instrumenting RTLTryEnterCriticalSection";
#endif
    }

    // Instrumenting RtlLeaveCriticalSection routine
    routineOfInterest = RTN_FindByName(img, "RtlLeaveCriticalSection");
    if (RTN_Valid(routineOfInterest)) {
      RTN_Open(routineOfInterest);
      RTN_InsertCall(routineOfInterest,
                     IPOINT_BEFORE,
                     (AFUNPTR)beforeLeaveCriticalSection,
                     IARG_PTR,
                     threadLibraryAnalysisObject,
                     IARG_THREAD_ID,
                     IARG_FUNCARG_ENTRYPOINT_REFERENCE,
                     0,
                     IARG_RETURN_IP,
                     IARG_END);
      RTN_InsertCall(routineOfInterest,
                     IPOINT_AFTER,
                     (AFUNPTR)afterLeaveCriticalSection,
                     IARG_PTR,
                     threadLibraryAnalysisObject,
                     IARG_THREAD_ID,
                     IARG_RETURN_IP,
                     IARG_END);
      RTN_Close(routineOfInterest);
#if defined TRACING_LEVEL1 || defined TRACING_LEVEL2
      BOOST_LOG_TRIVIAL(trace) << "[Trace][Lock] Instrumenting RTLLeaveCriticalSection";
#endif
    }

    // Instrumenting RtlDeleteCriticalSection routine
    routineOfInterest = RTN_FindByName(img, "RtlDeleteCriticalSection");
    if (RTN_Valid(routineOfInterest)) {
      RTN_Open(routineOfInterest);
      RTN_InsertCall(routineOfInterest,
                     IPOINT_BEFORE,
                     (AFUNPTR)beforeDeleteCriticalSection,
                     IARG_PTR,
                     threadLibraryAnalysisObject,
                     IARG_THREAD_ID,
                     IARG_FUNCARG_ENTRYPOINT_REFERENCE,
                     0,
                     IARG_RETURN_IP,
                     IARG_END);
      RTN_InsertCall(routineOfInterest,
                     IPOINT_AFTER,
                     (AFUNPTR)afterDeleteCriticalSection,
                     IARG_PTR,
                     threadLibraryAnalysisObject,
                     IARG_THREAD_ID,
                     IARG_RETURN_IP,
                     IARG_END);
      RTN_Close(routineOfInterest);
#if defined TRACING_LEVEL1 || defined TRACING_LEVEL2
      BOOST_LOG_TRIVIAL(trace) << "[Trace][Lock] Instrumenting RTLDeleteCriticalSection";
#endif
    }
  }
#endif
}

void instlibMainEnter(VOID *v)
{
  PinToolInstrumentWin32ThreadLibrary *threadLibraryInstrumentObject =
    static_cast<PinToolInstrumentWin32ThreadLibrary *>(v);
  PinToolAnalyzeWin32ThreadLibrary *threadLibraryAnalysisObject =
    threadLibraryInstrumentObject->pinToolAnalyzeWin32ThreadLibraryObject;
  tlibMainEnter(threadLibraryAnalysisObject);
}

void instlibMainLeave(VOID *v)
{
  PinToolInstrumentWin32ThreadLibrary *threadLibraryInstrumentObject =
    static_cast<PinToolInstrumentWin32ThreadLibrary *>(v);
  PinToolAnalyzeWin32ThreadLibrary *threadLibraryAnalysisObject =
    threadLibraryInstrumentObject->pinToolAnalyzeWin32ThreadLibraryObject;
  tlibMainLeave(threadLibraryAnalysisObject);
}

}  // namespace pcv

#endif
