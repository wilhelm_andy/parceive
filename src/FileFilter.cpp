//
// Created by wilhelma on 1/23/17.
//

#include "FileFilter.h"

namespace pcv {

FileFilter::FileFilter(const std::string &includePattern, const std::string &excludePattern)
  : include_(includePattern), exclude_(excludePattern){};

bool FileFilter::isIncluded(const std::string &fileName) const
{
  return regex_match(fileName, include_);
}

bool FileFilter::isExcluded(const std::string &fileName) const
{
  return regex_match(fileName, exclude_);
}

bool FileFilter::isIncluded(const std::string &fileName, uint32_t lineNo) const
{
  return !isExcluded(fileName) && isIncluded(fileName) && (lineNo > 0);
}

void FileFilter::assign(const std::string &include, const std::string &exclude)
{
  include_.assign(include);
  exclude_.assign(exclude);
}

}  // namespace pcv
