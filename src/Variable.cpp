
#include "Variable.h"

#include <cassert>
#include <iostream>

#include "Parceive.h"
#include "Resolver.h"
#include "Routine.h"
#include "Thread.h"

#include "entities/Class.h"

namespace pcv {
using pcv::entity::Class;

Variable::Variable(Parceive *parceive)
  : Module(parceive),
    _traceOffsets(pParceive->getKnob("offsets") == "on"),
    pModel(nullptr),
    pThread(nullptr),
    pResolver(nullptr)
{}

Variable::~Variable(void) {}

void Variable::initialize()
{
  pModel    = pParceive->getModel();
  pThread   = pParceive->getThread();
  pResolver = pParceive->getResolver();
}

void Variable::finalize() {}

void Variable::beforeInstrumentation()
{
  IMG_AddInstrumentFunction(cb_instrumentImage, this);
  TRACE_AddInstrumentFunction(cb_instrumentTrace, this);
}

void Variable::instrumentInstruction(IMG img, RTN rtn, INS ins)
{
  /*
  if (KnobModeName.Value() == "emulate" ||
  KnobInstrumentAccesses.Value() !== "true")
  return;
  */
  ADDRINT ip          = INS_Address(ins);
  UINT32 mem_operands = INS_MemoryOperandCount(ins);
  for (UINT32 mem_op = 0; mem_op < mem_operands; mem_op++) {
    int operand = INS_MemoryOperandIndexToOperandIndex(ins, mem_op);

    auto getAccessType = [](INS &ins, UINT32 mem_op) {
      auto isRead    = (INS_MemoryOperandIsRead(ins, mem_op));
      auto isWritten = (INS_MemoryOperandIsWritten(ins, mem_op));

      if (isRead && isWritten)
        return model::Access::Type::READWRITE;
      else if (isRead)
        return model::Access::Type::READ;
      else if (isWritten)
        return model::Access::Type::WRITE;
      else
        return model::Access::Type::ELSE;
    };

    auto accessType = getAccessType(ins, mem_op);
    if (accessType == model::Access::Type::ELSE) continue;

    INT32 lineNo{0};

    if (pParceive->getLookupSourceLocations()) {
      pParceive->getSymInfo()->getSourceLocation(ip, &lineNo, nullptr);
    }

    model::Reference::Type refType = model::Reference::Type::UNKNOWN;
    void *location                 = nullptr;

    resolveVariableInstrumentationTime(
      RTN_Id(rtn), ins, operand, mem_op, IMG_Id(img), &location, &refType);

    // Register function 'memory_access_read'
    // clang-format off
    INS_InsertPredicatedCall(ins, IPOINT_BEFORE, (AFUNPTR)callCaptureMemoryAccess,
                             IARG_PTR, this,
                             IARG_THREAD_ID,
                             IARG_MEMORYOP_EA, mem_op,
                             IARG_UINT32, IMG_Id(img),
                             IARG_UINT32, RTN_Id(rtn),
                             IARG_UINT32, mem_op,
                             IARG_UINT32, (UINT32)lineNo,
                             IARG_PTR, location,
                             IARG_UINT32, (UINT32)refType,
                             IARG_UINT32, (UINT32)accessType,
                             IARG_REG_VALUE, REG_GBP,
                             IARG_ADDRINT, ip,
                             IARG_END);
    // clang-format on
  }
}

void Variable::resolveVariableInstrumentationTime(unsigned int rtnId,
                                                  INS ins,
                                                  int operand,
                                                  UINT32 /*mem_op*/,
                                                  unsigned int /*imageId*/,
                                                  void **location,
                                                  model::Reference::Type *type)
{
  std::string baseReg    = REG_StringShort(INS_OperandMemoryBaseReg(ins,
                                                                    static_cast<UINT32>(operand)));
  std::string segmentReg = REG_StringShort(INS_OperandMemorySegmentReg(ins,
                                                                       static_cast<UINT32>(operand)));
  S_ADDRINT displacement = INS_OperandMemoryDisplacement(ins, static_cast<UINT32>(operand));
  if (baseReg == FRAME_POINTER) {
    SymbolInfo *info = pResolver->getStackSymbolInfo(rtnId, displacement);

    if (info == nullptr) {
      *type     = model::Reference::Type::UNKNOWN;
      *location = nullptr;
    } else {
      *location = reinterpret_cast<void *>(info);
      *type     = model::Reference::Type::STACK;

#if defined NORMALMODE || defined DATABASEWRITE
#ifndef STACKHANDLING
      if (localVar->firstTime) {
        char *varNameCString = const_cast<char *>((localVar->name).c_str());

        query->processReference(localVar->index,
                                (unsigned int)localVar->variableSize,
                                Reference::Type::STACK,
                                varNameCString,
                                currentRtnCount);

        localVar->firstTime = false;
      }
#endif
#endif
    }
#if defined __linux__ && defined __x86_64__
  } else if (baseReg == "rip") {
    S_ADDRUINT ip = INS_Address(ins);
    displacement  = ip + displacement + INS_Size(ins);
#elif defined __linux__ && defined __i386__
  } else if (baseReg == "*invalid*" && (segmentReg == "ds" || segmentReg == "es") &&
             displacement != 0) {
#elif defined _MSC_VER && _WIN64
  } else if (baseReg == "rip") {
    S_ADDRINT ip = INS_Address(ins);
    displacement = ip + displacement + INS_Size(ins);
#elif defined _MSC_VER && _WIN32
  } else if (baseReg == "*invalid*" && (segmentReg == "ds" || segmentReg == "es") &&
             displacement != 0) {
#endif

    S_ADDRUINT startAddress;
    SymbolInfo *info = pResolver->getGlobalSymbolInfo(static_cast<S_UINT64>(displacement), &startAddress);

    if (info == nullptr) {
      *type = model::Reference::Type::UNKNOWN;
    } else {
      *location = reinterpret_cast<void *>(info);
      *type     = model::Reference::Type::GLOBAL;
    }
  }
}

void Variable::cb_instrumentImage(IMG img, VOID *v)
{
  auto *pThis = static_cast<Variable *>(v);

  RTN routineOfInterest = RTN_FindByName(img, "malloc");
  if (RTN_Valid(routineOfInterest)) {
    RTN_Open(routineOfInterest);

    // clang-format off
      RTN_InsertCall(routineOfInterest, IPOINT_BEFORE, AFUNPTR(callDynamicMemoryAllocationBefore),
                     IARG_PTR, pThis,
                     IARG_THREAD_ID,
                     IARG_G_ARG0_CALLEE,
                     IARG_UINT32, 1,
                     IARG_PTR, NULL,
                     IARG_RETURN_IP,
                     IARG_END);
    // clang-format on
    RTN_Close(routineOfInterest);
  }
  routineOfInterest = RTN_FindByName(img, "calloc");
  if (RTN_Valid(routineOfInterest)) {
    // Instrumenting calloc routine
    RTN_Open(routineOfInterest);
    // clang-format off
      RTN_InsertCall(routineOfInterest, IPOINT_BEFORE, AFUNPTR(callDynamicMemoryAllocationBefore),
                     IARG_PTR, pThis,
                     IARG_THREAD_ID,
                     IARG_G_ARG0_CALLEE,
                     IARG_G_ARG1_CALLEE,
                     IARG_PTR, NULL,
                     IARG_RETURN_IP,
                     IARG_END);
    // clang-format on
    RTN_Close(routineOfInterest);
  }
  routineOfInterest = RTN_FindByName(img, "realloc");
  if (RTN_Valid(routineOfInterest)) {
    // Instrumenting realloc routine
    RTN_Open(routineOfInterest);
    // clang-format off
      RTN_InsertCall(routineOfInterest, IPOINT_BEFORE, AFUNPTR(callDynamicMemoryAllocationBefore),
                     IARG_PTR, pThis,
                     IARG_THREAD_ID,
                     IARG_G_ARG1_CALLEE,
                     IARG_UINT32, 1,
                     IARG_G_ARG0_CALLEE,
                     IARG_RETURN_IP,
                     IARG_END);
    // clang-format on
    RTN_Close(routineOfInterest);
  }
  routineOfInterest = RTN_FindByName(img, "free");
  if (RTN_Valid(routineOfInterest)) {
    // Instumenting free, operator delete and operator delete[] routines
    RTN_Open(routineOfInterest);
    // clang-format off
      RTN_InsertCall(routineOfInterest, IPOINT_BEFORE, AFUNPTR(callDynamicMemoryFreeBefore),
                     IARG_PTR, pThis,
                     IARG_FUNCARG_ENTRYPOINT_VALUE, 0,
                     IARG_END);
    // clang-format on
    RTN_Close(routineOfInterest);
  }
  for (SEC sec = IMG_SecHead(img); SEC_Valid(sec); sec = SEC_Next(sec)) {
    for (RTN rtn = SEC_RtnHead(sec); RTN_Valid(rtn); rtn = RTN_Next(rtn)) {
      std::string rtnName = PIN_UndecorateSymbolName(RTN_Name(rtn), UNDECORATION_NAME_ONLY);
      // Instrumenting malloc,operator new and operator new[] routines
      if (rtnName == "operator new") {
        RTN_Open(rtn);
        // clang-format off
          RTN_InsertCall(rtn, IPOINT_BEFORE, AFUNPTR(callDynamicMemoryAllocationBefore),
                         IARG_PTR, pThis,
                         IARG_THREAD_ID,
                         IARG_G_ARG0_CALLEE,
                         IARG_UINT32, 1,
                         IARG_PTR, NULL,
                         IARG_RETURN_IP,
                         IARG_END);
        // clang-format on
        RTN_Close(rtn);
      } else if (rtnName == "operator delete" || rtnName == "operator delete[]") {
        // Instumenting free, operator delete and operator delete[] routines
        RTN_Open(rtn);
        // clang-format off
          RTN_InsertCall(rtn, IPOINT_BEFORE, AFUNPTR(callDynamicMemoryFreeBefore),
                         IARG_PTR, pThis,
                         IARG_FUNCARG_ENTRYPOINT_VALUE, 0,
                         IARG_END);
        // clang-format on
        RTN_Close(rtn);
      }
    }
  }
}

void Variable::cb_instrumentTrace(TRACE trace, VOID *v)
{
  auto *pThis = static_cast<Variable *>(v);

  if (pThis->allocReturns_.empty()) return;

  for (auto bbl = TRACE_BblHead(trace); BBL_Valid(bbl); bbl = BBL_Next(bbl)) {
    for (auto ins = BBL_InsHead(bbl); INS_Valid(ins); ins = INS_Next(ins)) {
      S_ADDRINT insAddr = INS_Address(ins);
      auto it           = pThis->allocReturns_.find(insAddr);
      if (it != pThis->allocReturns_.end()) {
        // clang-format off
          INS_InsertCall(ins, IPOINT_BEFORE, AFUNPTR(callDynamicMemoryAllocationAfter),
                         IARG_PTR, pThis,
                         IARG_THREAD_ID,
                         IARG_FUNCRET_EXITPOINT_VALUE,
                         IARG_END);
        // clang-format on

        pThis->allocReturns_.erase(it);
      }
    }
  }
}

void Variable::callCaptureMemoryAccess(Variable *pThis,
                                       THREADID threadId,
                                       ADDRINT address,
                                       S_ADDRUINT imageId,
                                       S_ADDRUINT routineNo,
                                       S_ADDRUINT pos,
                                       S_ADDRUINT lineNo,
                                       void *location,
                                       UINT32 refType_in,
                                       UINT32 accessType_in,
                                       ADDRINT ebp,
                                       ADDRINT ip)
{
  if (!pThis->pParceive->getAnalysisEnabled()) return;

  const auto refType    = model::Reference::Type(refType_in);
  const auto accessType = model::Access::Type(accessType_in);

  model::Image::Id_t image{};
  uint32_t symbol{};
  std::string varName;
  ADDRINT allocatorIP = 0;
  S_ADDRUINT variableSize;

  auto threadData = pThis->pThread->getThreadData(threadId);
  auto callStack  = threadData->getCallStack();

  // the stack can't be empty. we're in a memory access callback,
  // which means we're in an instrumented function, so there
  // must be a stack frame.
  if (callStack->empty()) return;

  const auto &topStack = callStack->top();

  int realLineNo =
    static_cast<int>(topStack->routineId == routineNo ? lineNo : topStack->lastCallLineNo);

  if (topStack->isRecentlyAccessed(realLineNo, address, accessType)) return;

  Reference_t *ref;
  if (!pThis->pResolver->getReference(address, &ref)) {
    SymbolInfo *info = nullptr;
    if (refType == model::Reference::Type::UNKNOWN) {
      // first access of a global reference
      S_ADDRUINT startAddress{};
      info = pThis->pResolver->getGlobalSymbolInfo(address, &startAddress);
      if (info) {
        pThis->pResolver->addGlobalReference(startAddress, info, &ref);
        if (info->classId.isValid())
          pThis->pResolver->assignReferenceToClass(startAddress, ref->id, info->classId);
      } else if (pThis->pParceive->traceUnknownSymbols() && lineNo) {
        ref = pThis->pResolver->getUnknownReference();
      } else {
        return;
      }
    } else {
      info = reinterpret_cast<SymbolInfo *>(location);

      // we can only reach this in case of STACK or GLOBAL references,
      // so symbol info must have been passed in the callback.
      assert(info != nullptr);

      // add the reference
      if (refType == model::Reference::Type::STACK) {
        allocatorIP = routineNo;
        pThis->pResolver->addStackReference(address, info, topStack->callId, &ref);
      } else if (refType == model::Reference::Type::GLOBAL) {
        allocatorIP = imageId;
        pThis->pResolver->addGlobalReference(address, info, &ref);
      } else {
        // So, refType is not UNKNOWN, STACK, or GLOBAL
        // In this case the resolver must have given us a reference!
        assert(false);
      }

      // assign and refine member variables references in case of class/struct
      if (info->classId.isValid())
        pThis->pResolver->assignReferenceToClass(address, ref->id, info->classId);
    }
  }

  // At this point, ref points to a valid reference
  assert(ref != nullptr);
  assert(ref->type != model::Reference::Type::UNKNOWN);

  if (ref->type == model::Reference::Type::HEAP) {
    // At this point, the allocated reference must already exist. But
    // we may be accessing a member which does not. So save the
    // allocation location just in case.
    auto *hRef  = static_cast<AllocatedReference_t *>(ref);
    allocatorIP = hRef->allocationIP;
  }

  model::Reference::Id_t refId{ ref->id };
  symbol = (uint32_t)ref->info->symbolId;
  image  = (uint32_t)ref->info->imageId;

  varName      = ref->info->name;
  variableSize = ref->info->size;

  // refine reference in case of structs/classes
  while (1) {
    pThis->pModel->processReference(refId,
                                    Thread::getThreadId(threadId),
                                    (unsigned int)variableSize,
                                    ref->type,
                                    varName.c_str(),
                                    allocatorIP,
                                    symbol,
                                    image);

    model::Reference::Id_t refId_old = refId;
    pThis->pResolver->refineReference(
      address, *ref, &refId, &variableSize, &varName, &symbol, &image);

    if (refId_old == refId) {
      // there was no more refinement -> stop.
      break;
    }
  }

  // refineReference may change the refId to that of a class/struct member,
  // but it may not set it to EMPTY!
  assert(refId != model::Reference::EMPTY);

  // consider refined reference when leaving invocations
  model::Instruction::Id_t instructionID;
  pThis->pModel->processInstruction(
    threadId, topStack->callId, model::Instruction::Type::ACCESS, realLineNo, &instructionID);

  pThis->pModel->processVarAccess(threadId,
                                  instructionID,
                                  static_cast<unsigned>(pos),
                                  refId,
                                  accessType,
                                  model::Access::State::INIT,
                                  (ref->startAddress == 0) ? 0 : address - ref->startAddress);
}

void Variable::callDynamicMemoryAllocationBefore(Variable *pThis,
                                                 THREADID THREADID,
                                                 unsigned int requestedSize,
                                                 unsigned int numberOfBlocks,
                                                 S_ADDRUINT reAllocatedAddress,
                                                 S_ADDRINT returnIP)
{
  ThreadData *threadData = pThis->pThread->getThreadData(THREADID);

  if (!threadData->isAllocationStackEmpty()) {
    const auto &topAlloc = threadData->getTopAllocationData();
    if (topAlloc.returnIP == returnIP) {
      return;
    }
  }

  pThis->addAllocReturnAddress(returnIP);

  AllocationData data;
  data.reAllocAddress = reAllocatedAddress;
  data.size           = requestedSize * numberOfBlocks;
  data.returnIP       = returnIP;
  data.lineNo         = threadData->lineNo;

  threadData->pushAllocationData(data);
}

void Variable::callDynamicMemoryAllocationAfter(Variable *pThis,
                                                THREADID THREADID,
                                                S_ADDRUINT allocAddress)
{
  auto threadData = pThis->pThread->getThreadData(THREADID);

  // the instruction could happen after a allocation in case of nasty macros
  if (threadData->isAllocationStackEmpty()) return;

  auto callStack  = threadData->getCallStack();

  if (allocAddress != 0u && !callStack->empty()) {
    const model::Call::Id_t callID = callStack->top()->callId;
    Reference_t *ref;
    model::Instruction::Id_t instructionID;

    if (!pThis->pResolver->getReference(allocAddress, &ref) &&
        threadData->getAllocationStackSize() == 1) {
      // Why the test for allocation stack size == 1?
      // Allocations may be nested. E.g. new may call malloc internally. In that case both return
      // the same address. since the resolver can currently only handle one allocation reference per
      // address we'll only write the reference data when the alloation stack has a size of 1, since
      // this is nearest to where the allocation originated from a user perspective.
      const auto &data = threadData->getTopAllocationData();

      pThis->pModel->processInstruction(
        THREADID, callID, model::Instruction::Type::ALLOC, data.lineNo, &instructionID);

      // create and record the new reference
      std::string refName = std::to_string(allocAddress);
      pThis->pResolver->addAllocatedReference(
        allocAddress,
        std::unique_ptr<SymbolInfo>(
          new SymbolInfo(refName.c_str(), data.size, entity::ImageScopedId::getNone(), 0, 0)),
        instructionID,
        &ref);
      pThis->pModel->processReference(ref->id,
                                      Thread::getThreadId(THREADID),
                                      static_cast<unsigned int>(data.size),
                                      model::Reference::Type::HEAP,
                                      refName.c_str(),
                                      instructionID,
                                      static_cast<unsigned int>(ref->info->symbolId),
                                      ref->info->imageId);
    }
  }
  threadData->popAllocationData();
}

void Variable::callDynamicMemoryFreeBefore(Variable *pThis, S_ADDRUINT allocatedAddress)
{
  if (!(pThis->pParceive->getAnalysisEnabled())) {
    return;
  }

  pThis->pResolver->removeAllocatedReference(allocatedAddress);
}

void Variable::addAllocReturnAddress(S_ADDRUINT address) { allocReturns_.insert(address); }

}  // namespace pcv
