/* Copyright 2014 Siemens Technology and Services*/

#include "DataModel.h"
#include <string>

#include "LoopDetector.h"
#include "LoopManager.h"
#include "Parceive.h"
#include "SymInfo.h"
#include "Thread.h"
#include "Writer.h"

namespace pcv {

using namespace model;

DataModel::DataModel(Parceive *parceive) : Module(parceive), pWriter(nullptr), _filesCount(0) {}

DataModel::~DataModel(void) {}

void DataModel::initialize() { pWriter = pParceive->getWriter(); }

void DataModel::finalize() {}

bool DataModel::processCall(Call::Id_t callId,
                            Instruction::Id_t instructionID,
                            model::Thread::Id_t threadID,
                            Function::Id_t functionId,
                            unsigned long long startTime,
                            unsigned long long endTime)
{
  pWriter->insert(Call(callId, threadID, functionId, instructionID, startTime, endTime));
  return true;
}

bool DataModel::processCallEntry(Call::Id_t callerID,
                                 Call::Id_t calleeID,
                                 model::Thread::Id_t threadID,
                                 LoopIteration::Id_t loopIterationID)
{
  if (loopIterationID == LoopIteration::NULLVAL && !LoopManager::getInstance()->empty(callerID)) {
    const LoopState &state = LoopManager::getInstance()->top(callerID);
    loopIterationID        = state.iterationId;
  }

  auto threadData = pParceive->getThread()->getThreadData(static_cast<THREADID>(threadID));
  auto callStack  = threadData->getCallStack();

  Segment::Id_t segmentId     = Segment::getInstance()->nextId();
  callStack->top()->segmentId = segmentId;

  pWriter->insert(Segment(segmentId, calleeID, Segment::Type::REGION, loopIterationID));

  return true;
}

bool DataModel::processCallReEntry(Call::Id_t callID, model::Thread::Id_t threadID)
{
  processCallEntry(Call::EMPTY, callID, threadID, LoopIteration::NULLVAL);
  return true;
}

bool DataModel::processLoopEntry(model::Thread::Id_t threadID,
                                 Loop::Id_t loopId,
                                 unsigned int line,
                                 Call::Id_t call)
{
  if (LoopManager::getInstance()->enterLoop(call, loopId)) {
    pWriter->insert(Loop(loopId, line));
  }

  const LoopState &state  = LoopManager::getInstance()->top(call);
  Segment::Id_t segmentId = Segment::getInstance()->nextId();

  auto threadData = pParceive->getThread()->getThreadData(static_cast<THREADID>(threadID));
  auto callStack  = threadData->getCallStack();
  callStack->top()->segmentId = segmentId;

  pWriter->insert(Segment(segmentId, call, Segment::Type::LOOP, state.iterationId));

  // write a new loop iteration entry
  pWriter->insert(LoopIteration(state.iterationId, state.executionId, state.iterationNo));
  return true;
}

bool DataModel::processLoopExit(model::Thread::Id_t threadID, Loop::Id_t loopId, Call::Id_t call)
{
  if (LoopManager::getInstance()->empty(call)) return false;

  R_TIMERTYPE endTime = getTimer();

  const LoopState stateBefore = LoopManager::getInstance()->top(call);
  LoopManager::getInstance()->exitLoop(call, loopId);

  // erase loop iteration from database if no instruction was written
  // (this indicates a pure exit)
  if (!stateBefore.touched) {
    pWriter->remove(LoopIteration(stateBefore.iterationId, 0, 0));
  }

  LoopIteration::Id_t parentIterationId;
  Segment::Type segType;
  if (!LoopManager::getInstance()->empty(call)) {
    const LoopState stateAfter = LoopManager::getInstance()->top(call);
    parentIterationId          = stateAfter.iterationId;
    segType                    = Segment::Type::LOOP;
  } else {
    parentIterationId = LoopIteration::NULLVAL;
    segType           = Segment::Type::REGION;
  }

  Segment::Id_t segmentId = Segment::getInstance()->nextId();
  auto threadData         = pParceive->getThread()->getThreadData(static_cast<THREADID>(threadID));
  auto callStack          = threadData->getCallStack();
  callStack->top()->segmentId = segmentId;

  pWriter->insert(Segment(segmentId, call, segType, parentIterationId));

  pWriter->insert(LoopExecution(
    stateBefore.executionId, loopId, parentIterationId, stateBefore.startTime, endTime));

  return true;
}

bool DataModel::processReference(model::Reference::Id_t referenceId,
                                 model::Thread::Id_t,
                                 unsigned int size,
                                 model::Reference::Type memoryType,
                                 const char *name,
                                 model::Instruction::Id_t allocIns,
                                 unsigned int symbolId,
                                 model::Image::Id_t imageId)
{
  bool exist = (_processedReferenceIds.find(referenceId) != std::end(_processedReferenceIds));

  if (!exist) {
    pWriter->insert(Reference(referenceId, size, memoryType, name, allocIns, symbolId, imageId));
    _processedReferenceIds.emplace(referenceId);
  }

  return !exist;
}

bool DataModel::processInstruction(model::Thread::Id_t threadID,
                                   Call::Id_t callId,
                                   Instruction::Type instrType,
                                   int lineNo,
                                   Instruction::Id_t *instructionID)
{
  LoopManager::getInstance()->touch(callId);

  auto threadData = pParceive->getThread()->getThreadData(static_cast<THREADID>(threadID));
  auto callStack  = threadData->getCallStack();
  auto segmentId  = callStack->top()->segmentId;

  Instruction::Id_t curInsCount = Instruction::getInstance()->nextId();
  *instructionID                = curInsCount;
  pWriter->insert(Instruction(curInsCount, segmentId, instrType, lineNo, Instruction::NOCOLUMN));

  return true;
}

bool DataModel::processVarAccess(model::Thread::Id_t /*threadID*/,
                                 Instruction::Id_t instructionID,
                                 unsigned int pos,
                                 Reference::Id_t referenceId,
                                 Access::Type accessType,
                                 Access::State memState,
                                 uint64_t offset)
{
  pWriter->insert(Access(Access::getInstance()->nextId(),
                         instructionID,
                         pos,
                         referenceId,
                         accessType,
                         memState,
                         offset));
  return true;
}

bool DataModel::processThread(model::Thread::Id_t threadId,
                              unsigned long long start,
                              unsigned long long end,
                              Instruction::Id_t createInstruction,
                              Instruction::Id_t joinInstruction,
                              model::Thread::Id_t parentThreadId,
                              Call::Id_t call)
{
  pWriter->insert(
    model::Thread(threadId, start, end, createInstruction, joinInstruction, parentThreadId, call));
  return true;
}

bool DataModel::processImage(unsigned int imageId, const std::string &imageName)
{
  pWriter->insert(Image(imageId, imageName));
  const auto &files = pParceive->getSymInfo()->getFiles(imageId);

  for (const auto &file : files) {
    unsigned int fileId{};
    processFile(file->name, imageId, &fileId);
  }
  return true;
}

typedef struct routine_struct_t {
  std::string file;
  unsigned int lineNo;
  routine_struct_t(std::string file, unsigned int lineNo) : file(file), lineNo(lineNo) {}

  bool operator<(const struct routine_struct_t &rhs) const
  {
    return file <= rhs.file && lineNo < rhs.lineNo;
  }
} routine_struct_t;

bool DataModel::processRoutine(unsigned int imageId,
                               const std::string &routineName,
                               const std::string &prototype,
                               const std::string &file,
                               model::Function::Type functionType,
                               unsigned int lineNo,
                               unsigned int routineID,
                               unsigned int symbolId)
{
  unsigned int fileId{};
  processFile(file, imageId, &fileId);

  pWriter->insert(
    Function(routineID, routineName, prototype, fileId, lineNo, functionType, symbolId, imageId));

  return true;
}

bool DataModel::processFile(const std::string &file, unsigned int imageId, unsigned int *fileId)
{
  if (_fileNameIdMap.count(file) == 0) {
    unsigned int curFilesCount = _filesCount++;
    _fileNameIdMap[file]       = curFilesCount;
    pWriter->insert(File(curFilesCount, file, imageId));
  }

  *fileId = _fileNameIdMap[file];
  return true;
}

bool DataModel::processFile(model::Thread::Id_t /*THREADID*/,
                            char ** /*file*/,
                            unsigned int * /*fileId*/)
{
  bool status = false;
  return status;
}

}  // namespace pcv
