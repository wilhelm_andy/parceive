/* Copyright 2014 Siemens Technology and Services*/

#include <fstream>
#include <regex>
#include <string>
#include <vector>

#include "Common.h"
#include "Filter.h"
#include "JsonFilterSystem.h"
#include "XmlFilterSystem.h"

namespace pcv {

Filter::Filter(const string& filterFileName)
{
  if (endsWith(toUpper(filterFileName), ".JSON")) {
    filterSys = new JsonFilterSystem(filterFileName);
  } else {
    filterSys = new XmlFilterSystem(filterFileName);
  }
}

Filter::~Filter() { delete filterSys; }

#ifndef NODEJS
void Filter::forceInclusion(const RTN& rtn) { rtnFilter_map[RTN_Id(rtn)] = true; }

void Filter::forceInclusion(const IMG& img, const bool valid)
{
  imgFilter_map[IMG_Id(img)] = valid;
}

Filter::Action Filter::getDynamicFilterAction(UINT32 rtnId) const
{
  const auto it = dynamic_filter_map.find(rtnId);
  return (it == dynamic_filter_map.end()) ? NONE : it->second;
}

Filter::Action Filter::getLineFilterAction(const string& filePath, const int line) const
{
  return filterSys->getLineFilterAction(filePath, line);
}

void Filter::precalculateFilterStateFor(const RTN& rtn, const string& filename)
{
  const UINT32 rtnId   = RTN_Id(rtn);
  const string rtnName = PIN_UndecorateSymbolName(RTN_Name(rtn), UNDECORATION_COMPLETE);
  const auto it        = rtnFilter_map.find(rtnId);

  if (it == rtnFilter_map.end() && RTN_Valid(rtn) && !isInternal(rtnName)) {
    bool status = !filterSys->isRoutineExcluded(rtnName);
    if (status && !filename.empty()) {
      status = !filterSys->isFileExcluded(filename);
    }
    rtnFilter_map[rtnId] = status;

    if (filterSys->isRoutinePartial(rtnName)) {
      addPartial(RTN_Address(rtn));
    }
  }

  const Action dyn = filterSys->getDynamicFilterAction(rtnName);
  if (dyn == ENABLE || dyn == DISABLE) {
    dynamic_filter_map[rtnId] = dyn;
  }
}

bool Filter::isIncluded(const RTN& rtn) const
{
  const UINT32 rtnId = RTN_Id(rtn);
  const auto it      = rtnFilter_map.find(rtnId);
  return (it != rtnFilter_map.end()) && it->second;
}

bool Filter::isIncluded(const IMG& img)
{
  const UINT32 imgId = IMG_Id(img);
  const auto it      = imgFilter_map.find(imgId);
  if (it != imgFilter_map.end()) {
    return it->second;
  }
  const bool isIncluded = filterSys->isImageIncluded(IMG_Name(img));
  imgFilter_map[imgId]  = isIncluded;
  return isIncluded;
}

bool Filter::isIncluded(const std::string& filename)
{
  bool status = false;
  if (!filename.empty()) {
    status = !filterSys->isFileExcluded(filename);
  }
  return status;
}

bool Filter::isInternal(const string& rtnName)
{
  if (rtnName.compare(".text") == 0) return true;
  if (rtnName.compare(".unnamedImageEntryPoint") == 0) return true;
  if (rtnName.compare(".plt") == 0) return true;
  return rtnName.empty();
}

bool Filter::isRoutineAddressExpected(const ADDRINT target) const
{
  return contains(expectedIps_, target);
}

void Filter::addExpectedRoutineAddress(const ADDRINT ip) { expectedIps_.insert(ip); }

void Filter::setRtnInfo(const ADDRINT ip, std::unique_ptr<RtnInfo> info)
{
  rtnInfos_[ip] = std::move(info);
}

const RtnInfo* Filter::getRtnInfo(const ADDRINT ip)
{
  const auto it = rtnInfos_.find(ip);
  if (it == end(rtnInfos_)) return nullptr;
  return it->second.get();
}

#endif

void Filter::addPartial(ADDRINT ip) { partialIPs_.insert(ip); }

bool Filter::isPartial(ADDRINT target) const
{
  return (partialIPs_.find(target) != std::end(partialIPs_));
}

}  // namespace pcv
