#include "Exception.h"

#include <pin.H>
#include <iostream>

#include "SQLite.h"

namespace pcv {

void CorruptedBufferException(std::string err)
{
  PIN_WriteErrorMessage("Corrupted buffer", 1002, PIN_ERR_FATAL, 1, err.c_str());
}

void SQLiteException(std::string err, int code, std::string context)
{
  PIN_WriteErrorMessage("SQL Error", 1003, PIN_ERR_FATAL, 2, err.c_str(), context.c_str());
}

void SQLiteException(SQLite::Connection *db, int code, std::string context)
{
  SQLiteException(db->getErrorMessage(), code, context);
}

void SQLWriterException(string err, std::string context)
{
  PIN_WriteErrorMessage("SQLWriter Error", 1004, PIN_ERR_FATAL, 2, err.c_str(), context.c_str());
}

}  // namespace pcv
