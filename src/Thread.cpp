/* Copyright 2014 Siemens Technology and Services*/

#include "Thread.h"

#include <cassert>
#include <iostream>

#include "Parceive.h"

namespace pcv {

/* ThreadData methods */
size_t ThreadData::getAllocationStackSize() const { return allocations.size(); }
bool ThreadData::isAllocationStackEmpty() const { return allocations.empty(); }

const AllocationData &ThreadData::getTopAllocationData() const { return allocations.top(); }

void ThreadData::pushAllocationData(const AllocationData &data)
{
  allocations.push(data);

  // PIN_LockClient();
  // INT32 column, line;
  // string filename;
  // PIN_GetSourceLocation(data.returnIP, &column, &line, &filename);
  // auto rtn = RTN_FindByAddress(data.returnIP);
  // if (RTN_Valid(rtn)) {
  //  printf("alloc %d bytes at %p from routine: %s (%s/%d)\n", data.size, data.returnIP,
  //  RTN_Name(rtn).c_str(), filename.c_str(), line);
  //}
  // PIN_UnlockClient();
}

bool ThreadData::popAllocationData()
{
  if (isAllocationStackEmpty()) {
    return false;
  }

  // const auto&data = getTopAllocationData();
  // PIN_LockClient();
  // INT32 column, line;
  // string filename;
  // PIN_GetSourceLocation(data.returnIP, &column, &line, &filename);
  // auto rtn = RTN_FindByAddress(data.returnIP);
  // if (RTN_Valid(rtn)) {
  //  printf("popping alloc of %d bytes at %p from routine: %s (%s/%d)\n", data.size, data.returnIP,
  //  RTN_Name(rtn).c_str(), filename.c_str(), line);
  //}
  // PIN_UnlockClient();

  allocations.pop();
  return true;
}

/* Thread methods */
Thread::Thread(Parceive *parceive) : Module(parceive), pModel(nullptr) { PIN_RWMutexInit(&mutex); }

Thread::~Thread(void) { PIN_RWMutexFini(&mutex); }

void Thread::initialize() { pModel = pParceive->getModel(); }

void Thread::finalize()
{
  for (auto &it : threadDataMap) finalizeThread(it.first);
}

void Thread::beforeInstrumentation()
{
  PIN_AddThreadStartFunction(Thread::cb_threadStart, this);
  PIN_AddThreadFiniFunction(Thread::cb_threadFini, this);
}

ThreadData *Thread::getThreadData(THREADID threadId)
{
  PIN_RWMutexReadLock(&mutex);
  auto it = threadDataMap.find(threadId);
  PIN_RWMutexUnlock(&mutex);
  assert(it != std::end(threadDataMap));
  return it->second.get();
}

void Thread::setThreadData(THREADID threadId, std::unique_ptr<ThreadData> threadData)
{
  PIN_RWMutexWriteLock(&mutex);
  assert(threadDataMap.find(threadId) == std::end(threadDataMap));
  threadDataMap[threadId] = std::move(threadData);
  PIN_RWMutexUnlock(&mutex);
}

void Thread::removeThreadData(THREADID threadId)
{
  PIN_RWMutexWriteLock(&mutex);
  auto it = threadDataMap.find(threadId);
  assert(it != std::end(threadDataMap));
  threadDataMap.erase(it);
  PIN_RWMutexUnlock(&mutex);
}

void Thread::initializeThread(THREADID threadId)
{
  setThreadData(threadId, std::unique_ptr<ThreadData>{new ThreadData()});
}

void Thread::finalizeThread(THREADID threadId)
{
  auto threadData = getThreadData(threadId);
  auto callStack  = threadData->getCallStack();

  while (!callStack->empty()) {
    pModel->processCall(callStack->top()->callId,
                        threadData->model.createInstruction,
                        threadId,
                        callStack->top()->routineId,
                        threadData->model.start,
                        threadData->model.end);
    callStack->pop(pParceive->getResolver());
  }

  pModel->processThread(threadData->model.id,
                        threadData->model.start,
                        threadData->model.end,
                        threadData->model.createInstruction,
                        threadData->model.joinInstruction,
                        threadData->model.parentThread,
                        threadData->model.call);

  removeThreadData(threadId);
}

model::Thread::Id_t Thread::getThreadId(const THREADID &threadId)
{
  return static_cast<model::Thread::Id_t>(threadId);
}

void Thread::cb_threadStart(THREADID threadId, CONTEXT *ctxt, INT32 /*code*/, VOID *v)
{
  auto *pThis = static_cast<Thread *>(v);

  pThis->initializeThread(threadId);
  auto threadData = pThis->getThreadData(threadId);

  threadData->model.start = getTimer();
  threadData->model.id    = getThreadId(threadId);
}

void Thread::cb_threadFini(THREADID threadId, const CONTEXT * /*ctxt*/, INT32 /*code*/, VOID *v)
{
  auto *pThis            = static_cast<Thread *>(v);
  ThreadData *threadData = pThis->getThreadData(threadId);

  threadData->model.end = getTimer();

  // We don't call finalizeThread(threadId) here, because ThreadPosix
  // needs the ThreadData in the processing of pthread_join
  // But that's ok. Our finalize() will clean up.
}

}  // namespace pcv
