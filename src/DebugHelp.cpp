

/* Copyright 2014 Siemens Technology and Services*/

#ifdef _WIN32

namespace pcv {

DebugHelp::DebugHelp(void)
{
  variableList = new VariableList();
  initialSymbols();
  indexCount = 0;
}

DebugHelp::~DebugHelp(void) { unloadModules(); }

void DebugHelp::printUDTInfo() { variableList->printUDT_MembersInfo(); }

void DebugHelp::removeUDTInfoMap() { variableList->removeUDTName_MembersInfo_Map(); }

void DebugHelp::sortUDTInfo() { variableList->sortUDTMembersInfovectors(); }

void DebugHelp::removeUDTInfo() { variableList->removeUDTMembersInfovectors(); }

std::vector<UDTMembersInfo> *DebugHelp::getUDTMembersList(WINDOWS::ULONG typeIndex)
{
  WINDOWS::WCHAR *UDTName;
  WINDOWS::DWORD const MAXCHILDREN = 1000;
  WINDOWS::DWORD children[MAXCHILDREN];
  WINDOWS::DWORD numofchild = -1;
  std::vector<UDTMembersInfo> *membersInfoAddress;

  if (WINDOWS::SymGetTypeInfo(
        WINDOWS::GetCurrentProcess(), modBase, typeIndex, WINDOWS::TI_GET_SYMNAME, &UDTName)) {
    std::wstring typeName(UDTName);
    std::string type(typeName.begin(), typeName.end());

    if (variableList->getUDTMembersVectorAddress(type, &membersInfoAddress)) {
      if (getChildren(
            WINDOWS::GetCurrentProcess(), modBase, typeIndex, children, &numofchild, MAXCHILDREN)) {
        for (unsigned int i = 0; i < numofchild; i++) {
          CSymbolInfoPackage childSymbol;
          WINDOWS::SymFromIndex(
            WINDOWS::GetCurrentProcess(), modBase, children[i], &childSymbol.si);
          WINDOWS::DWORD offset;
          if (childSymbol.si.Tag == WINDOWS::SymTagData) {
            WINDOWS::SymGetTypeInfo(
              WINDOWS::GetCurrentProcess(), modBase, children[i], WINDOWS::TI_GET_OFFSET, &offset);
            UDTMembersInfo *UDTmember = new UDTMembersInfo((S_ADDRUINT)offset,
                                                           (S_ADDRUINT)childSymbol.si.Size,
                                                           (std::string)childSymbol.si.Name,
                                                           NULL);
            membersInfoAddress->push_back(*UDTmember);
          } else if (childSymbol.si.Tag == WINDOWS::SymTagBaseClass) {
            WINDOWS::SymGetTypeInfo(
              WINDOWS::GetCurrentProcess(), modBase, children[i], WINDOWS::TI_GET_OFFSET, &offset);
            UDTMembersInfo *UDTmember =
              new UDTMembersInfo((S_ADDRUINT)offset,
                                 (S_ADDRUINT)childSymbol.si.Size,
                                 (std::string)childSymbol.si.Name,
                                 getUDTMembersList(childSymbol.si.TypeIndex));
            membersInfoAddress->push_back(*UDTmember);
          }
        }
      }
    }
    return membersInfoAddress;
  }
  return NULL;
}

void DebugHelp::showTypeDetails(WINDOWS::SYMBOL_INFO *symInfo)
{
  WINDOWS::DWORD UDTKind;

  if (WINDOWS::SymGetTypeInfo(
        WINDOWS::GetCurrentProcess(), modBase, symInfo->Index, WINDOWS::TI_GET_UDTKIND, &UDTKind)) {
    if (UDTKind == 1) {
      classTypeCount++;
    }
  }
}

void DebugHelp::showSymbolDetails(WINDOWS::SYMBOL_INFO *symInfo)
{
  // Todo:: works for 64 but need to do extensive testing
  long offset = (long)symInfo->Address;
  long tempOffset;
  if (offset >= 0) {
    tempOffset = offset;
  } else {
    // TODO(Samya): understand in detail
    tempOffset = -(UINT_MAX - offset + 1);
  }

  if (!(imageLoad.count(imageId)) && symInfo->Size > 0 && symInfo->Tag == WINDOWS::SymTagData) {
    GlobalDynamicVariable globalVariable;
    globalVariable.address      = (S_ADDRUINT)symInfo->Address;
    globalVariable.index        = static_cast<int>(symInfo->Index);
    globalVariable.typeIndex    = static_cast<int>(symInfo->TypeIndex);
    globalVariable.variableSize = (unsigned long long)symInfo->Size;
    globalVariable.name         = (std::string)symInfo->Name;
    globalVariable.firstTime    = true;
    globalVariable.variableType = Reference::Type::GLOBAL;
    globalVariable.allocatorIP  = imageId;
    globalVariable.moreUDTInfo  = getUDTMembersList(symInfo->TypeIndex);
    variableList->globalListInsert(globalVariable, imageId);

#ifdef TRACING_LEVEL2
    BOOST_LOG_TRIVIAL(debug) << "[Debug][Variable][Insert] Global Variable:: Index:"
                             << globalVariable.index
                             << " variableSize:" << globalVariable.variableSize
                             << " name:" << globalVariable.name;
#endif
  } else if (imageLoad.count(imageId) > 0) {
    this->isLocalvariableSeen = true;
    if (symInfo->Flags & SYMFLAG_REGREL) {
      LocalVariable localVariable;
      localVariable.index        = static_cast<int>(symInfo->Index);
      localVariable.typeIndex    = static_cast<int>(symInfo->TypeIndex);
      localVariable.name         = (std::string)symInfo->Name;
      localVariable.offset       = tempOffset;
      localVariable.variableSize = (unsigned int)symInfo->Size;
      localVariable.firstTime    = true;
      localVariable.variableType = Reference::Type::STACK;
      localVariable.imageID      = imageId;
      localVariable.moreUDTInfo  = getUDTMembersList(symInfo->TypeIndex);
      variableList->localVariableMapInsert(routineNo, localVariable);

#ifdef TRACING_LEVEL2
      BOOST_LOG_TRIVIAL(debug) << "[Debug][Variable][Insert] Local Variable:: Index:"
                               << localVariable.index << " offset:" << localVariable.offset
                               << " name:" << localVariable.name;
#endif
    } else {
      GlobalDynamicVariable globalVariable;
      globalVariable.address      = (S_ADDRUINT)symInfo->Address;
      globalVariable.index        = static_cast<int>(symInfo->Index);
      globalVariable.typeIndex    = static_cast<int>(symInfo->TypeIndex);
      globalVariable.variableSize = (unsigned long long)symInfo->Size;
      globalVariable.name         = (std::string)symInfo->Name;
      globalVariable.firstTime    = true;
      globalVariable.variableType = Reference::Type::STATICVAR;
      globalVariable.allocatorIP  = routineNo;
      globalVariable.moreUDTInfo  = getUDTMembersList(symInfo->TypeIndex);
      variableList->globalListInsert(globalVariable, imageId);
#ifdef TRACING_LEVEL2
      BOOST_LOG_TRIVIAL(debug) << "[Debug][Variable][Insert] Static Variable:: Index:"
                               << globalVariable.index << " address:" << globalVariable.address
                               << " name:" << globalVariable.name;
#endif
    }
  }
}

WINDOWS::BOOL CALLBACK myEnumSymbolsCallback(WINDOWS::SYMBOL_INFO *pSymInfo,
                                             WINDOWS::ULONG symbolSize,
                                             WINDOWS::PVOID userContext)
{
  if (pSymInfo != 0) {
    // Display information about the symbol
    static_cast<DebugHelp *>(userContext)->showSymbolDetails(pSymInfo);
  }
  return TRUE;  // Continue enumeration
}

WINDOWS::BOOL CALLBACK myEnumTypesCallback(WINDOWS::SYMBOL_INFO *pSymInfo,
                                           WINDOWS::ULONG symbolSize,
                                           WINDOWS::PVOID userContext)
{
  if (pSymInfo != 0) {
    // Display information about the symbol
    static_cast<DebugHelp *>(userContext)->showTypeDetails(pSymInfo);
  }
  return TRUE;  // Continue enumeration
}

bool DebugHelp::checkIfTypeNameExists(std::string typeName)
{
  classTypeCount   = 0;
  std::string type = std::string("");
  type.append(std::string("*!")).append(typeName);

  WINDOWS::SymEnumTypesByName(
    WINDOWS::GetCurrentProcess(), modBase, type.c_str(), myEnumTypesCallback, this);

  if (classTypeCount) return true;

  return false;
}

void DebugHelp::getSymbolsbyAddr(S_ADDRUINT ip)
{
  WINDOWS::BOOL bRet = FALSE;

  /*Contains information about a range of pages in the virtual address space
  //of a process.*/
  WINDOWS::MEMORY_BASIC_INFORMATION mbi;

  // Retrieves information about a range of pages in the virtual address
  // space of the calling process
  VirtualQuery(reinterpret_cast<void *>(ip), &mbi, sizeof(mbi));

  WINDOWS::SymSetSearchPath(WINDOWS::GetCurrentProcess(), symbolSearchPath.c_str());
  // Initializing the symbol handler for the process
  if (modBaseMap.count((S_ADDRUINT)mbi.AllocationBase) == 0) {
    modBase = WINDOWS::SymLoadModule(WINDOWS::GetCurrentProcess(),
                                     NULL,
                                     getMainExecutableName().c_str(),
                                     NULL,
                                     (S_DWORD)mbi.AllocationBase,
                                     0);
    modBaseMap[(S_ADDRUINT)mbi.AllocationBase] = modBase;

    if (modBase == NULL) {
      BOOST_LOG_TRIVIAL(warning) << "[Warning][DEBUG HELP] "
                                    "SymLoadModule failed ";
    } else {
      BOOST_LOG_TRIVIAL(info) << "[Info][DEBUG HELP] "
                                 "SymLoadModule worked ";
    }
  } else {
    modBase = modBaseMap[(S_ADDRUINT)mbi.AllocationBase];
  }

  if (modBase == 0) return;

  // For the first time it enumerates global variables & stores in a hashmap
  if (!(imageLoad.count(imageId))) {
    WINDOWS::SymEnumSymbols(WINDOWS::GetCurrentProcess(),
                            modBase,
                            "*!*",
                            myEnumSymbolsCallback,  // The callback function
                            this);                  // User-defined context

    imageLoad[imageId] = true;
  }

  WINDOWS::SymSetOptions(WINDOWS::SymGetOptions() & ~SYMOPT_UNDNAME);
  WINDOWS::IMAGEHLP_STACK_FRAME stk;
  stk.InstructionOffset = ip;
  SymSetContext(WINDOWS::GetCurrentProcess(), &stk, NULL);

  // Contains SYMBOL_INFO structure & additional space for name of the symbol
  CSymbolInfoPackage sip;
  WINDOWS::DWORD64 Displacement = 0;

  bRet = WINDOWS::SymFromAddr(
    // Process handle of the current process
    WINDOWS::GetCurrentProcess(),
    // Symbol address
    ip,
    // Address of the variable that will receive the displacement
    &Displacement,
    // Address of the SYMBOL_INFO structure (inside "sip" object)
    &sip.si);

  if (bRet) {
    // Finds if the variable is a local variable
    if (sip.si.Tag == WINDOWS::SymTagFunction) {
      // Enumerate local variables
      bRet = WINDOWS::SymEnumSymbols(
        // Process handle of the current process
        WINDOWS::GetCurrentProcess(),
        // 0 -> SymEnumSymbols will use the context set with SymSetContext
        0,
        // Mask must also be 0 to use the context
        0,
        // The callback function
        myEnumSymbolsCallback,
        // User-defined context
        this);
    }
  } else {
#if defined TRACING_LEVEL2
    BOOST_LOG_TRIVIAL(warning) << "[Warning][Enumeration][Routine] SymFromAddr failed";
#endif
  }
}

bool DebugHelp::getChildren(
  WINDOWS::HANDLE hProcess,     // [in]  Process handle
  WINDOWS::DWORD64 ModuleBase,  // [in]  Module base address
  WINDOWS::ULONG TypeIndex,     // [in]  Index of the symbol whose children are needed
  WINDOWS::ULONG *pChildren,    // [out] Points to the buffer that will receive
                                // the child indices
  WINDOWS::DWORD *NumChildren,  // [out] Number of children found
  WINDOWS::DWORD MaxChildren    // [in]  Maximal number of child indices the
                                // buffer can store
)
{
  // Get the number of children
  WINDOWS::DWORD ChildCount = 0;

  if (!WINDOWS::SymGetTypeInfo(
        hProcess, ModuleBase, TypeIndex, WINDOWS::TI_GET_CHILDRENCOUNT, &ChildCount)) {
    WINDOWS::DWORD ErrCode = WINDOWS::GetLastError();
#ifdef TRACING_LEVEL1
    BOOST_LOG_TRIVIAL(warning) << "[Warning][DEBUG HELP] "
                                  "SymGetTypeInfo(TI_GET_CHILDRENCOUNT) failed. "
                                  "Error Code="
                               << ErrCode;
#endif
    return false;
  }

  if (ChildCount == 0) {
    *NumChildren = 0;
    return true;
  }

  // Get the children
  int FindChildrenSize =
    sizeof(WINDOWS::TI_FINDCHILDREN_PARAMS) + ChildCount * sizeof(WINDOWS::ULONG);
  WINDOWS::TI_FINDCHILDREN_PARAMS *pFC =
    (WINDOWS::TI_FINDCHILDREN_PARAMS *)_alloca(FindChildrenSize);
  memset(pFC, 0, FindChildrenSize);
  pFC->Count = ChildCount;

  if (!SymGetTypeInfo(hProcess, ModuleBase, TypeIndex, WINDOWS::TI_FINDCHILDREN, pFC)) {
    WINDOWS::DWORD ErrCode = WINDOWS::GetLastError();
#ifdef TRACING_LEVEL1
    BOOST_LOG_TRIVIAL(warning) << "[Warning][DEBUG HELP] "
                                  "SymGetTypeInfo(TI_FINDCHILDREN) failed. Error "
                                  "Code="
                               << ErrCode;
#endif
    return false;
  }

  WINDOWS::DWORD ChildIndex = 0;
  for (WINDOWS::DWORD i = 0; i < ChildCount; i++) {
    pChildren[ChildIndex] = pFC->ChildId[i];
    ChildIndex++;

    if (ChildIndex == MaxChildren) break;
  }

  *NumChildren = ChildIndex;
  return true;
}

// Gives the fully qualified name of the variable, useful member variables in
// nested objects or nested structs
bool DebugHelp::getExactVariableName(WINDOWS::ULONG parent_type_index,
                                     S_ADDRUINT parent_base_address,
                                     S_ADDRUINT EffectiveReadorWriteAddress,
                                     std::string *fully_qualified_name,
                                     WINDOWS::SYMBOL_INFO **symInfoObj,
                                     S_ADDRUINT operandSize)
{
  WINDOWS::DWORD numofchild        = -1;
  WINDOWS::DWORD const MAXCHILDREN = 1000;
  WINDOWS::DWORD children[MAXCHILDREN];
  WINDOWS::BOOL bRet;

  if (modBase == NULL) return false;

  if (getChildren(WINDOWS::GetCurrentProcess(),  // [in]  Process handle
                  modBase,                       // [in]  Module base address
                  parent_type_index,             // [in]  Index of the symbol whose
                                                 // children are needed
                  children,                      // [out] Points to the buffer that will receive the
                                                 // child indices
                  &numofchild,                   // [out] Number of children found
                  // [in]  Maximal number of child indices the buffer can store
                  MAXCHILDREN)) {
    if (numofchild == 0) return false;

    for (unsigned int i = 0; i < numofchild; i++) {
      CSymbolInfoPackage sip;
      bRet = WINDOWS::SymFromIndex(WINDOWS::GetCurrentProcess(),  // Process handle of the current
                                                                  // process
                                   modBase,                       // Module base address
                                   children[i],                   // Child Index
                                   // Address of the SYMBOL_INFO structure (inside "sip" object)
                                   &sip.si);

      WINDOWS::DWORD offset;
      if (WINDOWS::SymGetTypeInfo(
            WINDOWS::GetCurrentProcess(), modBase, children[i], WINDOWS::TI_GET_OFFSET, &offset)) {
        if (EffectiveReadorWriteAddress >= (parent_base_address + offset) &&
            (EffectiveReadorWriteAddress + operandSize - 1) <
              (parent_base_address + offset + sip.si.Size)) {
          if (sip.si.Tag == WINDOWS::SymTagData) {
            (*fully_qualified_name).append(".").append(sip.si.Name);
          }
          if (getExactVariableName(sip.si.TypeIndex,
                                   parent_base_address + offset,
                                   EffectiveReadorWriteAddress,
                                   fully_qualified_name,
                                   symInfoObj,
                                   operandSize)) {
            return true;
          } else {
            *symInfoObj  = new WINDOWS::SYMBOL_INFO;
            **symInfoObj = sip.si;

            return true;
          }
        }
      }
    }
    return false;
  }
  return false;
}

void DebugHelp::enumerateLocalVariables(S_ADDRUINT ip, S_ADDRUINT routineNo, S_ADDRUINT imgId)
{
  this->imageId   = imgId;
  this->routineNo = routineNo;

  getSymbolsbyAddr(ip);
}

void DebugHelp::deleteGlobalVariables(S_ADDRUINT imgId)
{
  variableList->globalVariablesListDelete(imgId);
}

void DebugHelp::setSymbolSearchPath(std::string imgPath) { this->symbolSearchPath = imgPath; }

bool DebugHelp::getIsLocalVariablesSeen() { return this->isLocalvariableSeen; }

void DebugHelp::setIsLocalVariablesSeen(bool value) { this->isLocalvariableSeen = value; }

bool DebugHelp::copyToAllGlobalsList(S_ADDRUINT imageId)
{
  return variableList->consolidateGlobalvariableLists(imageId);
}

void DebugHelp::resolveLocalVariable(S_ADDRINT offset,
                                     S_ADDRUINT routineNo,
                                     LocalVariable **locationAddress,
                                     S_ADDRUINT operandSize)
{
  variableList->localVariableMapSearch(offset, routineNo, locationAddress);

  /*if (*locationAddress != NULL) {
    std::stringstream keyStream;
    keyStream << routineNo << "_" << offset << "_" << operandSize;
    std::string key = keyStream.str();

    if (exactVariableNameMap.count(key) == 0) {
      WINDOWS::SYMBOL_INFO *symInfoObj = NULL;
      std::string fqn = (*locationAddress)->name;
      getExactVariableName((*locationAddress)->typeIndex,
                           (*locationAddress)->offset, offset, &fqn,
                           &symInfoObj, operandSize);

      if (symInfoObj != NULL) {
        LocalVariable *locVar = new LocalVariable;
        *locVar = **locationAddress;
        locVar->typeIndex = symInfoObj->TypeIndex;
        locVar->name = fqn;
        locVar->index = (--indexCount);
        locVar->variableSize = symInfoObj->Size;
        locVar->firstTime = true;
        exactVariableNameMap[key] = reinterpret_cast<void *>(locVar);
        *locationAddress =
            reinterpret_cast<LocalVariable *>(exactVariableNameMap[key]);

        delete symInfoObj;
      }
    } else {
      *locationAddress =
          reinterpret_cast<LocalVariable *>(exactVariableNameMap[key]);
    }
  }*/
}

void DebugHelp::resolveGlobalVariable(S_ADDRUINT address,
                                      GlobalDynamicVariable **locationAddress,
                                      S_ADDRUINT operandSize,
                                      S_ADDRUINT imageId)
{
  variableList->globalVariableListSearch(address, locationAddress, imageId);

  /*if (*locationAddress != NULL) {
    std::stringstream keyStream;
    keyStream << imageId << "_" << address << "_" << operandSize;
    std::string key = keyStream.str();

    if (exactVariableNameMap.count(key) == 0) {
      WINDOWS::SYMBOL_INFO *symInfoObj = NULL;
      std::string fqn = (*locationAddress)->name;
      getExactVariableName((*locationAddress)->typeIndex,
                           (*locationAddress)->address, address, &fqn,
                           &symInfoObj, operandSize);

      if (symInfoObj != NULL) {
        GlobalDynamicVariable *globalVar = new GlobalDynamicVariable;
        *globalVar = **locationAddress;
        globalVar->typeIndex = symInfoObj->TypeIndex;
        globalVar->name = fqn;
        globalVar->index = (--indexCount);
        globalVar->variableSize = symInfoObj->Size;
        globalVar->firstTime = true;
        exactVariableNameMap[key] = reinterpret_cast<void *>(globalVar);
        *locationAddress = reinterpret_cast<GlobalDynamicVariable *>(
            exactVariableNameMap[key]);

        delete symInfoObj;
      }
    } else {
      *locationAddress =
          reinterpret_cast<GlobalDynamicVariable *>(exactVariableNameMap[key]);
    }
  }*/
}

void DebugHelp::freeExactlyNamedVariables()
{
  for (auto it = exactVariableNameMap.begin(); it != exactVariableNameMap.end(); ++it)
    free(it->second);
}

void DebugHelp::freeExactlyNamedVariablesMap() { exactVariableNameMap.clear(); }

void DebugHelp::resolveGlobalVariableExactName(S_ADDRUINT imageId,
                                               S_ADDRUINT address,
                                               S_ADDRUINT accessed_size,
                                               GlobalDynamicVariable *globalDynamicVariable,
                                               S_ADDRUINT *varSize,
                                               bool *processRef,
                                               std::string *childIndex,
                                               std::string *childName)
{
  *processRef = false;
  std::string fullyQualifiedName;
  S_ADDRUINT lastOffset;

  if (!(variableList->getExactUDTMemberName((address - globalDynamicVariable->address),
                                            accessed_size,
                                            globalDynamicVariable->moreUDTInfo,
                                            &fullyQualifiedName,
                                            varSize,
                                            &lastOffset))) {
    return;
  }

  std::stringstream keyStream;
  keyStream << "G" << imageId << "_" << (address - lastOffset) << "_" << *varSize;
  std::string key = keyStream.str();

  PIN_RWMutexWriteLock(&rwMutexExactVariableMap);
  if (exactVariableNameMap.count(key) == 0) {
    exactVariableNameMap[key] = reinterpret_cast<void *>(new ChildVariable(
      fullyQualifiedName, reinterpret_cast<void *>(globalDynamicVariable), *varSize));
    *processRef               = true;
  }
  PIN_RWMutexUnlock(&rwMutexExactVariableMap);

  globalDynamicVariable = reinterpret_cast<GlobalDynamicVariable *>(
    (reinterpret_cast<ChildVariable *>(exactVariableNameMap[key])->parentVariable));
  *childName = reinterpret_cast<ChildVariable *>(exactVariableNameMap[key])->variableName;
  *varSize   = reinterpret_cast<ChildVariable *>(exactVariableNameMap[key])->varSize;
  std::stringstream childIndexStream;
  childIndexStream << "_" << (address - lastOffset - globalDynamicVariable->address) << "_"
                   << *varSize;
  (*childIndex).append(childIndexStream.str());
}

void DebugHelp::resolveLocalVariableExactName(Function::Id_t routineNo,
                                              int offset,
                                              S_ADDRUINT accessed_size,
                                              LocalVariable *localVariable,
                                              S_ADDRUINT *varSize,
                                              bool *processRef,
                                              std::string *childIndex,
                                              std::string *childName)
{
  *processRef = false;
  std::string fullyQualifiedName;
  S_ADDRUINT lastOffset;

  if (!(variableList->getExactUDTMemberName(abs((offset - localVariable->offset)),
                                            accessed_size,
                                            localVariable->moreUDTInfo,
                                            &fullyQualifiedName,
                                            varSize,
                                            &lastOffset))) {
    return;
  }

  std::stringstream keyStream;
  keyStream << "L" << routineNo << "_" << (offset - lastOffset) << "_" << *varSize;
  std::string key = keyStream.str();

  PIN_RWMutexWriteLock(&rwMutexExactVariableMap);
  if (exactVariableNameMap.count(key) == 0) {
    exactVariableNameMap[key] = reinterpret_cast<void *>(
      new ChildVariable(fullyQualifiedName, reinterpret_cast<void *>(localVariable), *varSize));
    *processRef = true;
  }
  PIN_RWMutexUnlock(&rwMutexExactVariableMap);

  localVariable = reinterpret_cast<LocalVariable *>(
    (reinterpret_cast<ChildVariable *>(exactVariableNameMap[key])->parentVariable));
  *childName = reinterpret_cast<ChildVariable *>(exactVariableNameMap[key])->variableName;
  *varSize   = reinterpret_cast<ChildVariable *>(exactVariableNameMap[key])->varSize;
  std::stringstream childIndexStream;
  childIndexStream << "_" << (offset - lastOffset - localVariable->offset) << "_" << *varSize;
  (*childIndex).append(childIndexStream.str());
}

void DebugHelp::resolveVariable(S_ADDRUINT address,
                                S_ADDRUINT accessed_size,
                                S_ADDRUINT ip,
                                S_ADDRUINT left_ebp,
                                S_ADDRUINT right_ebp,
                                S_ADDRUINT imageId,
                                Function::Id_t left_routineNo,
                                Function::Id_t right_routineNo,
                                Function::Id_t *foundAtRoutineNo,
                                S_ADDRUINT *foundAtImageId,
                                Reference::Type *variableType,
                                std::string *varName,
                                S_ADDRUINT *index,
                                S_ADDRUINT *allocatorIP,
                                bool *firstTime,
                                uint64_t *time,
                                S_ADDRUINT *variableSize,
                                std::string *childIndex)
{
  this->routineNo = routineNo;
#ifdef TRACING_LEVEL2
  BOOST_LOG_TRIVIAL(debug) << "[Debug][Variable][Search] address:" << address
                           << " ip:" << ip /*<< " ebp:" << ebp*/
                           << " routineNo:" << routineNo;
#endif

  S_ADDRUINT varSize;
  GlobalDynamicVariable *globalDynamicVariable = NULL;
  bool globalOrDynamicVariableFound            = false;
  std::string childName                        = "";
  bool processRef                              = false;
  *offset = 0;  // todo: implement offset handling for member variables / arrays

  if (variableList->dynamicVariableListSearch(&globalDynamicVariable, address)) {
    globalOrDynamicVariableFound = true;
  } else if (variableList->globalVariableListSearch(
               &globalDynamicVariable, address, foundAtImageId)) {
    globalOrDynamicVariableFound = true;
    varSize                      = globalDynamicVariable->variableSize;
    if (globalDynamicVariable->moreUDTInfo != NULL) {
      resolveGlobalVariableExactName(*foundAtImageId,
                                     address,
                                     accessed_size,
                                     globalDynamicVariable,
                                     &varSize,
                                     &processRef,
                                     childIndex,
                                     &childName);
    }
  }

  if (globalOrDynamicVariableFound) {
    if (globalDynamicVariable->variableType == Reference::Type::HEAP) {
      *variableType = Reference::Type::HEAP;
      std::stringstream dynamicVarName;
      dynamicVarName << globalDynamicVariable->address;
      *varName      = dynamicVarName.str();
      *allocatorIP  = globalDynamicVariable->allocatorIP;
      *firstTime    = globalDynamicVariable->firstTime;
      *time         = globalDynamicVariable->allocatedTime;
      *variableSize = globalDynamicVariable->variableSize;
#ifdef TRACING_LEVEL2
      BOOST_LOG_TRIVIAL(debug) << "[Debug][Variable][Resolved] Found dynamic variable: varName"
                               << *varName;
#endif
    } else if (globalDynamicVariable->variableType == Reference::Type::GLOBAL ||
               globalDynamicVariable->variableType == Reference::Type::STATICVAR) {
      *variableType = globalDynamicVariable->variableType;
      *varName      = (globalDynamicVariable->name);
      (*varName).append(childName);
      *allocatorIP  = globalDynamicVariable->allocatorIP;
      *index        = globalDynamicVariable->index;
      *firstTime    = globalDynamicVariable->firstTime || processRef;
      *variableSize = varSize;
#ifdef TRACING_LEVEL2
      BOOST_LOG_TRIVIAL(debug) << "[Debug][Variable][Resolved] Found global variable: varName"
                               << *varName;
#endif
    }

  } else {
    LocalVariable *localVariable = NULL;
    int offset;
    *foundAtRoutineNo = right_routineNo;
    offset            = address - right_ebp;
    if ((!(variableList->localVariableMapSearch(
          right_routineNo, &localVariable, address, right_ebp))) &&
        left_routineNo != 0) {
      variableList->localVariableMapSearch(left_routineNo, &localVariable, address, left_ebp);
      *foundAtRoutineNo = left_routineNo;
      offset            = address - left_ebp;
    }

    if (localVariable) {
      varSize = localVariable->variableSize;
      if (localVariable->moreUDTInfo != NULL) {
        resolveLocalVariableExactName(*foundAtRoutineNo,
                                      offset,
                                      accessed_size,
                                      localVariable,
                                      &varSize,
                                      &processRef,
                                      childIndex,
                                      &childName);
      }
    }

    if (localVariable) {
      *variableType = Reference::Type::STACK;
      *varName      = (localVariable->name);
      (*varName).append(childName);
      *index          = localVariable->index;
      *firstTime      = localVariable->firstTime || processRef;
      *variableSize   = varSize;
      *foundAtImageId = localVariable->imageID;
#ifdef TRACING_LEVEL2
      BOOST_LOG_TRIVIAL(debug) << "[Debug][Variable][Resolved] Found local variable: varName"
                               << *varName;
#endif

    } else {
      *variableType = Reference::Type::UNKNOWN;
#ifdef TRACING_LEVEL2
      BOOST_LOG_TRIVIAL(debug) << "[Debug][Variable][Unknown]" << routineNo << ""
                               << address /*<< " " << ebp*/;
      variableList->printDynamicVariableList();
      variableList->printLocalVariableList(routineNo);
      variableList->printGlobalVariableLists();

#endif
    }
  }
}

int DebugHelp::getNoOfNamespaces(const std::string &signature, namespace_t *nmsp)
{
  return 0;  // not implemented
}

bool DebugHelp::insertDynamicAllocatedVariable(S_ADDRUINT address,
                                               S_ADDRUINT variableSize,
                                               S_ADDRUINT allocatorIP,
                                               Reference::Id_t refId,
                                               uint64_t allocatedTime)
{
  GlobalDynamicVariable dynamicVariable;
  dynamicVariable.address       = address;
  dynamicVariable.allocatorIP   = allocatorIP;
  dynamicVariable.variableSize  = variableSize;
  dynamicVariable.variableType  = Reference::Type::HEAP;
  dynamicVariable.allocatedTime = allocatedTime;
  dynamicVariable.firstTime     = true;
  this->variableList->dynamicListInsert(dynamicVariable);
  return true;
}

bool DebugHelp::removeDynamicAllocatedVariable(S_ADDRUINT allocatedAddress)
{
  this->variableList->dynamicVariableDelete(allocatedAddress);
  return true;
}

void DebugHelp::initialSymbols()
{
  WINDOWS::SymInitialize(WINDOWS::GetCurrentProcess(), NULL, FALSE);
}

void DebugHelp::unloadModules()
{
  for (modBaseMapIterator = modBaseMap.begin(); modBaseMapIterator != modBaseMap.end();
       modBaseMapIterator++) {
    WINDOWS::SymUnloadModule(WINDOWS::GetCurrentProcess(), (modBaseMapIterator->first));
  }
  WINDOWS::SymCleanup(WINDOWS::GetCurrentProcess());
}

}  // namespace pcv

#endif
