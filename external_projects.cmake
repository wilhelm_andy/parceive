
if (MSVC)
    # ----------------------------------------- ZLIB ------------------------------------------------- #
    ExternalProject_Add(zlib
        SOURCE_DIR ${CMAKE_CURRENT_BINARY_DIR}/libz
        BUILD_IN_SOURCE 1
        URL ${ZLIB_URL}
        BUILD_COMMAND cmake --build . --config Release
        BUILD_IN_SOURCE 1
        INSTALL_COMMAND ""
        LOG_DOWNLOAD 1
        LOG_CONFIGURE 1
        LOG_BUILD 1)
    include_directories(${CMAKE_CURRENT_BINARY_DIR}/libz)
    link_directories(${CMAKE_CURRENT_BINARY_DIR}/libz/Release)
else()
    # ----------------------------------------- Libdwarf ------------------------------------------------- #
    set(CONFIG_WRAPPER ${CMAKE_CURRENT_LIST_DIR}/runconfigure.sh)
    configure_file(${CMAKE_CURRENT_LIST_DIR}/runconfigure.sh.in ${CONFIG_WRAPPER} @ONLY)
    ExternalProject_Add(libdwarf
        SOURCE_DIR ${CMAKE_CURRENT_BINARY_DIR}/libdwarf
        BUILD_IN_SOURCE 1
        URL ${DWARF_URL}
        CONFIGURE_COMMAND sh ${CONFIG_WRAPPER} ${CMAKE_CURRENT_BINARY_DIR}/libdwarf/configure ${CMAKE_C_FLAGS}
            ${CMAKE_CXX_FLAGS} ${LD_LIBDWARF_FLAGS} ""
        BUILD_COMMAND ${MAKE}
        BUILD_IN_SOURCE 1
        INSTALL_COMMAND ""
        LOG_DOWNLOAD 1
        LOG_CONFIGURE 1
        LOG_BUILD 1)
    include_directories(${CMAKE_CURRENT_BINARY_DIR}/libdwarf/libdwarf)
    link_directories(${CMAKE_CURRENT_BINARY_DIR}/libdwarf/libdwarf)
    # ----------------------------------------- Libelf ------------------------------------------------- #
    ExternalProject_Add(libelf
        SOURCE_DIR ${CMAKE_CURRENT_BINARY_DIR}/libelf
        BUILD_IN_SOURCE 1
        URL ${ELF_URL}
        CONFIGURE_COMMAND sh ${CONFIG_WRAPPER} "${CMAKE_CURRENT_BINARY_DIR}/libelf/configure --disable-shared ${ELF_FLAG}"
            ${CMAKE_C_FLAGS} ${CMAKE_CXX_FLAGS} "" ""
        BUILD_COMMAND ${MAKE}
        BUILD_IN_SOURCE 1
        INSTALL_COMMAND ""
        LOG_DOWNLOAD 1
        LOG_CONFIGURE 1
        LOG_BUILD 1)
    include_directories(${CMAKE_CURRENT_BINARY_DIR}/libelf/lib)
    link_directories(${CMAKE_CURRENT_BINARY_DIR}/libelf/lib)
    # ----------------------------------------- ZLIB ------------------------------------------------- #
    ExternalProject_Add(zlib
        SOURCE_DIR ${CMAKE_CURRENT_BINARY_DIR}/libz
        BUILD_IN_SOURCE 1
        URL ${ZLIB_URL}
        CONFIGURE_COMMAND sh ${CONFIG_WRAPPER} "${CMAKE_CURRENT_BINARY_DIR}/libz/configure --static" ${CMAKE_C_FLAGS} ${CMAKE_CXX_FLAGS} "" ""
        BUILD_COMMAND ${MAKE}
        BUILD_IN_SOURCE 1
        INSTALL_COMMAND ""
        LOG_DOWNLOAD 1
        LOG_CONFIGURE 1
        LOG_BUILD 1)
    include_directories(${CMAKE_CURRENT_BINARY_DIR}/libz)
    link_directories(${CMAKE_CURRENT_BINARY_DIR}/libz)
endif()