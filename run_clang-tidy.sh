#!/bin/bash

SCRIPT_DIR="$( cd "$(dirname "$0")" ; pwd -P )"
COMPILE_COMMANDS=$1
COMPILE_COMMANDS_DIR="$( cd "$(dirname "$1")" ; pwd -P )"

if ! [ -x "$(command -v clang-tidy)" ]; then
	echo 'Error: clang-tidy is not installed. aborting.' >&2
	exit 1
fi

if ! [ -x "$(command -v run-clang-tidy.py)" ]; then
	echo 'Error: run-clang-tidy.py was not found. aborting.' >&2
	exit 1
fi

if ! [ -f $COMPILE_COMMANDS ]; then
	echo 'Error: compile_commands.json was not found. aborting.'
	exit 1
fi

pushd $COMPILE_COMMANDS_DIR
run-clang-tidy.py -header-filter=.* -checks='cppcoreguidelines-*,modernize-*,misc-*,performance-*,readability-*,google-*'
popd
