#include "DbgCollector.h"

#include <string>
#include <unordered_set>

#include "Common.h"
#include "DbgFunction.h"
#include "DbgHelpApi.h"
#include "DbgType.h"
#include "DbgUtil.h"
#include "DbgVariable.h"

using std::string;
using std::unordered_set;

// #define PRINT_DEBUG

#ifdef PRINT_DEBUG
#define DBGHELP_LOG(msg) printf(msg)                     // NOLINT(runtime/printf)
#define DBGHELP_LOGP(msg, ...) printf(msg, __VA_ARGS__)  // NOLINT(runtime/printf)
#else
#define DBGHELP_LOG(msg)
#define DBGHELP_LOGP(msg, ...)
#endif

namespace pcv {
namespace dbghelp {

static BOOL CALLBACK type_enum_func(_In_ PSYMBOL_INFO pSymInfo,
                                    _In_ ULONG SymbolSize,
                                    _In_opt_ PVOID UserContext)
{
  auto coll = static_cast<DbgCollector*>(UserContext);
  string fileName;
  uint32_t line;
  if (coll->getCodeLocation(pSymInfo->Address, fileName, line)) {
    if (!coll->isSourceLocationIncluded(fileName, line)) {
      return TRUE;
    }
  }
  enum SymTagEnum tag = static_cast<enum SymTagEnum>(pSymInfo->Tag);

  switch (tag) {
    case SymTagTypedef:
      coll->symbols.insert(pSymInfo->Name);
      DBGHELP_LOG("typedef   ");
      break;
    case SymTagUDT: {
      auto it = coll->map_name_to_type.find(pSymInfo->Name);
      if (it == coll->map_name_to_type.end()) {
        const auto type = new DbgType(
          pSymInfo->Name, fileName, pSymInfo->Index, pSymInfo->SizeOfStruct, UdtKind::UdtNone);
        coll->types.push_back(type);
        coll->map_index_to_type[pSymInfo->TypeIndex] = type;
        coll->map_name_to_type[pSymInfo->Name]       = type;
        coll->symbols.insert(pSymInfo->Name);
        DBGHELP_LOG("UDT       ");
      } else {
        coll->map_index_to_type[pSymInfo->TypeIndex] = it->second;
        DBGHELP_LOG("duplicate ");
      }
    } break;
    case SymTagEnum:
      coll->symbols.insert(pSymInfo->Name);
      DBGHELP_LOG("enum      ");
      break;
    default:
      DBGHELP_LOG("other     ");
      break;
  }
  DBGHELP_LOGP("%s\n", pSymInfo->Name);
  return TRUE;
}

static bool getTypeName(void* procHandle,
                        unsigned long long modBase,
                        ULONG typeIdx,
                        std::string& typeName)
{
  WCHAR* wname;
  char name[1024];
  const auto success = DbgSymGetTypeInfo(procHandle, modBase, typeIdx, TI_GET_SYMNAME, &wname);
  if (success) {
    size_t convertedChars;
    if (0 == wcstombs_s(&convertedChars, name, wname, 1024)) {
      typeName = name;
    }
    LocalFree(wname);
    return true;
  }
  return false;
}

static BOOL CALLBACK local_enum_func(_In_ PSYMBOL_INFO pSymInfo,
                                     _In_ ULONG SymbolSize,
                                     _In_opt_ PVOID UserContext)
{
  auto coll = static_cast<DbgCollector*>(UserContext);
  string fileName;
  uint32_t line;
  if (coll->getCodeLocation(pSymInfo->Address, fileName, line)) {
    if (!coll->isSourceLocationIncluded(fileName, line)) {
      return TRUE;
    }
  }

  DbgFunction* func   = coll->functionContext;
  enum SymTagEnum tag = static_cast<enum SymTagEnum>(pSymInfo->Tag);
  auto flags          = pSymInfo->Flags;
  switch (tag) {
    case SymTagData: {
      if (contains(func->collectedLocalIndices, pSymInfo->Index)) {
        return TRUE;  // already found the local
      }

      for (const auto& local : func->locals) {
        if (local->name == pSymInfo->Name) {
          // the local is already collected. This can only happen when a local with the same name is
          // reintroduced with a new sym info index. Experience has shown that it can happen when
          // run in the context of a pin tool.
          return TRUE;
        }
      }

      DWORD tmp;
      DbgSymGetTypeInfo(func->process, pSymInfo->ModBase, pSymInfo->Index, TI_GET_DATAKIND, &tmp);
      DataKind dk    = (DataKind)tmp;
      AddressKind ak = AddressIsAbsolute;
      if (flags & SYMFLAG_FRAMEREL) {
        ak = AddressIsFrameRelative;
      } else if (flags & SYMFLAG_REGREL) {
        ak = AddressIsRegisterRelative;
      }
      auto address = pSymInfo->Address;
      if (ak == AddressIsAbsolute) {
        address -= pSymInfo->ModBase;
      }
      const auto local = new DbgVariable(pSymInfo->Name,
                                         fileName,
                                         pSymInfo->Index,
                                         pSymInfo->TypeIndex,
                                         address,
                                         pSymInfo->Size,
                                         dk,
                                         ak);

      // try to find the local's type either via type index or lexicographically. The latter is
      // necessary as experience has shown that we might encounter novel type ids pointing to the
      // same data on Windows 7
      local->type = getValueOrNullptr(coll->map_index_to_type, (size_t)pSymInfo->TypeIndex);
      if (local->type == nullptr) {
        std::string typeName;
        if (getTypeName(coll->processHandle, pSymInfo->ModBase, pSymInfo->TypeIndex, typeName)) {
          local->type = getValueOrNullptr(coll->map_name_to_type, typeName);
          if (local->type != nullptr) {
            coll->map_index_to_type[local->type_index] = local->type;
          }
        }
      }

      func->locals.push_back(local);
      func->collectedLocalIndices.emplace(pSymInfo->Index);

      switch (dk) {
        case DataIsObjectPtr:
          DBGHELP_LOGP("  objptr    0x%016llx %8u ", pSymInfo->Address, pSymInfo->Size);
          break;
        case DataIsParam:
          DBGHELP_LOGP("  parameter 0x%016llx %8u ", pSymInfo->Address, pSymInfo->Size);
          break;
        case DataIsLocal:
          DBGHELP_LOGP("  local     0x%016llx %8u ", pSymInfo->Address, pSymInfo->Size);
          break;
        case DataIsStaticLocal: {
          DBGHELP_LOGP("  slocal    0x%016llx %8u ", pSymInfo->Address, pSymInfo->Size);

          //// we need to copy the local informtion in order to avoid double frees later on
          local->isStatic      = true;
          const auto localCopy = new DbgVariable(*local);
          coll->globalVariables.push_back(localCopy);
          coll->map_name_to_variable[pSymInfo->Name] = localCopy;
          coll->symbols.insert(pSymInfo->Name);
        } break;
        case DataIsFileStatic:
          DBGHELP_LOGP("  sfile     0x%016llx %8u ", pSymInfo->Address, pSymInfo->Size);
          break;
        default:
          DBGHELP_LOG("  data      ");
          break;
      }
    } break;
    default:
      DBGHELP_LOG("  other     ");
      break;
  }
  DBGHELP_LOGP("%s\n", pSymInfo->Name);
  return TRUE;
}

static bool isConstructor(const string& functionName)
{
  auto parts          = splitName(functionName);
  const auto numNames = parts.size();
  return (numNames >= 2) && (parts[numNames - 2] == parts[numNames - 1]);
}

static BOOL CALLBACK global_enum_func(_In_ PSYMBOL_INFO pSymInfo,
                                      _In_ ULONG SymbolSize,
                                      _In_opt_ PVOID UserContext)
{
  auto coll = static_cast<DbgCollector*>(UserContext);
  string fileName;
  uint32_t line;
  if (coll->getCodeLocation(pSymInfo->Address, fileName, line)) {
    if (!coll->isSourceLocationIncluded(fileName, line)) {
      return TRUE;
    }
  }
  enum SymTagEnum tag = static_cast<enum SymTagEnum>(pSymInfo->Tag);

  switch (tag) {
    case SymTagFunction: {
      const bool functionIsConstructor = isConstructor(pSymInfo->Name);
      const auto func                  = new DbgFunction(pSymInfo->Name,
                                        fileName,
                                        pSymInfo->Index,
                                        pSymInfo->TypeIndex,
                                        pSymInfo->Address,
                                        pSymInfo->Size,
                                        pSymInfo->Flags,
                                        functionIsConstructor);
      func->process                    = coll->processHandle;
      coll->functions.push_back(func);
      coll->map_name_to_function[pSymInfo->Name] = func;
      coll->symbols.insert(pSymInfo->Name);
      DBGHELP_LOGP("function  0x%016llx %8u ", pSymInfo->Address, pSymInfo->Size);
    } break;
    case SymTagData: {
      DWORD tmp;
      DbgSymGetTypeInfo(
        coll->processHandle, pSymInfo->ModBase, pSymInfo->Index, TI_GET_DATAKIND, &tmp);
      const auto dk  = static_cast<DataKind>(tmp);
      const auto var = new DbgVariable(pSymInfo->Name,
                                       fileName,
                                       pSymInfo->Index,
                                       pSymInfo->TypeIndex,
                                       pSymInfo->Address - pSymInfo->ModBase,
                                       pSymInfo->Size,
                                       dk,
                                       AddressIsAbsolute);
      auto type      = coll->getType(pSymInfo->TypeIndex);
      if (type != nullptr) {
        var->type = type;
      }
      coll->globalVariables.push_back(var);
      coll->map_name_to_variable[pSymInfo->Name] = var;
      coll->symbols.insert(pSymInfo->Name);

      switch (dk) {
        case DataIsGlobal:
          DBGHELP_LOGP("global    0x%016llx %8u ", pSymInfo->Address, pSymInfo->Size);
          break;
        case DataIsFileStatic:
          DBGHELP_LOGP("sfile     0x%016llx %8u ", pSymInfo->Address, pSymInfo->Size);
          break;
        default:
          DBGHELP_LOG("data      ");
          break;
      }
    } break;
    case SymTagPublicSymbol:
      DBGHELP_LOG("public    ");
      break;
    default:
      DBGHELP_LOG("other     ");
      break;
  }
  DBGHELP_LOGP("%s\n", pSymInfo->Name);
  return TRUE;
}

BOOL CALLBACK line_enum_func(_In_ const PSRCCODEINFO LineInfo, _In_ const PVOID UserContext)
{
  auto coll         = static_cast<DbgCollector*>(UserContext);
  const auto offset = coll->getLoadDirectives().linkToLoadtimeAddressOffset;

  coll->map_address_to_context[LineInfo->Address + offset] = {LineInfo->FileName,
                                                              LineInfo->LineNumber};
  return TRUE;
}

void collectType(const DbgCollector& coll, DbgType* type, void* proc, DWORD64 modbase)
{
  BOOL err;
  DWORD childcount;
  err = DbgSymGetTypeInfo(proc, modbase, type->index, TI_GET_CHILDRENCOUNT, &childcount);

  if (childcount != 0) {
    const size_t childs_size = sizeof(TI_FINDCHILDREN_PARAMS) + sizeof(ULONG) * (childcount);
    const auto children      = reinterpret_cast<TI_FINDCHILDREN_PARAMS*>(alloca(childs_size));
    memset(children, 0, childs_size);
    children->Count = childcount;
    err             = DbgSymGetTypeInfo(proc, modbase, type->index, TI_FINDCHILDREN, children);

    DBGHELP_LOGP("members of type %s\n", type->name.c_str());

    for (unsigned ii = 0; ii < childcount; ii++) {
      const ULONG childIdx = children->ChildId[ii];

      string name;
      if (!getTypeName(proc, modbase, childIdx, name)) {
        continue;
      }
      if (contains(type->insertedMemberIndices, childIdx)) {
        continue;
      }

      ULONG ultag;
      err            = DbgSymGetTypeInfo(proc, modbase, childIdx, TI_GET_SYMTAG, &ultag);
      const auto tag = static_cast<enum SymTagEnum>(ultag);

      ULONG64 addr;
      err = DbgSymGetTypeInfo(proc, modbase, childIdx, TI_GET_ADDRESS, &addr);

      DWORD ofs;
      err = DbgSymGetTypeInfo(proc, modbase, childIdx, TI_GET_OFFSET, &ofs);

      DWORD typeindex;
      err = DbgSymGetTypeInfo(proc, modbase, childIdx, TI_GET_TYPE, &typeindex);

      LONG64 len;
      err = DbgSymGetTypeInfo(proc, modbase, typeindex, TI_GET_LENGTH, &len);

      DWORD tmp;
      err            = DbgSymGetTypeInfo(proc, modbase, childIdx, TI_GET_DATAKIND, &tmp);
      const auto dk  = DataKind(tmp);
      AddressKind ak = AddressIsAbsolute;
      if (dk == DataIsMember) {
        addr = ULONG64(ofs);
        ak   = AddressIsStructOffset;
      }

      switch (tag) {
        case SymTagFunction: {
          // we're seeing a difference in behavior between win7 and win10:
          // name may either be fully qualified (win7) or not (win10).
          const string prefix = type->full_name + "::";
          const string full_name(startsWith(name, prefix) ? name : prefix + name);

          auto method = getValueOrNullptr(coll.map_name_to_function, full_name);
          // Since we're resolving functions by full name, we currently may get overload collisions.
          // Thus we have to guard against double insertions of the same overload to guarantee the
          // integrity of the data regarding id uniqueness
          if (method != nullptr && !contains(type->insertedMemberIndices, method->index)) {
            type->methods.push_back(method);
            method->method_of = type;
            type->insertedMemberIndices.emplace(method->index);
            DBGHELP_LOGP("  method    0x%016llx %8u ", addr, len);
          } else {
            DBGHELP_LOG("  ignored   ");
          }
        } break;
        case SymTagData: {
          const string full_name = type->full_name + "::" + name;
          auto var =
            new DbgVariable(full_name, type->file_name, childIdx, typeindex, addr, len, dk, ak);
          var->ownerType = type;

          const auto varType = coll.getType(typeindex);
          if (varType != nullptr) {
            var->type = varType;
          }
          type->members.push_back(var);
          type->insertedMemberIndices.emplace(var->index);
        }
          DBGHELP_LOGP("  member    0x%016llx %8u ", addr, len);
          break;
        case SymTagBaseClass: {
          auto baseClass = coll.getType(typeindex);
          if (baseClass != nullptr) {
            type->baseclasses.push_back(baseClass);
            baseClass->superclasses.push_back(type);
          }
        }
          DBGHELP_LOG("  baseclass ");
          break;
        case SymTagUDT:
          DBGHELP_LOG("  UDT       ");
          break;
        case SymTagEnum:
          DBGHELP_LOG("  enum      ");
          break;
        case SymTagTypedef:
          DBGHELP_LOG("  typedef   ");
          break;
        case SymTagVTable:
          DBGHELP_LOG("  vtable    ");
          break;
        default:
          DBGHELP_LOG("  other     ");
          break;
      }

      DBGHELP_LOGP("%s\n", name);
    }
  }

  DWORD tmp;
  err            = DbgSymGetTypeInfo(proc, modbase, type->index, TI_GET_UDTKIND, &tmp);
  UdtKind uk     = (UdtKind)tmp;
  type->udt_kind = uk;
}

DbgCollector::~DbgCollector() { resetCollectorState(); }

void DbgCollector::resetCollectorState()
{
  deleteContainerEntries(types);
  deleteContainerEntries(globalVariables);
  deleteContainerEntries(functions);
  map_index_to_type.clear();
  map_name_to_type.clear();
  map_name_to_function.clear();
  map_name_to_variable.clear();
  symbols.clear();
  processHandle   = nullptr;
  functionContext = nullptr;
}

void DbgCollector::collectLocals()
{
  IMAGEHLP_STACK_FRAME stk;

  for (auto func : this->functions) {
    DBGHELP_LOGP("locals for function %s\n", func->name.c_str());
    functionContext = func;

    memset(&stk, 0, sizeof(stk));
    const size_t scanRange = directives_.deepScan ? func->size : 1;
    BOOL rv;
    for (ULONG64 offset = func->offset; offset < func->offset + scanRange; ++offset) {
      stk.InstructionOffset = offset;
      rv                    = DbgSymSetContext(processHandle, &stk, NULL);
      rv                    = DbgSymEnumSymbols(processHandle, 0, NULL, local_enum_func, this);
    }

    if (getCodeLocation(func->offset, func->file_name, func->line)) {
      DBGHELP_LOGP("  line %4u in file %s\n", func->line, func->file->name.c_str());
    }
  }

  // restore context
  memset(&stk, 0, sizeof(stk));
  BOOL rv = DbgSymSetContext(processHandle, &stk, NULL);
}

bool DbgCollector::loadSymbols(const string& fileName, const SymbolLoadDirectives& directives)
{
  directives_ = directives;
  bool res    = true;
  BOOL rv;

  resetCollectorState();

  DBGHELP_LOG("loading dbghelp\n");

  if (loadDbgHelpApi()) {
    HANDLE proc = GetCurrentProcess();
    rv          = DbgSymInitialize(proc, NULL, FALSE);

    auto opts    = DbgSymGetOptions();
    auto newOpts = (opts | SYMOPT_LOAD_LINES | SYMOPT_EXACT_SYMBOLS) & ~SYMOPT_UNDNAME;
    DbgSymSetOptions(newOpts);

    DBGHELP_LOGP("%s\n", file_name.c_str());

    HMODULE mod     = GetModuleHandleA(fileName.c_str());
    bool freeModule = false;
    if (0 == mod) {
      mod        = LoadLibrary(fileName.c_str());
      freeModule = (mod != nullptr);
    }

    const auto modbase = DbgSymLoadModule(proc, NULL, fileName.c_str(), NULL, (DWORD64)mod, 0);
    if (modbase == 0) {
      res = false;
    } else {
      this->processHandle = proc;
      res &= (TRUE == DbgSymEnumTypesByName(proc, modbase, "*", type_enum_func, this));
      res &= (TRUE == DbgSymEnumSymbols(proc, modbase, "*", global_enum_func, this));
      res &= (TRUE == DbgSymEnumLines(proc, modbase, nullptr, nullptr, line_enum_func, this));

      // collect type methods and members
      for (auto type : this->types) {
        collectType(*this, type, proc, modbase);
      }

      collectLocals();
      collectNamespaces();

      res &= (TRUE == DbgSymUnloadModule(proc, modbase));
    }

    res &= (TRUE == DbgSymCleanup(proc));

    if (freeModule) {
      FreeLibrary(mod);
    }

    loadedModuleFileName_ = fileName;
  } else {
    DBGHELP_LOG("Failed to load dbghelp\n");
    res = false;
  }

  unloadDbgHelpApi();
  return res;
}

bool DbgCollector::hasLoadedSymbols() const { return !loadedModuleFileName_.empty(); }

string DbgCollector::getLoadedModuleFileName() const { return loadedModuleFileName_; }

void DbgCollector::collectNamespaces()
{
  unordered_set<DbgNamespace*> namespaces;

  for (auto& symbolName : this->symbols) {
    auto name_parts        = splitName(symbolName);
    DbgNamespace* parentNs = &namespaceRoot;

    string symbolFragment;

    for (unsigned ii = 0; ii < name_parts.size() - 1; ii++) {
      const string part(name_parts[ii]);
      if (ii > 0) {
        symbolFragment += "::";
      }
      symbolFragment += part;

      auto ns           = parentNs->getChildOrNullptr(part);
      const bool isType = containsKey(map_name_to_type, symbolFragment);
      const bool isFunc = containsKey(map_name_to_function, symbolFragment);
      const bool isVar  = containsKey(map_name_to_variable, symbolFragment);

      if (isType || isFunc || isVar) {
        continue;
      }

      if (ns == nullptr) {
        ns                       = new DbgNamespace(part);
        parentNs->children[part] = ns;
        ns->parent               = parentNs;
        namespaces.insert(ns);
      }
      parentNs = ns;
    }

    auto type = getValueOrNullptr(map_name_to_type, symbolName);
    if (type != nullptr) {
      type->parentNamespace = parentNs;
      auto containingType   = getValueOrNullptr(map_name_to_type, symbolFragment);
      if (containingType != nullptr) {
        containingType->nestedTypes.push_back(type);
        type->nestingParent = containingType;
      }
    } else {
      const auto func = getValueOrNullptr(map_name_to_function, symbolName);
      if (func != nullptr) {
        func->owningNamespace = parentNs;
      }
    }
  }

  for (auto nsp : namespaces) {
    DBGHELP_LOGP("namespace %s\n", nsp->getFullName().c_str());
  }
}

bool DbgCollector::getCodeLocation(const uint64_t address, string& file, uint32_t& line) const
{
  auto it = map_address_to_context.upper_bound(address);
  if (it != map_address_to_context.begin()) {
    --it;
    file = it->second.fileName;
    line = it->second.line;
    return true;
  }
  return false;
}

DbgType* DbgCollector::getType(const size_t& index) const
{
  return getValueOrNullptr(map_index_to_type, index);
}

bool DbgCollector::isAddressIncluded(const uint64_t address) const
{
  if (filter_ != nullptr) {
    string fileName;
    uint32_t line;
    if (getCodeLocation(address, fileName, line)) {
      return isSourceLocationIncluded(fileName, line);
    }
  }
  return true;
}

bool DbgCollector::isSourceLocationIncluded(const std::string& fileName, uint32_t line) const
{
  return (filter_ == nullptr) ? true : filter_->isIncluded(fileName, line);
}

bool DbgCollector::canLoadSymbols(const string& fileName)
{
  bool res = true;

  if (loadDbgHelpApi()) {
    const HANDLE proc = GetCurrentProcess();
    DbgSymInitialize(proc, nullptr, FALSE);

    auto opts    = DbgSymGetOptions();
    auto newOpts = (opts | SYMOPT_LOAD_LINES | SYMOPT_EXACT_SYMBOLS) & ~SYMOPT_UNDNAME;
    DbgSymSetOptions(newOpts);

    HMODULE mod = GetModuleHandleA(fileName.c_str());
    const auto modBase =
      DbgSymLoadModule(proc, nullptr, fileName.c_str(), nullptr, (DWORD64)mod, 0);
    if (modBase == 0) {
      res = false;
    } else {
      DbgSymUnloadModule(proc, modBase);
    }
    DbgSymCleanup(proc);
  } else {
    res = false;
  }

  unloadDbgHelpApi();
  return res;
}
}  // namespace dbghelp
}  // namespace pcv
