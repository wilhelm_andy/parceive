#include "DbgHelpApi.h"

static HMODULE module = nullptr;

namespace pcv {
namespace dbghelp {
#define X(NAME) NAME##Proc Dbg##NAME = nullptr;
FUNCTIONS
#undef X

bool loadDbgHelpApi()
{
  module = LoadLibrary("dbghelp.dll");

#define X(NAME)                                          \
  Dbg##NAME = (NAME##Proc)GetProcAddress(module, #NAME); \
  if (nullptr == Dbg##NAME) {                            \
    OutputDebugString("Failed to loaded " #NAME ".\n");  \
    return false;                                        \
  }
  FUNCTIONS
#undef X

  return true;
}

void resetFunctionPointers()
{
#define X(NAME) Dbg##NAME = nullptr;
  FUNCTIONS
#undef X
}

void unloadDbgHelpApi()
{
  resetFunctionPointers();

  if (module != nullptr) {
    FreeLibrary(module);
    module = nullptr;
  }
}

}  // namespace dbghelp
}  // namespace pcv
