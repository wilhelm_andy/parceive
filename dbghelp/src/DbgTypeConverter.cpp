#if 0
#include "Logging.h"
#else
#define CHECK(A, ...)
#define INFO(...)
#endif
#include "DbgTypeConverter.h"

using namespace pcv;
using namespace pcv::dbghelp;
using namespace pcv::entity;
using namespace std;

DbgTypeConverter::DbgTypeConverter() : image_(nullptr), empty_namespace_(nullptr) {}

void DbgTypeConverter::convertDbgHelpTypes(const DbgCollector& coll,
                                           const pcv::Id_t associatedImageId)
{
  CHECK(coll.hasLoadedSymbols(), "The given DbgCollector instance has not loaded any symbols.");

  image_ = new Image(associatedImageId, coll.getLoadedModuleFileName());
  const string emptyNsName;
  Namespace* emptyNsParent = nullptr;
  empty_namespace_         = new Namespace(emptyNsName, emptyNsParent, image_);

  buildNamespaceConversionLookup(&coll.namespaceRoot, empty_namespace_);

  // first convert and store classes ..
  for (auto type : coll.types) {
    if (type->nestingParent != nullptr) {
      INFO("Ignoring type %s since it's contained in %s.",
           type->name.c_str(),
           type->containingType->name.c_str());
      continue;
    }
    convertType(type);
  }

  // .. and only then fill class members. This is done in a separate pass since
  // member types may depend on these types and not resolve fully otherwise
  for (auto type : coll.types) {
    fillClassMembers(type);
  }

  for (auto var : coll.globalVariables) {
    auto type = Variable::Type::GLOBAL;
    if (var->isStatic) {
      type |= Variable::Type::STATICVAR;
    }
    auto globalVar = createVariableFrom(var, type);
    global_vars_.emplace_back(globalVar);
  }

  for (auto func : coll.functions) {
    // if this is a method it's already stored
    if (!containsKey(routines_, func->full_name)) {
      const auto routine = createRoutineFrom(func);
      storeRoutine(routine, func->full_name);
    }
  }

  fillContext();
}

const Context& DbgTypeConverter::getContext() const { return ctxt_; }

void DbgTypeConverter::convertType(DbgType* type)
{
  auto classEntity                            = createClassFrom(type);
  index_to_class_[classEntity->getUniqueId()] = classEntity;
  name_to_class_[type->full_name]             = classEntity;

  for (auto nestedType : type->nestedTypes) {
    convertType(nestedType);

    auto nestedClass = name_to_class_[nestedType->full_name];
    nestedClass->cls = classEntity;
    classEntity->nestedClasses.push_back(nestedClass);
    nestedClass->nestingParent = classEntity;

    INFO("class %s is contained by %s", compositeClass->name.c_str(), classEntity->name.c_str());
    if (!classEntity->members.empty()) {
      INFO("members:");
      for (auto& member : classEntity->members) {
        INFO("\t%s", member->name.c_str());
      }
    }
  }
}

void DbgTypeConverter::fillClassMembers(DbgType* type)
{
  auto classEntity = name_to_class_[type->full_name];

  for (auto var : type->members) {
    auto member            = createVariableFrom(var, Variable::Type::UNKNOWN);
    member->declaringClass = classEntity;
    classEntity->members.push_back(member);
  }
  for (auto func : type->methods) {
    auto method = createRoutineFrom(func);
    classEntity->methods.push_back(method);
    method->cls = classEntity;
    storeRoutine(method, func->full_name);
  }
}

Class* DbgTypeConverter::createClassFrom(DbgType* type) const
{
  auto typeNs = lookupNamespace(type->parentNamespace);
  if (typeNs == nullptr) {
    INFO("Could not find pcv namespace %s for type %s\n!",
         type->parentNamespace->getFullName().c_str(),
         type->name.c_str());
    typeNs = empty_namespace_;
  }
  return new Class(type->index,
                   type->name,
                   image_,
                   typeNs,
                   nullptr,
                   new File(type->file_name, image_),
                   0,
                   type->size);
}

Variable* DbgTypeConverter::createVariableFrom(DbgVariable* var, const Variable::Type type) const
{
  Namespace* varNs = empty_namespace_;
  if (var->type != nullptr) {
    const auto typeNs = lookupNamespace(var->type->parentNamespace);
    if (typeNs == nullptr) {
      INFO("Could not find pcv namespace for variable %s of type %s!",
           var->name.c_str(),
           var->type->name.c_str());
    } else {
      varNs = typeNs;
    }
  }
  Class* varType;
  if (var->type != nullptr) {
    varType = getValueOrNullptr(name_to_class_, var->type->full_name);
  } else {
    const ImageScopedId typeId(image_->id, var->type_index);
    varType = getValueOrNullptr(index_to_class_, typeId);
  }

  // let take callers take care about membership and treat this as uncontained here
  Class* const ownerClassType = nullptr;
  return new Variable(static_cast<Id_t>(var->index),
                      var->full_name,
                      image_,
                      varNs,
                      varType,
                      new File(var->file_name, image_),
                      0,
                      var->size,
                      var->offset,
                      type,
                      ownerClassType);
}

Routine* DbgTypeConverter::createRoutineFrom(DbgFunction* func) const
{
  auto funcNs = lookupNamespace(func->owningNamespace);
  if (funcNs == nullptr) {
    funcNs = empty_namespace_;
  }

  auto routine           = new Routine(func->index,
                             func->name,
                             image_,
                             funcNs,
                             nullptr,
                             new File(func->file_name, image_),
                             func->line);
  routine->size          = func->size;
  routine->isConstructor = func->isConstructor;

  for (auto lvar : func->locals) {
    auto type = Variable::Type::STACK;
    if (lvar->isStatic) {
      type |= Variable::Type::STATICVAR;
    }
    const auto local        = createVariableFrom(lvar, type);
    local->declaringRoutine = routine;
    routine->locals.push_back(local);
  }
  return routine;
}

void DbgTypeConverter::storeRoutine(Routine* routine, const string& fullName)
{
  routines_[fullName] = routine;
  for (auto local : routine->locals) {
    if (local->type == Variable::Type::STATICVAR) {
      // ultimately, both locals and globals will end up in one context. We must not
      // store the same variable as both global and local since the variable id will
      // be the same. Static variable of course are also counted as global.
      continue;
    }
    locals_.emplace_back(local);
  }
  if (routine->cls != nullptr) {
    name_to_class_[fullName] = routine->cls;
  }
}

void DbgTypeConverter::fillContext()
{
  ctxt_.prepareForNewImage(image_->id);
  ctxt_.addImage(image_);
  for (const auto& kvp : namespace_conversion_lookup_) {
    ctxt_.addNamespace(kvp.second);
  }
  for (const auto& kvp : routines_) {
    ctxt_.addRoutine(kvp.second);
  }
  for (const auto& kvp : index_to_class_) {
    ctxt_.addClass(kvp.second);
  }
  for (const auto& var : global_vars_) {
    ctxt_.addVariable(var);
  }
  for (const auto& var : locals_) {
    ctxt_.addVariable(var);
  }
}

void DbgTypeConverter::buildNamespaceConversionLookup(const DbgNamespace* dbgParent,
                                                      Namespace* parent)
{
  namespace_conversion_lookup_[dbgParent] = parent;

  for (const auto pair : dbgParent->children) {
    const auto dbgChild = pair.second;
    const auto child    = new Namespace(dbgChild->name, parent, image_);
    parent->children.push_back(child);

    buildNamespaceConversionLookup(dbgChild, child);
  }
}

Namespace* DbgTypeConverter::lookupNamespace(const DbgNamespace* ns) const
{
  return getValueOrNullptr(namespace_conversion_lookup_, ns);
}