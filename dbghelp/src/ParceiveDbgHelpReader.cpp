#include <chrono>
#include <iostream>
#include <sstream>

#include "Common.h"
#include "Filter.h"
#include "ParceiveDbgHelpReader.h"

namespace windows {
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
}

#include "sqlite/ContextReader.h"

using namespace pcv;
using namespace pcv::dbghelp;
using namespace std;

bool ParceiveDbgHelpReader::isFirstSymbolCollection = true;

string getDirectoryName(const string& filePath)
{
  const auto lastSlashIdx = lastIndexOf(filePath, '\\');
  return filePath.substr(0, lastSlashIdx);
}

string getFileName(const string& filePath)
{
  const auto lastSlashIdx = max(0, lastIndexOf(filePath, '\\'));
  const auto offset       = 1 + lastSlashIdx;
  return filePath.substr(offset, filePath.length() - offset);
}

string getFileNameWithoutExtension(const string& filePath)
{
  string fileName(getFileName(filePath));
  const int lastDotIdx = lastIndexOf(fileName, '.');
  if (lastDotIdx > 0) {
    return fileName.substr(0, lastDotIdx);
  }
  return fileName;
}

ParceiveDbgHelpReader::ParceiveDbgHelpReader(Filter* filter) : filter(filter) {}

bool ParceiveDbgHelpReader::getSourceLocation(const S_ADDRUINT& ip,
                                              int32_t* line,
                                              string* fileName) const
{
  INT32 column{0};
  PIN_LockClient();
  PIN_GetSourceLocation(ip, &column, line, fileName);
  PIN_UnlockClient();

  return (line != nullptr);
}

bool ParceiveDbgHelpReader::readSymInfo(const std::string& modulePath,
                                        Id_t imageId,
                                        SymInfo::offset_t moduleBaseAddress)
{
  using namespace windows;
  bool isMainModule = false;
  HMODULE mod       = GetModuleHandleA(nullptr);
  if (mod != nullptr) {
    char mainModuleBuf[MAX_PATH] = {0};
    if (GetModuleFileNameA(mod, mainModuleBuf, sizeof(mainModuleBuf)) > 0) {
      isMainModule = (modulePath == mainModuleBuf);
    }
  }

  if (isMainModule) {
    mainModulePath = modulePath;
  }
  // trying to write all symbol info into one db
  const std::string targetedModulePath = mainModulePath.empty() ? modulePath : mainModulePath;

  const string symbolsPath       = getDirectoryName(parceivePath) + "\\symbols.exe";
  const string imageFileName     = getFileNameWithoutExtension(targetedModulePath);
  const string symbolsDb         = imageFileName + "_symbols.db";
  const string targetDir         = getDirectoryName(targetedModulePath);
  const string fullSymbolsDbPath = targetDir + "\\" + symbolsDb;

  using namespace pcv::symbols::sqlite;

  outOfProcessContext.prepareForNewImage(imageId);
  if (runSymbolCollectionProcess(
        modulePath, imageId, symbolsPath, fullSymbolsDbPath, skipLocalBlocks)) {
    ContextReader reader;
    return reader.readContext(fullSymbolsDbPath, outOfProcessContext, imageId, moduleBaseAddress);
  }
  return false;
}

const Context& ParceiveDbgHelpReader::getContext() const { return outOfProcessContext; }

bool ParceiveDbgHelpReader::runSymbolCollectionProcess(const string& modulePath,
                                                       const Id_t imageId,
                                                       const string& symbolsPath,
                                                       const string& symbolsDb,
                                                       bool skipLocalBlocks)
{
  stringstream cmdLine;
  cmdLine << "\"" << symbolsPath << "\" collect --imageid " << imageId << " --image \""
          << modulePath << "\" --db \"" << symbolsDb << "\"";
  if (isFirstSymbolCollection) {
    // clear any old data if existing.
    cmdLine << " --clear";
    isFirstSymbolCollection = false;
  }
  if (skipLocalBlocks) {
    cmdLine << " --skip_local_blocks";
  }

  using namespace windows;

  STARTUPINFO si;
  PROCESS_INFORMATION pi;

  ZeroMemory(&si, sizeof(si));
  si.cb = sizeof(si);
  ZeroMemory(&pi, sizeof(pi));

  string desiredCurDir = getDirectoryName(modulePath);
  DWORD exitCode       = 23;

  auto now  = std::chrono::system_clock::now();
  time_t tt = chrono::system_clock::to_time_t(now);
  std::cout << ctime(&tt) << cmdLine.str() << " ";

  if (CreateProcess(NULL,                          // No module name
                    LPSTR(cmdLine.str().c_str()),  // Command line
                    NULL,                          // Process handle not inheritable
                    NULL,                          // Thread handle not inheritable
                    FALSE,                         // Set handle inheritance to FALSE
                    CREATE_NO_WINDOW,              // create without window
                    NULL,                          // Use parent's environment block
                    desiredCurDir.c_str(),         // current directory
                    &si,                           // Pointer to STARTUPINFO structure
                    &pi)                           // Pointer to PROCESS_INFORMATION structure
  ) {
    WaitForSingleObject(pi.hProcess, INFINITE);
    GetExitCodeProcess(pi.hProcess, &exitCode);

    windows::CloseHandle(pi.hProcess);
    windows::CloseHandle(pi.hThread);
  }

  if (exitCode != 0) {
    std::cout << "[FAIL]" << endl;
    printf("'%s' failed with return code %lu\n", cmdLine.str().c_str(), exitCode);
  } else {
    std::cout << "[OK]" << endl;
  }
  return exitCode == 0;
}
