#include <chrono>
#include <iostream>
#include <sstream>

#include "DbgHelpReader.h"
#include "DbgTypeConverter.h"
#include "entities/Class.h"
#include "entities/ImageScopedId.h"
#include "entities/Namespace.h"
#include "entities/Routine.h"
#include "entities/Variable.h"

//#define PRINT_DEBUG

#ifdef PRINT_DEBUG
#define DBGHELP_LOG(msg) printf(msg)                     // NOLINT(runtime/printf)
#define DBGHELP_LOGP(msg, ...) printf(msg, __VA_ARGS__)  // NOLINT(runtime/printf)
#else
#define DBGHELP_LOG(msg)
#define DBGHELP_LOGP(msg, ...)
#endif

using pcv::entity::Class;
using pcv::entity::ImageScopedId;
using pcv::entity::Namespace;
using pcv::entity::Variable;
using std::string;
using std::stringstream;
using std::unique_ptr;
using std::vector;

using namespace pcv;
using namespace pcv::dbghelp;

static vector<ImageInfo> getEnabledImages(const json11::Json &rules)
{
  std::vector<ImageInfo> images;

  if (rules.is_array()) {
    for (const auto &item : rules.array_items()) {
      if (item["type"].string_value() != "Image") continue;
      images.emplace_back(static_cast<pcv::entity::Id_t>(item["id"].int_value()),
                          item["path"].string_value());
    }
  }

  return images;
}

static size_t getNumNamespaces(const entity::Routine *routine)
{
  size_t numNs          = 0;
  entity::Namespace *ns = nullptr;
  if (routine != nullptr) {
    ns = routine->nmsp;
  }

  while (ns != nullptr) {
    if (!ns->name.empty()) {
      ++numNs;
    }
    ns = ns->parent;
  }

  return numNs;
}

DbgHelpReader::DbgHelpReader() : filter(nullptr) {}

DbgHelpReader::DbgHelpReader(const std::string &fileName, const FileFilter *filter)
  : filter(filter), nodeJsFileName(fileName), coll(filter)
{}

DbgHelpReader::~DbgHelpReader()
{
  // note: allocated elements held by image_, empty_namespace_, global_vars_, locals_, routines_,
  // index_to_class_ and name_to_class_ are freed via context_ since it holds unique pointers to
  // all these elements after a call to readSymInfo()
}

void DbgHelpReader::start(const json11::Json &rules)
{
  auto images = getEnabledImages(rules);

  if (images.empty()) {
    readSymInfo(nodeJsFileName, 1, 0);
  } else {
    for (const auto &image : images) {
      readSymInfo(image.filePath, image.imageId, 0);
    }
  }
}

const pcv::Context &DbgHelpReader::getContext() const { return converter.getContext(); }

bool DbgHelpReader::readSymInfo(const std::string &fileName, Id_t imageId, SymInfo::offset_t offset)
{
  offset = 0;
  SymbolLoadDirectives directives;
  directives.linkToLoadtimeAddressOffset = 0;

  if (coll.loadSymbols(fileName, directives)) {
    converter.convertDbgHelpTypes(coll, imageId);
    return true;
  }
  return false;
}

bool DbgHelpReader::isBinaryOptimized(const string &fileName)
{
  return !DbgCollector::canLoadSymbols(fileName);
}

void DbgHelpReader::clearSymInfo()
{
  // empty
}

const entity::Routine *DbgHelpReader::getRoutine(const Id_t imageId,
                                                 const std::string rtnName) const
{
  return getContext().getRoutine(imageId, rtnName);
}

bool DbgHelpReader::getSourceLocation(const S_ADDRUINT &ip, int32_t *line, string *fileName) const
{
  uint32_t tmpLine = 0;
  string tmpFileName;

  if (coll.getCodeLocation(ip, tmpFileName, tmpLine)) {
    if (line) {
      *line = static_cast<int32_t>(tmpLine);
    }
    if (fileName) {
      *fileName = tmpFileName;
    }
    return true;
  }
  return false;
}

ImageScopedId DbgHelpReader::getUniqueClassId(const Id_t imageId, const string &rtnName) const
{
  return getContext().getClassId(imageId, rtnName);
}

string DbgHelpReader::refineRtnName(const Id_t imageId, const string &rtnName)
{
  const auto routine = getContext().getRoutine(imageId, rtnName);
  const size_t numNs = getNumNamespaces(routine);

  stringstream ss;
  if (numNs != 0) {
    ss << numNs;
  }
  ss << rtnName;

  return ss.str();
}

unique_ptr<vector<const Variable *>> DbgHelpReader::getGlobalVariables(Id_t imageId) const
{
  return getContext().getGlobalVariables(imageId);
}

unique_ptr<vector<const Variable *>> DbgHelpReader::getStaticVariables(Id_t imageId) const
{
  return getContext().getStaticVariables(imageId);
}

const Variable *DbgHelpReader::getMemberVariable(const entity::ImageScopedId &classId,
                                                 Class::offset_t offset) const
{
  return getContext().getMemberVariable(classId, offset);
}

std::vector<const entity::File *> DbgHelpReader::getFiles(Id_t imageId)
{
  std::vector<const entity::File *> tmp;
  return tmp;
}