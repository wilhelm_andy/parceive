#pragma once

#ifndef DC_CPP_DBGHELP_INCLUDE_DBGTYPE_H_
#define DC_CPP_DBGHELP_INCLUDE_DBGTYPE_H_

#include "DbgUtil.h"
#include "DbgVariable.h"
#include "UdtKind.h"

#include <cstdint>
#include <set>
#include <string>
#include <vector>

namespace pcv {
namespace dbghelp {

struct DbgFunction;
struct DbgNamespace;

struct DbgType {
  std::string full_name;
  std::string name;
  std::string file_name;
  uint32_t index;
  size_t size;
  UdtKind udt_kind;
  std::vector<DbgFunction *> methods;
  std::vector<DbgVariable *> members;
  std::vector<DbgType *> baseclasses;
  std::vector<DbgType *> superclasses;
  std::vector<DbgType *> nestedTypes;
  std::set<size_t> insertedMemberIndices;
  DbgNamespace *parentNamespace;
  DbgType *nestingParent;

  DbgType(const std::string &full_name,
          const std::string &file_name,
          const uint32_t index,
          const size_t size,
          const UdtKind udt_kind)
    : full_name(full_name),
      file_name(file_name),
      index(index),
      size(size),
      udt_kind(udt_kind),
      parentNamespace(nullptr),
      nestingParent(nullptr)
  {
    name = getUnqualifiedName(full_name);
  }

  ~DbgType()
  {
    for (auto var : members) {
      delete var;
    }
  }
};

}  // namespace dbghelp
}  // namespace pcv

#endif  // DC_CPP_DBGHELP_INCLUDE_DBGTYPE_H_
