#pragma once

#ifndef DC_CPP_DBGHELP_INCLUDE_DBGVARIABLE_H_
#define DC_CPP_DBGHELP_INCLUDE_DBGVARIABLE_H_

#include "AddressKind.h"
#include "DataKind.h"
#include "DbgUtil.h"

#include <cstdint>
#include <string>

namespace pcv {
namespace dbghelp {

struct DbgType;

struct DbgVariable {
  std::string full_name;
  std::string name;
  std::string file_name;
  uint32_t index;
  uint32_t type_index;
  size_t offset;
  size_t size;
  DataKind data_kind;
  AddressKind address_kind;
  DbgType* type;
  DbgType* ownerType;
  bool isStatic{};

  DbgVariable(const std::string& full_name,
              const std::string& file_name,
              const uint32_t index,
              const uint32_t type_index,
              const size_t offset,
              const size_t size,
              const DataKind data_kind,
              const AddressKind address_kind)
    : full_name(full_name),
      file_name(file_name),
      index(index),
      type_index(type_index),
      offset(offset),
      size(size),
      data_kind(data_kind),
      address_kind(address_kind),
      type(nullptr),
      ownerType(nullptr)
  {
    name = getUnqualifiedName(full_name);
  }
};

}  // namespace dbghelp
}  // namespace pcv

#endif  // DC_CPP_DBGHELP_INCLUDE_DBGVARIABLE_H_
