#pragma once

#ifndef DC_CPP_DBGHELP_INCLUDE_DATAKIND_H_
#define DC_CPP_DBGHELP_INCLUDE_DATAKIND_H_

namespace pcv {
namespace dbghelp {

enum DataKind {
  DataIsUnknown,
  DataIsLocal,
  DataIsStaticLocal,
  DataIsParam,
  DataIsObjectPtr,
  DataIsFileStatic,
  DataIsGlobal,
  DataIsMember,
  DataIsStaticMember,
  DataIsConstant
};

}  // namespace dbghelp
}  // namespace pcv

#endif  // DC_CPP_DBGHELP_INCLUDE_DATAKIND_H_
