#pragma once

#include <cctype>

#include "ArgumentParser.h"
#include "Common.h"
#include "Logging.h"

using namespace pcv::symbols;
using namespace std;

int getIntOrDefault(const string& text);

bool Arguments::isValid() const
{
  if (command == CommandType::COLLECT) {
    bool valid = true;
    if (dbFilePath.empty()) {
      ERROR("No db path given.");
      valid = false;
    }
    if (imageFilePath.empty()) {
      ERROR("no image path given.");
      valid = false;
    }
    if (imageId < 0) {
      ERROR("No positive image id given.");
      valid = false;
    }
    return valid;
  }

  ERROR("Invalid or no command given.");
  return false;
}

ArgumentParser::ArgumentParser(const int argc, char** argv) : argc(argc), argv(argv) {}

void ArgumentParser::printUsage()
{
  const char* const usage[] = {
    "usage: symbols <command> [<args>]",
    "",
    "Some arguments may only be relevant in certain contexts depending on the",
    "chosen command.",
    "",
    "Commands:",
    "  collect  --imageid <id> --image <path> --db <path> [--locdb <path>] [--clear] "
    "[--skip_local_blocks]",
    "           collects symbols and writes them into a SQLite data base.",
    "           optionally also writes adress to file/line mappings into a db.",
    "           --clear: clears preexisting data before writing into a db.",
    "           --skip_local_blocks: prevents deep local scanning which is much faster.",
    "",
    "Global Arguments:",
    "  --verbosity <level>    Sets the logging verbosity level. Default: 0",
  };
  for (const auto& line : usage) {
    printf("%s\n", line);
  }
}

bool ArgumentParser::parseArguments(int argc, char** argv, Arguments& args) const
{
  const string image("--image");
  const string imageId("--imageid");
  const string db("--db");
  const string locationDb("--locdb");
  const string verbosity("--verbosity");
  const string clear("--clear");
  const string skipLocalBlocks("--skip_local_blocks");

  if (argc < 2) {
    ERROR("No command given.");
    return false;
  }
  args.command = getCommandOrDefault(argv[1]);

  for (int idx = 2; idx < argc; ++idx) {
    if (image == argv[idx]) {
      args.imageFilePath = getArgumentOrDefault(idx + 1);
    } else if (imageId == argv[idx]) {
      args.imageId = getIntArgumentOrDefault(idx + 1);
    } else if (db == argv[idx]) {
      args.dbFilePath = getArgumentOrDefault(idx + 1);
    } else if (locationDb == argv[idx]) {
      args.locationDbFilePath = getArgumentOrDefault(idx + 1);
    } else if (verbosity == argv[idx]) {
      setVerbosity(getIntArgumentOrDefault(idx + 1));
    } else if (clear == argv[idx]) {
      args.clearPreexistingData = true;
    } else if (skipLocalBlocks == argv[idx]) {
      args.skipLocalBlocks = true;
    }
  }

  return args.isValid();
}

string ArgumentParser::getArgumentOrDefault(int idx) const { return idx < argc ? argv[idx] : ""; }

int ArgumentParser::getIntArgumentOrDefault(int idx) const
{
  return getIntOrDefault(getArgumentOrDefault(idx));
}

CommandType ArgumentParser::getCommandOrDefault(const string text)
{
  CommandType cmd = CommandType::NONE;

  if (toLower(text) == "collect") {
    cmd = CommandType::COLLECT;
  }
  return cmd;
}

int getIntOrDefault(const string& text) { return atoi(text.c_str()); }