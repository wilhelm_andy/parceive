#define LOGURU_IMPLEMENTATION 1
#include "Logging.h"

#ifdef _MSC_VER
#define WIN32_LEAN_AND_MEAN
#include "intrin.h"
#include "windows.h"
void debugBreak() { __debugbreak(); }
#else
#include <signal.h>
void debugBreak() { raise(SIGTRAP); }
#endif

void pcv::symbols::initLogging(int argc, char** argv) { loguru::init(argc, argv); }

void pcv::symbols::setLogMode(const LogMode& mode)
{
  const bool isDetailed = (LogMode::DETAILED == mode);

  loguru::g_preamble_date   = isDetailed;
  loguru::g_preamble_time   = isDetailed;
  loguru::g_preamble_uptime = isDetailed;
  loguru::g_preamble_thread = isDetailed;
  loguru::g_preamble_file   = isDetailed;
}

void pcv::symbols::setVerbosity(const int level)
{
  loguru::g_stderr_verbosity = level;
  INFO("Set verbosity level to %d", level);
}