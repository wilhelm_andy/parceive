#include "Symbols.h"
#include "DbgCollector.h"
#include "DbgTypeConverter.h"
#include "Logging.h"
#include "sqlite/CodeLocationReader.h"
#include "sqlite/CodeLocationWriter.h"
#include "sqlite/ContextReader.h"
#include "sqlite/ContextWriter.h"

using namespace pcv::symbols;
using namespace pcv::dbghelp;

int collectSymbols(const Arguments& args);
void writeSourceCodeLocations(const Arguments& args, DbgCollector coll);
void writeSymbolContext(const Arguments& args, const pcv::Context& ctxt);

int pcv::symbols::runProgram(const Arguments& args)
{
  int retVal = 1;

  if (args.command == CommandType::COLLECT) {
    retVal = collectSymbols(args);
  }
  return retVal;
}

int collectSymbols(const Arguments& args)
{
  VERBOSE_SCOPE(1, "Collect symbols");

  int retVal = 1;
  DbgCollector coll;
  const bool deepScan = !args.skipLocalBlocks;

  SymbolLoadDirectives directives;
  directives.deepScan = deepScan;

  if (coll.loadSymbols(args.imageFilePath, directives)) {
    DbgTypeConverter converter;
    converter.convertDbgHelpTypes(coll, args.imageId);
    const auto& ctxt = converter.getContext();

    if (!args.locationDbFilePath.empty()) {
      writeSourceCodeLocations(args, coll);
    }
    writeSymbolContext(args, ctxt);
    retVal = 0;
  } else {
    ERROR("Unable to collect file '%s'.", args.imageFilePath.c_str());
  }
  return retVal;
}

void writeSourceCodeLocations(const Arguments& args, DbgCollector coll)
{
  VERBOSE_SCOPE(1, "Writing source code locations");

  sqlite::CodeLocationWriter srcWriter;
  srcWriter.insertCodeLocations(
    args.locationDbFilePath, args.imageId, coll.map_address_to_context, args.clearPreexistingData);
}

void writeSymbolContext(const Arguments& args, const pcv::Context& ctxt)
{
  VERBOSE_SCOPE(1, "Writing symbol context");

  sqlite::ContextWriter writer;
  writer.insertContext(args.dbFilePath, ctxt, args.clearPreexistingData);
}