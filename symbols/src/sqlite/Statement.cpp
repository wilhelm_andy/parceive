#include <sqlite3.h>

#include "Logging.h"
#include "sqlite/Statement.h"

using namespace pcv::symbols::sqlite;
using namespace std;

Statement::Statement() : stmt(nullptr) {}

Statement::Statement(sqlite3_stmt* stmt) : stmt(stmt) {}

Statement::Statement(sqlite3_stmt* stmt, const string& sql) : stmt(stmt), sql(sql) {}

Statement::~Statement() { finalize(); }

Statement& Statement::operator=(Statement& rhs)
{
  // full transfer of ownership. This is needed since statement usage in
  // this project's context is meant to work without reference counting
  // and object destruction enforces statement finalization
  stmt     = rhs.stmt;
  sql      = rhs.sql;
  rhs.stmt = nullptr;
  return *this;
}

void Statement::setStatement(sqlite3_stmt* stmt, const char* sql)
{
  finalize();
  this->stmt = stmt;
  this->sql  = sql ? sql : "";
}

void Statement::finalize()
{
  if (isValid()) {
    int code = sqlite3_finalize(stmt);
    if (SQLITE_OK != code) {
      ERROR("Unable to finalize statement: %s", sqlite3_errstr(code));
    }
    stmt = nullptr;
  }
}

void Statement::bind(const int idx, const int64_t value) const
{
  int code = sqlite3_bind_int64(stmt, idx, value);
  ERROR_IF(SQLITE_OK != code, "Unable to bind value %ll at index %d.", value, idx);
}

void Statement::bind(const int idx, const uint32_t value) const
{
  int code = sqlite3_bind_int(stmt, idx, value);
  ERROR_IF(SQLITE_OK != code, "Unable to bind value %ll at index %d.", value, idx);
}

void Statement::bind(const int idx, const int value) const
{
  int code = sqlite3_bind_int(stmt, idx, value);
  ERROR_IF(SQLITE_OK != code, "Unable to bind value %ll at index %d.", value, idx);
}

void Statement::bind(const int idx, const std::string& value) const
{
  int code = sqlite3_bind_text(stmt, idx, value.c_str(), -1, SQLITE_TRANSIENT);
  ERROR_IF(SQLITE_OK != code, "Unable to bind value %s at index %d.", value.c_str(), idx);
}

void Statement::bind(const int idx, const char* value) const
{
  int code = sqlite3_bind_text(stmt, idx, value, -1, SQLITE_TRANSIENT);
  ERROR_IF(SQLITE_OK != code, "Unable to bind value %ll at index %d.", value, idx);
}

void Statement::bindNULL(const int idx) const
{
  int code = sqlite3_bind_null(stmt, idx);
  ERROR_IF(SQLITE_OK != code, "Unable to bind NULL at index %d.", idx);
}

int Statement::columnInt(const int idx) const { return sqlite3_column_int(stmt, idx); }

int64_t Statement::columnInt64(const int idx) const { return sqlite3_column_int64(stmt, idx); }

string Statement::columnString(const int idx) const
{
  return string(reinterpret_cast<char const*>(sqlite3_column_text(stmt, idx)));
}

bool Statement::execute() const
{
  bool res = true;
  res &= step();
  res &= reset();
  res &= clearBindings();
  return res;
}

bool Statement::retrieveData(const DataRetrievalCallback& rowRetrieved) const
{
  bool reading = true;
  while (reading) {
    const int code = sqlite3_step(stmt);
    switch (code) {
      case SQLITE_ROW:
        rowRetrieved(*this);
        break;
      case SQLITE_DONE:
        reading = false;
        break;
      default:
        ERROR("Unable to retrieve data: %s", sqlite3_errstr(code));
        return false;
    }
  }
  return true;
}

bool Statement::step() const
{
  int code = sqlite3_step(stmt);
  if (SQLITE_DONE != code) {
    ERROR("Unable to step: %s.", sqlite3_errstr(code));
    return false;
  }
  return true;
}

bool Statement::reset() const
{
  int code = sqlite3_reset(this->stmt);
  if (SQLITE_OK != code) {
    ERROR("Unable to reset statement.");
    return false;
  }
  return true;
}

bool Statement::clearBindings() const
{
  int code = sqlite3_clear_bindings(this->stmt);
  if (SQLITE_OK != code) {
    ERROR("Unable to clear statement bindings.");
    return false;
  }
  return true;
}

int Statement::getColumnCount() const { return isValid() ? sqlite3_column_count(stmt) : 0; }
