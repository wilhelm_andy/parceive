#include <functional>
#include <map>
#include <sstream>

#include "Logging.h"
#include "sqlite/CodeLocationReader.h"
#include "sqlite/Connection.h"
#include "sqlite/StatementReader.h"

using namespace pcv;
using namespace pcv::dbghelp;
using namespace pcv::symbols::sqlite;
using namespace std;

#define BIND(FUNC) bind(&CodeLocationReader::FUNC, this, placeholders::_1)

bool CodeLocationReader::readCodeLocations(const string& dbFilePath,
                                           const pcv::Id_t imageId,
                                           AddressToCodeLocationMap& locationMap)
{
  VERBOSE_SCOPE(1, "Reading code locations");

  Connection conn;
  if (conn.open(dbFilePath.c_str())) {
    target = &locationMap;

    stringstream ss;
    ss << "SELECT * from CodeLocation WHERE ImageId = ";
    ss << imageId;
    ss << ";";

    auto selectCodeLocation = conn.createStatement(ss.str().c_str());
    if (selectCodeLocation.isValid()) {
      selectCodeLocation.retrieveData(BIND(onCodeLocationRowRetrieved));
      return true;
    }
  }
  return false;
}

void CodeLocationReader::onCodeLocationRowRetrieved(const Statement& stmt) const
{
  pcv::Id_t imageId;
  uint64_t address;
  CodeLocation location;
  stmt >> imageId >> address >> location.fileName >> location.line;

  (*target)[address] = location;
}
