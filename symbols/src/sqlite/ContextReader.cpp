#include <cinttypes>
#include <functional>
#include <sstream>

#include "sqlite/Connection.h"
#include "sqlite/ContextReader.h"
#include "sqlite/ContextWriter.h"
#include "sqlite/StatementBinder.h"
#include "sqlite/StatementReader.h"

#include "Common.h"
#include "Context.h"
#include "entities/Class.h"
#include "entities/Image.h"
#include "entities/Namespace.h"
#include "entities/Routine.h"
#include "entities/Variable.h"

#include "Logging.h"

using namespace pcv;
using namespace pcv::entity;
using namespace pcv::symbols::sqlite;
using namespace std;

#define BIND(FUNC) bind(&ContextReader::FUNC, this, placeholders::_1)

static string getQuery(const string& tableName, const pcv::Id_t imageId)
{
  stringstream ss;
  ss << "SELECT * FROM " << tableName;
  if (imageId > 0) {
    ss << " WHERE ImageId = " << imageId;
  }
  ss << ";";
  return ss.str();
}

bool ContextReader::readContext(const string& dbFilePath,
                                Context& ctxt,
                                pcv::Id_t imageIdSelection,
                                uint64_t moduleBaseAddress)
{
  VERBOSE_SCOPE(1, "Read Context");

  this->moduleBaseAddress = moduleBaseAddress;
  Connection conn;
  if (conn.open(dbFilePath.c_str())) {
    struct Mappings {
      string tableName;
      Statement::DataRetrievalCallback callback;
    };
    Mappings mappings[] = {
      {"SoftwareEntity", BIND(onSoftwareEntityRowRetrieved)},
      {"ClassRelationships", BIND(onClassRelationshipsRowRetrieved)},
      {"Namespace", BIND(onNamespaceRowRetrieved)},
      {"Routine", BIND(onRoutineRowRetrieved)},
      {"Variable", BIND(onVariableRowRetrieved)},
    };

    for (const auto& mapping : mappings) {
      bool inputOk     = false;
      const auto query = getQuery(mapping.tableName, imageIdSelection);
      const auto stmt  = conn.createStatement(query);
      if (stmt.isValid()) {
        if (stmt.retrieveData(mapping.callback)) {
          inputOk = true;
        }
      }
      if (!inputOk) {
        ERROR("Unable to read context.");
        return false;
      }
    }

    restoreLinks();
    fillContext(ctxt);
  }
  return true;
}

void ContextReader::onSoftwareEntityRowRetrieved(const Statement& stmt)
{
  ImageScopedId id;
  SoftareEntityLinks link;

  auto reader = stmt >> id.id >> id.imageId >> link.type;
  auto entity = createEntityOfType(link.type, id);
  std::string fileName;
  reader >> link.namespaceId >> link.classId >> entity->name >> entity->size >> fileName >>
    entity->line;
  entity->file  = new File(fileName, entity->img);
  link.entityId = id;
  links.emplace_back(link);
}

void ContextReader::onClassRelationshipsRowRetrieved(const Statement& stmt)
{
  ImageScopedId id;
  ClassRelationshipLink link;
  stmt >> id.id >> link.secondClassId >> id.imageId >> link.type;
  classRelationsMap[id] = link;
}

void ContextReader::onNamespaceRowRetrieved(const Statement& stmt)
{
  ImageScopedId id;
  Id_t parentId;
  stmt >> id.id >> id.imageId >> parentId;
  if (parentId != NO_ID) {
    auto ns    = getNamespace(id);
    ns->parent = getNamespace({id.imageId, parentId});
  }
}

void ContextReader::onRoutineRowRetrieved(const Statement& stmt)
{
  ImageScopedId id;
  auto reader  = stmt >> id.id >> id.imageId;
  auto routine = getRoutine(id);
  reader >> routine->isConstructor;
}

void ContextReader::onVariableRowRetrieved(const Statement& stmt)
{
  ImageScopedId id;
  Id_t ownerClassId;
  Id_t ownerRoutineId;
  auto reader   = stmt >> id.id >> id.imageId >> ownerClassId >> ownerRoutineId;
  auto variable = getVariable(id);
  if (ownerClassId != NO_ID) {
    variable->declaringClass = getClass({id.imageId, ownerClassId});
    variable->declaringClass->members.emplace_back(variable);
  }
  if (ownerRoutineId != NO_ID) {
    variable->declaringRoutine = getRoutine({id.imageId, ownerRoutineId});
    variable->declaringRoutine->locals.emplace_back(variable);
  }
  reader >> variable->offset >> variable->type;
  if (Variable::Type::GLOBAL == (variable->type & Variable::Type::GLOBAL) ||
      (Variable::Type::STATICVAR == (variable->type & Variable::Type::STATICVAR))) {
    variable->offset += this->moduleBaseAddress;
  }
}

SoftwareEntity* ContextReader::createEntityOfType(const EntityType type, const ImageScopedId& id)
{
  SoftwareEntity* res = nullptr;
  switch (type) {
    case EntityType::Class:
      res          = new Class();
      classMap[id] = static_cast<Class*>(res);
      break;
    case EntityType::Routine:
      res            = new Routine();
      routineMap[id] = static_cast<Routine*>(res);
      break;
    case EntityType::Variable:
      res             = new Variable();
      variableMap[id] = static_cast<Variable*>(res);
      break;
    case EntityType::Namespace:
      res              = new Namespace();
      namespaceMap[id] = static_cast<Namespace*>(res);
      break;
    case EntityType::Image:
      res          = new Image();
      imageMap[id] = static_cast<Image*>(res);
      break;
    default:
      return nullptr;
  }
  res->id = id.id;
  if (type != EntityType::Image) {
    res->img = static_cast<Image*>(getEntityOfType({id.imageId, id.imageId}, EntityType::Image));
  }

  return res;
}

void ContextReader::restoreLinks()
{
  restoreEntityLinks();
  restoreClassRelationships();
}

void ContextReader::fillContext(Context& ctxt)
{
  for (const auto& img : imageMap) {
    ctxt.addImage(img.second);
  }
  for (const auto& ns : namespaceMap) {
    ctxt.addNamespace(ns.second);
  }
  for (const auto& rtn : routineMap) {
    ctxt.addRoutine(rtn.second);
  }
  for (const auto& cls : classMap) {
    ctxt.addClass(cls.second);
  }
  for (const auto& var : variableMap) {
    ctxt.addVariable(var.second);
  }
}

void ContextReader::restoreEntityLinks()
{
  for (const auto& link : links) {
    const auto targetEntity = getEntityOfType(link.entityId, link.type);
    if (NO_ID != link.classId) {
      targetEntity->cls = getClass({link.entityId.imageId, link.classId});
    }
    if (NO_ID != link.namespaceId) {
      targetEntity->nmsp = getNamespace({link.entityId.imageId, link.namespaceId});
    }
  }
}

void ContextReader::restoreClassRelationships()
{
  for (const auto& classRelation : classRelationsMap) {
    const auto& targetId         = classRelation.first;
    const auto& data             = classRelation.second;
    const ImageScopedId sourceId = {targetId.imageId, data.secondClassId};

    auto target = getClass(targetId);
    auto source = getClass(sourceId);

    switch (data.type) {
      case ClassRelationshipType::Base:
        target->baseClasses.emplace_back(source);
        break;
      case ClassRelationshipType::Inherited:
        target->inheritClasses.emplace_back(source);
        break;
      case ClassRelationshipType::Nested:
        target->nestedClasses.emplace_back(source);
        source->nestingParent = target;
        break;
      case ClassRelationshipType::Composite:
        target->composites.emplace_back(source);
        break;
    }
  }
}

Namespace* ContextReader::getNamespace(const ImageScopedId& id)
{
  return static_cast<Namespace*>(getEntityOfType(id, EntityType::Namespace));
}

Routine* ContextReader::getRoutine(const ImageScopedId& id)
{
  return static_cast<Routine*>(getEntityOfType(id, EntityType::Routine));
}

Variable* ContextReader::getVariable(const ImageScopedId& id)
{
  return static_cast<Variable*>(getEntityOfType(id, EntityType::Variable));
}

Class* ContextReader::getClass(const ImageScopedId& id)
{
  return static_cast<Class*>(getEntityOfType(id, EntityType::Class));
}

SoftwareEntity* ContextReader::getEntityOfType(const ImageScopedId& id, const EntityType type)
{
  SoftwareEntity* entity = nullptr;
  switch (type) {
    case EntityType::Class:
      entity = getEntityOrDefault(id, classMap);
      break;
    case EntityType::Routine:
      entity = getEntityOrDefault(id, routineMap);
      break;
    case EntityType::Variable:
      entity = getEntityOrDefault(id, variableMap);
      break;
    case EntityType::Namespace:
      entity = getEntityOrDefault(id, namespaceMap);
      break;
    case EntityType::Image:
      entity = getEntityOrDefault(id, imageMap);
      break;
  }

  if (entity == nullptr) {
    ERROR("Could not find entity with id %" PRIu64 " and image id %" PRIu64 " and type %d.",
          id.id,
          id.imageId,
          type);
  }
  return entity;
}