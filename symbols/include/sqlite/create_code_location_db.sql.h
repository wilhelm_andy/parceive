R"====(CREATE TABLE IF NOT EXISTS CodeLocation (
  ImageId INTEGER NOT NULL,
  Address INTEGER NOT NULL,
  FileName VARCHAR NOT NULL,
  Line INTEGER NOT NULL,
  PRIMARY KEY(ImageId, Address)
);
)===="