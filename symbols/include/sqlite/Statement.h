#pragma once

#include <cstdint>
#include <functional>
#include <string>

struct sqlite3_stmt;

namespace pcv {
namespace symbols {
namespace sqlite {

class Connection;

class Statement final
{
 public:
  typedef std::function<void(const Statement&)> DataRetrievalCallback;

  Statement();
  explicit Statement(sqlite3_stmt* stmt);
  explicit Statement(sqlite3_stmt* stmt, const std::string& sql);
  ~Statement();

  Statement& operator=(Statement& rhs);

  void setStatement(sqlite3_stmt* stmt, const char* sql = nullptr);
  void finalize();

  void bind(const int idx, const uint64_t val) const { bind(idx, static_cast<int64_t>(val)); }
  void bind(const int idx, const int64_t value) const;
  void bind(const int idx, const uint32_t value) const;
  void bind(const int idx, const int value) const;
  void bind(const int idx, const std::string& value) const;
  void bind(const int idx, const char* value) const;
  void bindNULL(int) const;

  template <typename T>
  T column(const int idx) const;
  template <typename T>
  void getColumn(T& data, const int idx) const
  {
    data = column<T>(idx);
  }

  bool execute() const;
  bool retrieveData(const DataRetrievalCallback& rowRetrieved) const;

  bool step() const;
  bool reset() const;
  bool clearBindings() const;

  bool isValid() const { return stmt != nullptr; }

 private:
  int columnInt(const int idx) const;
  int64_t columnInt64(const int idx) const;
  std::string columnString(const int idx) const;

  int getColumnCount() const;

  int columnCount;
  int lastBound;
  sqlite3_stmt* stmt;
  std::string sql;
};

template <typename T>
T Statement::column(const int idx) const
{
  return static_cast<T>(columnInt(idx));
}
template <>
inline int64_t Statement::column<int64_t>(const int idx) const
{
  return columnInt64(idx);
}
template <>
inline uint64_t Statement::column<uint64_t>(const int idx) const
{
  return (uint64_t)columnInt64(idx);
}
template <>
inline std::string Statement::column<std::string>(const int idx) const
{
  return columnString(idx);
}
template <>
inline bool Statement::column<bool>(const int idx) const
{
  return 0 != columnInt(idx);
}

class NullClass
{
};
extern NullClass SQLNULL;

}  // namespace sqlite
}  // namespace symbols
}  // namespace pcv
