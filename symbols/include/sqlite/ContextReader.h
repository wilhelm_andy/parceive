#pragma once

#include <cinttypes>
#include <string>
#include <unordered_map>
#include <vector>

#include "entities/ImageScopedId.h"

#include "Common.h"
#include "Logging.h"
#include "sqlite/Connection.h"
#include "sqlite/ContextTypes.h"
#include "sqlite/Statement.h"

namespace pcv {
namespace entity {
enum class EntityType;

struct Variable;
struct Routine;
struct Namespace;
struct Image;
struct Class;
struct SoftwareEntity;
}  // namespace entity
class Context;

namespace symbols {
namespace sqlite {

class Connection;

class ContextReader final
{
 public:
  bool readContext(const std::string& dbFilePath,
                   Context& ctxt,
                   pcv::Id_t imageIdSelection,
                   uint64_t offset);

 private:
  void onSoftwareEntityRowRetrieved(const Statement& stmt);
  void onClassRelationshipsRowRetrieved(const Statement& stmt);
  void onNamespaceRowRetrieved(const Statement& stmt);
  void onRoutineRowRetrieved(const Statement& stmt);
  void onVariableRowRetrieved(const Statement& stmt);

  entity::SoftwareEntity* createEntityOfType(const entity::EntityType type,
                                             const entity::ImageScopedId& id);
  void restoreClassRelationships();
  void restoreEntityLinks();
  void restoreLinks();
  void fillContext(Context& ctxt);

  struct SoftareEntityLinks {
    entity::ImageScopedId entityId;
    entity::EntityType type;
    pcv::Id_t namespaceId, classId;
  };
  struct ClassRelationshipLink {
    pcv::Id_t secondClassId;
    ClassRelationshipType type;
  };

  entity::Namespace* getNamespace(const entity::ImageScopedId& id);
  entity::Routine* getRoutine(const entity::ImageScopedId& id);
  entity::Variable* getVariable(const entity::ImageScopedId& id);
  entity::Class* getClass(const entity::ImageScopedId& id);
  entity::SoftwareEntity* getEntityOfType(const entity::ImageScopedId& id,
                                          const entity::EntityType type);

  template <typename TEntity>
  entity::SoftwareEntity* getEntityOrDefault(
    const entity::ImageScopedId& id, std::unordered_map<entity::ImageScopedId, TEntity*>& map);

  std::vector<SoftareEntityLinks> links;
  std::unordered_map<entity::ImageScopedId, ClassRelationshipLink> classRelationsMap;
  std::unordered_map<entity::ImageScopedId, entity::Class*> classMap;
  std::unordered_map<entity::ImageScopedId, entity::Routine*> routineMap;
  std::unordered_map<entity::ImageScopedId, entity::Variable*> variableMap;
  std::unordered_map<entity::ImageScopedId, entity::Namespace*> namespaceMap;
  std::unordered_map<entity::ImageScopedId, entity::Image*> imageMap;
  uint64_t moduleBaseAddress;
};

template <typename TEntity>
entity::SoftwareEntity* ContextReader::getEntityOrDefault(
  const entity::ImageScopedId& id, std::unordered_map<entity::ImageScopedId, TEntity*>& map)
{
  const auto it = map.find(id);
  if (it == map.end()) {
    ERROR(
      "Could not find namespace with id %" PRIu64 " and image id %" PRIu64 ".", id.id, id.imageId);
    return nullptr;
  }
  return it->second;
}

}  // namespace sqlite
}  // namespace symbols
}  // namespace pcv
