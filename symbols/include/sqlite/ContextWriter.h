#pragma once

#include <array>
#include <string>
#include "sqlite/Connection.h"
#include "sqlite/ContextTypes.h"
#include "sqlite/Statement.h"

namespace pcv {
namespace entity {

enum class EntityType;
struct Variable;
struct Routine;
struct Namespace;
struct Image;
struct Class;
struct SoftwareEntity;
}  // namespace entity
class Context;

namespace symbols {
namespace sqlite {

class Connection;

class ContextWriter final
{
 public:
  ContextWriter();

  void insertContext(const std::string& dbFilePath,
                     const Context& ctxt,
                     bool clearPreexistingData = true);

 private:
  void writeData(const Context& ctxt);
  void insert(const entity::SoftwareEntity& entity, const entity::EntityType& type);
  void insert(const entity::Image& image);
  void insert(const entity::Namespace& ns);
  void insert(const entity::Routine& routine);
  void insert(const entity::Class& cls);
  void insert(const entity::Variable& var);
  void insertClassRelationship(const entity::Class& owner,
                               const entity::Class* owned,
                               const ClassRelationshipType& type);

  bool createDatabase() const;
  bool prepareStatements();
  void finalizeAllStatements();
  bool areAllStatementsValid();

  std::array<Statement*, 7> getAllStatements();

  // order: connection variable is stated first so it
  // will be destructed after the statemens
  Connection conn;
  Statement beginTransaction;
  Statement commitTransaction;
  Statement insertSoftwareEntity;
  Statement insertClassRelationships;
  Statement insertNamespace;
  Statement insertRoutine;
  Statement insertVariable;
};

}  // namespace sqlite
}  // namespace symbols
}  // namespace pcv
