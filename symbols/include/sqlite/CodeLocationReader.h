#pragma once

#include <cinttypes>
#include <map>
#include <string>

#include "Common.h"
#include "DbgCollector.h"
#include "sqlite/Statement.h"

namespace pcv {
namespace symbols {
namespace sqlite {

class CodeLocationReader final
{
 public:
  bool readCodeLocations(const std::string& dbFilePath,
                         const pcv::Id_t imageId,
                         dbghelp::AddressToCodeLocationMap& locationMap);

 private:
  void onCodeLocationRowRetrieved(const Statement& stmt) const;

  std::map<uint64_t, dbghelp::CodeLocation>* target{};
};

}  // namespace sqlite
}  // namespace symbols
}  // namespace pcv
