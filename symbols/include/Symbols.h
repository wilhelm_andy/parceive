#pragma once

#include "ArgumentParser.h"

namespace pcv {
namespace symbols {
int runProgram(const Arguments& args);
}
}  // namespace pcv