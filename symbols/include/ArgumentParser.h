#pragma once

#include <string>

namespace pcv {
namespace symbols {

enum class CommandType {
  NONE,
  COLLECT,
};

struct Arguments {
  bool isValid() const;

  CommandType command{};
  std::string dbFilePath{};
  std::string locationDbFilePath{};
  std::string imageFilePath{};
  int imageId = -1;
  bool clearPreexistingData{};
  bool skipLocalBlocks{};
};

class ArgumentParser
{
 public:
  ArgumentParser(int argc, char** argv);
  static void printUsage();
  bool parseArguments(int argc, char** argv, Arguments& args) const;

 private:
  std::string getArgumentOrDefault(int idx) const;
  int getIntArgumentOrDefault(int idx) const;
  static CommandType getCommandOrDefault(const std::string text);

  int argc;
  char** argv;
};

}  // namespace symbols
}  // namespace pcv