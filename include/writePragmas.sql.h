R"=====(
PRAGMA auto_vacuum = 0;
PRAGMA automatic_index = FALSE;
PRAGMA foreign_keys = FALSE;
PRAGMA ignore_check_constraints = TRUE;
PRAGMA locking_mode = EXCLUSIVE;
PRAGMA journal_mode=WAL;
PRAGMA synchronous = OFF;
)====="
