#pragma once

#include "Writer.h"

namespace pcv {

class DummyWriter : public Writer
{
 public:
  DummyWriter()  = default;
  ~DummyWriter() = default;

  void insert(const model::File &&) override{};
  void insert(const model::Image &&) override{};
  void insert(const model::Function &&) override{};
  void insert(const model::Thread &&) override{};
  void insert(const model::Call &&) override{};
  void insert(const model::Segment &&) override{};
  void insert(const model::Instruction &&) override{};
  void insert(const model::Loop &&) override{};
  void insert(const model::LoopExecution &&) override{};
  void insert(const model::LoopIteration &&) override{};
  void insert(const model::Member &&) override{};
  void insert(const model::Access &&) override{};
  void insert(const model::Reference &&) override{};

  void remove(const model::LoopIteration &&) override{};
};

}  // namespace pcv
