#ifndef SQLWRITER_H
#define SQLWRITER_H

#include <pin.H>
#include <cstdint>
#include <memory>
#include <string>

#include "sqlite3.h"

#include "SQLite.h"
#include "Writer.h"

namespace pcv {

class SQLWriter : public Writer
{
 public:
  explicit SQLWriter(const std::string &file, bool createDb = false);
  SQLWriter(std::shared_ptr<SQLite::Connection> db, bool createDb = false);
  ~SQLWriter() override;

  void begin();
  void commit();

  void insert(const model::File &&) override;
  void insert(const model::Image &&) override;
  void insert(const model::Function &&) override;
  void insert(const model::Thread &&) override;
  void insert(const model::Call &&) override;
  void insert(const model::Segment &&) override;
  void insert(const model::Instruction &&) override;
  void insert(const model::Loop &&) override;
  void insert(const model::LoopExecution &&) override;
  void insert(const model::LoopIteration &&) override;
  void insert(const model::Member &&) override;
  void insert(const model::Access &&) override;
  void insert(const model::Reference &&) override;

  void remove(const model::LoopIteration &&) override;

 private:
  std::shared_ptr<SQLite::Connection> _db;

  PIN_MUTEX _mutex;

  std::shared_ptr<SQLite::Statement> _insertFileStmt;
  std::shared_ptr<SQLite::Statement> _insertImageStmt;
  std::shared_ptr<SQLite::Statement> _insertFunctionStmt;
  std::shared_ptr<SQLite::Statement> _insertThreadStmt;
  std::shared_ptr<SQLite::Statement> _insertCallStmt;
  std::shared_ptr<SQLite::Statement> _insertSegmentStmt;
  std::shared_ptr<SQLite::Statement> _insertInstructionStmt;
  std::shared_ptr<SQLite::Statement> _insertLoopStmt;
  std::shared_ptr<SQLite::Statement> _insertLoopExecutionStmt;
  std::shared_ptr<SQLite::Statement> _insertLoopIterationStmt;
  std::shared_ptr<SQLite::Statement> _insertMemberStmt;
  std::shared_ptr<SQLite::Statement> _insertAccessStmt;
  std::shared_ptr<SQLite::Statement> _insertReferenceStmt;

  std::shared_ptr<SQLite::Statement> _removeLoopIterationStmt;

  std::shared_ptr<SQLite::Statement> _beginTransactionStmt;
  std::shared_ptr<SQLite::Statement> _commitTransactionStmt;

  void prepareStatements();
  void createDatabase();
  void runPragmas();
  void clearDatabase();

  void lock();
  void unlock();
};

}  // namespace pcv

#endif  // SQLWRITER_H
