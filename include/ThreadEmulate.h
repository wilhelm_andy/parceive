/*! \file EmulatePThreads.h
 *  \brief This file contains the PinToolInstrumentPThreadLibrary class
 * which contains instrumenting functions for PThread library
 */

/* Copyright 2014 Siemens Technology and Services*/

#ifndef SRC_BACKEND_INCLUDE_EMULATEPTHREADS_H_
#define SRC_BACKEND_INCLUDE_EMULATEPTHREADS_H_

#pragma once
#include <memory>
#include <unordered_map>

#include "Thread.h"
#include "model/Call.h"
#include "model/Function.h"
#include "model/Reference.h"
#include "model/Thread.h"

namespace pcv {

extern const std::string mutex_lock_str;
extern const std::string mutex_lock_str_u;
extern const std::string mutex_unlock_str;
extern const std::string mutex_unlock_str_u;

/**
 * @brief This class serves as a static helper class that supports the static
 * callbacks and replacment functions from PinToolInstumentPThreadLibrary.
 */
class EmulatePThreadHandler
{
 public:
  typedef struct threadInfo_t {
    model::Thread::Id_t threadId;
    unsigned int processId;
    unsigned long start;
    unsigned long end;
    unsigned int createInstruction;
    model::Call::Id_t callId;
    void* result;

    threadInfo_t() : result(nullptr) {}
  } threadInfo_t;

  EmulatePThreadHandler()
    : mutex_lock_id(model::Function::EMPTY), mutex_unlock_id(model::Function::EMPTY)
  {
    threadStack.push((unsigned long&&)model::Thread::MAIN);
  }
  ~EmulatePThreadHandler() {}

  std::stack<model::Thread::Id_t> threadStack;

  std::unordered_map<model::Thread::Id_t, std::unique_ptr<threadInfo_t>> threadMap;

  model::Function::Id_t mutex_lock_id;
  model::Function::Id_t mutex_unlock_id;
};

/**
 * @brief This class extends the InstrumentThreadLibrary class and provides
 * instrumentation routines for the PThread library
 */
class ThreadEmulate : public pcv::Thread
{
  DataModel* pModel;
  Resolver* pResolver;
  EmulatePThreadHandler sHandler;

 public:
  using threadInfo_t = EmulatePThreadHandler::threadInfo_t;

  ThreadEmulate(Parceive* parceive);
  ~ThreadEmulate() override;

  void initialize() override;
  void finalize() override;

  void beforeInstrumentation(void) override;
  void instrumentImage(IMG) override;

  model::Thread::Id_t getThreadId(const THREADID& threadId);

  /** @brief Instrument pthread_create   */
  void instrumentCreate(IMG img /**< [IN] The PIN image. */);

  /** @brief Instrument pthread_join   */
  void instrumentJoin(IMG img /**< [IN] The PIN image. */);

  /** @brief Instrument pthread_mutex_lock   */
  void instrumentLock(IMG img /**< [IN] The PIN image. */);

  /** @brief Instrument pthread_mutex_unlock   */
  void instrumentUnlock(IMG img /**< [IN] The PIN image. */);

  /*! \brief the replacement function for pthread_create */
  static int w_pthreadCreate(ThreadEmulate* pThis,
                             CONTEXT* context,
                             pthread_t* thread,
                             pthread_attr_t* attr,
                             void* (*start_routine)(void*),
                             void* arg,
                             AFUNPTR orig_funptr);

  /*! \brief the replacement function for pthread_join */
  static int w_pthreadJoin(
    ThreadEmulate* pThis, CONTEXT* context, pthread_t thread, void** result, AFUNPTR orig_funptr);

  /*! \brief the callback for thread start */
  static void cb_threadStart(THREADID pin_tid, CONTEXT* context, INT32 code, VOID* v);

  /*! \brief the callback for thread fini */
  static void cb_threadFini(THREADID pin_tid, const CONTEXT* context, INT32 code, VOID* v);

  /**
   * @brief Returns the id of the (possibly created) mutex reference;
   */
  model::Reference::Id_t getReferenceId(
    THREADID threadId,   /**< [IN] The thread id that called the function. */
    const ADDRINT* mutex /**< [IN] The address of the aquired mutex. */
  );

  /**
   * @brief Analysis method for pthread_mutex_lock.
   */
  static void an_pthreadMutexLock(
    ThreadEmulate* pThis,
    THREADID threadId, /**< [IN] The thread id that called the function. */
    ADDRINT* mutex     /**< [IN] The address of the aquired mutex. */
  );

  /**
   * @brief Analysis method for pthread_mutex_lock.
   */
  static void an_pthreadMutexUnlock(
    ThreadEmulate* pThis,
    THREADID threadId, /**< [IN] The thread id that called the function. */
    ADDRINT* mutex     /**< [IN] The address of the released mutex. */
  );
};

}  // namespace pcv

#endif  // SRC_BACKEND_INCLUDE_PINTOOLINSTRUMENTPTHREADLIBRARY_H_
