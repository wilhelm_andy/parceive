/*! \file Member.h
 *  \brief This file contains the class for the Member entity.
 *
 * Copyright 2015 Siemens Technology and Services
 */
#ifndef SRC_BACKEND_INCLUDE_MEMBER_H_
#define SRC_BACKEND_INCLUDE_MEMBER_H_

#include <string>

#include "Entity.h"

namespace pcv {
namespace model {

/*!
 * \brief The Member class.
 */
class Member : public Entity<Member>
{
 public:
  Member() : Entity<Member>() {}
  explicit Member(Id_t id, std::string name) : id(id), name(std::move(name)) {}

  // members
  Id_t id;
  std::string name;
};

}  // namespace model
}  // namespace pcv

#endif  // SRC_BACKEND_INCLUDE_MEMBER_H_
