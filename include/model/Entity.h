/*! \file entity.h
 *  \brief This file contains the Entity class, a template class that
 *  enables the Singleton pattern and a thread-safe id incrementation for
 *  all inherited entities.
 *
 * Copyright 2015 Siemens Technology and Services
 */
#ifndef SRC_BACKEND_INCLUDE_ENTITY_H_
#define SRC_BACKEND_INCLUDE_ENTITY_H_

#include <atomic>
#include "Common.h"

namespace pcv {
namespace model {

/*!
 * \brief The Entity class.
 */
template <class Class>
class Entity
{
 public:
  typedef ::pcv::Id_t Id_t;

  static const Id_t EMPTY = static_cast<Id_t>(-1);

  /*!\brief get the singleton instance.*/
  static Class* getInstance()
  {
    static Class instance;
    return &instance;
  }

  /*!\brief! shift to the next free id and return it*/
  Id_t nextId() { return ++count_; }

  /*!\brief! get the current id */
  Id_t currentId() { return count_; }

 protected:
  Entity() : count_(0) {}

 private:
  std::atomic<Id_t> count_;  // the (thread-safe) id counter

  // prevent generated functions
  Entity(const Entity&);
  Entity& operator=(const Entity&);
};

}  // namespace model
}  // namespace pcv

#endif  // SRC_BACKEND_INCLUDE_ENTITY_H_
