/*! \file File.h
 *  \brief This file contains the class for the File entity.
 *
 * Copyright 2015 Siemens Technology and Services
 */
#ifndef SRC_BACKEND_INCLUDE_FILE_H_
#define SRC_BACKEND_INCLUDE_FILE_H_

#include <string>
#include <utility>

#include "Entity.h"

namespace pcv {
namespace model {

/*!
 * \brief The Function class.
 */
class File : public Entity<File>
{
 public:
  File() : Entity<File>() {}
  explicit File(Id_t id, std::string name, Id_t image) : id(id), name(std::move(name)), image(image)
  {}

  // members
  Id_t id;
  std::string name;
  Id_t image;
};

}  // namespace model
}  // namespace pcv

#endif  // SRC_BACKEND_INCLUDE_FILE_H_
