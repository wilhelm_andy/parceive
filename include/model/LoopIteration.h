/*! \file loopiteration.h
 *  \brief This file contains the class for the Loop Iteration entity.
 *
 * Copyright 2015 Siemens Technology and Services
 */
#ifndef SRC_BACKEND_INCLUDE_LOOPITERATION_H_
#define SRC_BACKEND_INCLUDE_LOOPITERATION_H_
#include "Entity.h"

namespace pcv {
namespace model {

/*!
 * \brief The Loop class.
 */
class LoopIteration : public Entity<LoopIteration>
{
 public:
  static const Id_t INITVAL = 0;                      // initial value
  static const Id_t NULLVAL = static_cast<Id_t>(-1);  // null value

  LoopIteration() : Entity<LoopIteration>() {}
  explicit LoopIteration(Id_t id, Id_t execution, unsigned iteration)
    : Entity<LoopIteration>(), id(id), execution(execution), iteration(iteration)
  {}

  // members
  Id_t id;
  Id_t execution;
  unsigned iteration;
};

}  // namespace model
}  // namespace pcv

#endif  // SRC_BACKEND_INCLUDE_LOOPITERATION_H_
