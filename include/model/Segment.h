/*! \file segment.h
 *  \brief This file contains the class for the Segment entity.
 *
 * Copyright 2015 Siemens Technology and Services
 */
#ifndef SRC_BACKEND_INCLUDE_SEGMENT_H_
#define SRC_BACKEND_INCLUDE_SEGMENT_H_
#include "Entity.h"

namespace pcv {
namespace model {

/*!
 * \brief The Segment class.
 */
class Segment : public Entity<Segment>
{
 public:
  /*!/brief The type of the segment */
  enum class Type { REGION = 0, LOOP = 1 };

  Segment() : Entity<Segment>() {}
  explicit Segment(Id_t id, Id_t call, Type type, Id_t loopIteration)
    : Entity<Segment>(), id(id), call(call), type(type), loopIteration(loopIteration)
  {}

  // members
  Id_t id;
  Id_t call;
  Type type;
  Id_t loopIteration;
};

}  // namespace model
}  // namespace pcv

#endif  // SRC_BACKEND_INCLUDE_SEGMENT_H_
