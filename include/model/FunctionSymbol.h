/*! \file Symbol.h
 *  \brief This file contains the class for the FunctionSymbol entity.
 *
 * Copyright 2015 Siemens Technology and Services
 */
#pragma once

#include "Entity.h"

namespace pcv {
namespace model {

/*!
 * \brief The Reference class.
 */
class FunctionSymbol : public Entity<FunctionSymbol>
{
 public:
  FunctionSymbol() : Entity<FunctionSymbol>() {}

  explicit FunctionSymbol(Id_t function, Symbol_t symbol) : function(function), symbol(symbol) {}

  // members
  Id_t function;
  Symbol_t symbol;
};

}  // namespace model
}  // namespace pcv
