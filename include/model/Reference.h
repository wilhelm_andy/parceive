/*! \file reference.h
 *  \brief This file contains the class for the Reference entity.
 *
 * Copyright 2015 Siemens Technology and Services
 */
#pragma once

#include <string>
#include <utility>

#include "Entity.h"

namespace pcv {
namespace model {

/*!
 * \brief The Reference class.
 */
class Reference : public Entity<Reference>
{
 public:
  static const Id_t NOMEMBER = static_cast<Id_t>(-1);

  using size_t = uint16_t;

  /*/brief! The type of the reference */
  enum class Type : uint8_t {
    STACK     = 1,
    HEAP      = 2,
    STATICVAR = 3,
    GLOBAL    = 4,
    UNKNOWN   = 5,
    REST      = 6
  };

  Reference() : Entity<Reference>() {}

  explicit Reference(Id_t id,
                     int size,
                     Type type,
                     std::string name,
                     Id_t allocInstruction,
                     uint32_t symbol,
                     Id_t image)
    : id(id),
      size(size),
      type(type),
      name(std::move(name)),
      allocInstruction(allocInstruction),
      symbol(symbol),
      image(image)
  {}

  // members
  Id_t id;
  int size;
  Type type;
  std::string name;
  Id_t allocInstruction;
  uint32_t symbol;
  Id_t image;
};

}  // namespace model
}  // namespace pcv
