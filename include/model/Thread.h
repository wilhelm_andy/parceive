/*! \file Thread.h
 *  \brief This file contains the class for the Thread entity.
 *
 * Copyright 2016 Siemens Technology and Services
 */
#ifndef SRC_BACKEND_INCLUDE_THREAD_H_
#define SRC_BACKEND_INCLUDE_THREAD_H_

#include <utility>

#include "Entity.h"
#include "pin.H"

namespace pcv {
namespace model {

/*!
 * \brief The Thread class.
 */
class Thread : public Entity<Thread>
{
 public:
  enum { MAIN = 0 };

  // types
  using cycles_t = uint64_t;

  Thread() : Entity<Thread>() {}
  explicit Thread(Id_t id,
                  cycles_t start,
                  cycles_t end,
                  Id_t createInstruction,
                  Id_t joinInstruction,
                  Id_t parentThread,
                  Id_t call)
    : Entity(),
      id(id),
      start(start),
      end(end),
      createInstruction(createInstruction),
      joinInstruction(joinInstruction),
      parentThread(parentThread),
      processId(0),
      call(call)
  {}

  // members
  Id_t id;
  cycles_t start;
  cycles_t end;
  Id_t createInstruction;
  Id_t joinInstruction;
  Id_t parentThread;
  int processId;
  Id_t call;
};

}  // namespace model
}  // namespace pcv

#endif  // SRC_BACKEND_INCLUDE_THREAD_H_
