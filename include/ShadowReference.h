/*
 * Copyright (c) 2016, Siemens AG. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SHADOWREFERENCE_H
#define SHADOWREFERENCE_H

#include <cstring>
#include <string>

#include "entities/ImageScopedId.h"
#include "model/Reference.h"

namespace pcv {

struct Reference_t;  // forward declaration

struct SymbolInfo {
  char *name;
  size_t size;
  entity::ImageScopedId classId;
  model::Reference::Id_t refId;
  Id_t symbolId;
  Id_t imageId;

  explicit SymbolInfo(const std::string &in_name,
                      size_t size,
                      const entity::ImageScopedId &classId,
                      Id_t symbolId,
                      Id_t imageId)
    : size(size),
      classId(classId),
      refId(model::Reference::EMPTY),
      symbolId(symbolId),
      imageId(imageId)
  {
    name = new char[in_name.size() + 1];
    strcpy(name, in_name.c_str());
  }

  ~SymbolInfo() { delete[] name; }
};

typedef struct Reference_t {
  model::Reference::Id_t id;
  model::Reference::Type type;
  S_ADDRUINT startAddress;
  SymbolInfo *info;
  model::Reference::Id_t parent;

  Reference_t(model::Reference::Id_t reference,
              model::Reference::Type type,
              SymbolInfo *info,
              model::Reference::Id_t parent = model::Reference::EMPTY)
    : id(reference), type(type), startAddress(0), info(info), parent(parent)
  {}
} Reference_t;

typedef struct StackReference_t : public Reference_t {
  StackReference_t(model::Reference::Id_t reference, SymbolInfo *info)
    : Reference_t(reference, model::Reference::Type::STACK, info)
  {}
} StackReference_t;

typedef struct AllocatedReference_t : public Reference_t {
  S_ADDRUINT allocationIP;

  AllocatedReference_t(model::Reference::Id_t reference, SymbolInfo *info, S_ADDRUINT allocationIP)
    : Reference_t(reference, model::Reference::Type::HEAP, info), allocationIP(allocationIP)
  {}
} AllocatedReference_t;

typedef struct GlobalReference_t : public Reference_t {
  S_ADDRUINT allocationIP;

  GlobalReference_t(model::Reference::Id_t reference, SymbolInfo *info)
    : Reference_t(reference, model::Reference::Type::GLOBAL, info)
  {}
} GlobalReference_t;

}  // namespace pcv

#endif  // SHADOWREFERENCE_H
