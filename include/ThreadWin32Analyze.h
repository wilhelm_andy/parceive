/*! \file PinToolAnalyzeWin32ThreadLibrary.h
 * \brief This file contains the PinToolAnalyzeWin32ThreadLibrary
 * class which contains the analysis functions to be called for the functions
 * instrumented in the Win32 Thread library
 */
#ifdef _WIN32
/* Copyright 2014 Siemens Technology and Services*/

#ifndef SRC_BACKEND_INCLUDE_PINTOOLANALYZEWIN32THREADLIBRARY_H_
#define SRC_BACKEND_INCLUDE_PINTOOLANALYZEWIN32THREADLIBRARY_H_

namespace pcv {

#pragma once
/* Included pin.H above analyzethreadlibrary.h because analyzethreadlibrary.h
 * file includes timer.h file, which defines #define _WIN32.
 * It conflicts with pin.H
 */
#include <map>
#include <sstream>
#include <string>
#include <vector>

#include "AnalyzeThreadLibrary.h"
#include "Query.h"
#include "ThreadData.h"
#include "pin.H"

namespace WINDOWS {
#include <Windows.h>
/*! Function checks the return value from WaitForSingleObject(), and returns
the appropriate value. */
unsigned int checkReturn(WINDOWS::DWORD returnValue);
/*! Function checks the return value from WaitForMultipleObjects(), and returns
the appropriate value. */
unsigned int checkReturnWaitForMultipleObjects(WINDOWS::DWORD returnValue, S_UINT handleCount);
/*! Function which returns the handle for which WaitForMultipleObjects()
succeeded, in case of WaitForMultipleObjects() not being a barrier. */
bool getHandleID(WINDOWS::DWORD returnValue, S_UINT handleCount, S_UINT *handleID);
}  // namespace WINDOWS

typedef struct WinThreadIDHandle {
  WINDOWS::HANDLE threadHandle;
  WINDOWS::DWORD threadID;
  WINDOWS::DWORD handleOwnerWindowsThreadID;
} WINTHREADIDHANDLE;

/*! \brief This class will contain methods related to analysis routines for
 * functions instrumented in the Win32 Thread Library.
 */
class PinToolAnalyzeWin32ThreadLibrary : public AnalyzeThreadLibrary
{
 private:
  /*! This is used to store a map from Synchronization primitive type to a
   * vector which contains the primitives of that type. The type of
   * synchronization primitives can be mutexes, critical sections, semaphores.
   * Check SYNC_PRIMITIVE for the data structure of the contents of the vector.
   */
  map<char, vector<SYNC_PRIMITIVE>> syncPrimitiveVectorMap;
  /*! This is used to store a map from the WINDOWS thread handle to the
   * thread ID assigned by WINDOWS
   */
  map<unsigned int, WINDOWS::HANDLE> threadHandleMap;
  vector<WINTHREADIDHANDLE> threadIDVector;
  /*! This is used to synchronize accesses to the syncPrimitiveVector map */
  PIN_RWMUTEX rwMutex;
  /*! This is used to synchronize accesses to the threadHandle map */
  PIN_RWMUTEX thMutex;
  /*! \brief This method searches the syncPrimitive in the syncPrimitiveVector
   * map. The syncPrimitiveType is used as key, to select the vector.
   * \return Position in the vector where the syncPrimitive is stored, -1, if
   * not found
   */
  int searchSyncPrimitivePosVectorMap(char syncPrimitiveType,
                                      /**< [IN] Selects the vector to which
                                      the syncPrimitive will be searched in */
                                      void **syncPrimitive
                                      /**< [IN] The syncPrimitive to be 
                                      searched */);
  /*! This is used to prevent Analysis routines being executed before/after main
   * routine is executed
   */
  bool mainSeen;

 public:
  PinToolAnalyzeWin32ThreadLibrary(void);
  ~PinToolAnalyzeWin32ThreadLibrary(void);

  /*! This method inserts the syncPrimitive data to the syncPrimitiveVector
   * map. The syncPrimitiveType is used as key. The createTime specifies when
   * the synchronization primitive was created.
   * \return void
   */
  void insertSyncPrimitiveVectorMap(char syncType,
                                    /**< [IN] Selects the vector to which
                                    the syncPrimitive will be inserted to */
                                    void **syncPrimitive,
                                    /**< [IN] The syncPrimitive to be
                                    inserted into the map */
                                    R_TIMERTYPE createTime
                                    /**< [IN] The time when the syncPrimitive 
                                    was created */);
  /*! This method searches the syncPrimitive in the syncPrimitiveVector
   * map. The syncPrimitiveType is used as key, to select the vector.
   * \return True, if success, else False
   * \post If the return value is true, createTime contains the time at which
   * the syncPrimitive was created, else, it is 0.
   */
  bool searchSyncPrimitiveVectorMap(char syncPrimitiveType,
                                    /**< [IN] Selects the vector to which
                                    the syncPrimitive will be searched in */
                                    void **syncPrimitive,
                                    /**< [IN] The syncPrimitive to be
                                    searched */
                                    R_TIMERTYPE *createTime
                                    /**< [OUT] The time at which the  
                                    syncPrimitive was created */);
  /*! This method removes the syncPrimitive from the vector of
   * synchronization primitives
   * \return True if the syncPrimitive is deleted, else False
   */
  bool removeSyncPrimitiveVectorMap(void **syncPrimitive
                                    /**< [IN] The syncPrimitive to be 
                                    removed */);
  /*! This method inserts the WINDOWS thread handle and the WINDOWS assigned
   * thread ID to the threadHandle map. The thread ID is used as key.
   * \return void
   */
  void insertThreadHandleMap(WINDOWS::HANDLE threadHandle,
                             /**< [IN] The WINDOWS thread handle for the thread */
                             WINDOWS::DWORD windowsThreadID,
                             /**< [IN] The thread ID assigned by WINDOWS */
                             WINDOWS::DWORD windowsParentThreadID
                             /**< [IN] The parent thread ID assigned by WINDOWS */);
  /*! This method searches the handle in the threadHandle map.
   * The thread ID is used as key, to select the handle.
   * \return True, if success, else False
   * \post If the return value is true, handle contains the thread handle,
   * else, it is 0.
   */
  bool searchThreadHandleMap(WINDOWS::DWORD windowsThreadID,
                             /**< [IN] The thread ID assigned by WINDOWS */
                             WINDOWS::HANDLE *handle
                             /**< [OUT] The WINDOWS thread handle */);
  /*! This method searches for the WINDOWS handle in the threadHandle map.
   * \return True, if success, else False
   * \post If the return value is true, handle contains the thread handle,
   * else, it is UINT_MAX.
   */
  bool searchThreadHandleMap(WINDOWS::HANDLE handle,
                             /**< [IN] The WINDOWS thread handle to be
                             searched */
                             WINDOWS::DWORD handleOwnerWindowsThreadID,
                             /**< [IN] The WINDOWS thread ID of the thread
                             invoking the searchThreadHandleMap() */
                             WINDOWS::DWORD *windowsThreadID
                             /**< [OUT] The WINDOWS thread ID associated
                             with the handle */);
  /*! This method removes the syncPrimitive from the vector of
   * synchronization primitives. The syncPrimitiveType acts as the key to select
   * the vector from the syncPrimitiveVectorMap
   * \return True if the syncPrimitive is deleted, else False
   */
  bool removeSyncPrimitiveVectorMap(char syncPrimitiveType,
                                    /**< [IN] Selects the vector to which
                                    the syncPrimitive will be deleted from */
                                    void **syncPrimitive
                                    /**< [IN] The syncPrimitive to be 
                                    removed */);
  /*! This method checks whether the returnIP does not belong to the
   * images or files that have been blacklisted.
   *
   * This method checks whether the returnIP, which has been passed to the
   * analysis routines of the Win32 Thread library, does not belong to the
   * images or files that have been blacklisted. Check the blacklist.xml for
   * the images and files that are currently in the blacklist. If a call to the
   * analysis routine comes from functions belonging to these images or files,
   * then the method returns false.
   * \return True, if the check succeeds, else False
   */
  bool checkReturnIP(ADDRINT returnIP
                     /**< [IN] The returnIP to be checked */);

  /*! This method is called after CreateMutexA() is executed. If the mainSeen
   * variable is set to true, then the mutexReturned is added to the
   * syncPrimitiveVectorMap.
   * \return void
   */
  friend void afterCreateMutexA(PinToolAnalyzeWin32ThreadLibrary *threadLibraryAnalysisObject,
                                /**< [IN] Pointer to the PinToolAnalyzeWin32ThreadLibrary object */
                                THREADID threadID,
                                /**< [IN] The ID of the thread which executed CreateMutexA() */
                                ADDRINT returnIP,
                                /**< [IN] The IP to which CreateMutexA() returns to */
                                WINDOWS::BOOL initialOwner,
                                /**< [IN] The initial owner of the mutex */
                                WINDOWS::LPCTSTR mutexName,
                                /**< [IN] The name of the mutex */
                                WINDOWS::HANDLE *mutexReturned
                                /**< [IN] The handle to the created mutex */);
  /*! This method is called after CreateMutexExA() is executed. If the mainSeen
   * variable is set to true, then the mutexReturned is added to the
   * syncPrimitiveVectorMap.
   * \return void
   */
  friend void afterCreateMutexExA(
    PinToolAnalyzeWin32ThreadLibrary *threadLibraryAnalysisObject,
    /**< [IN] Pointer to the PinToolAnalyzeWin32ThreadLibrary object */
    THREADID threadID,
    /**< [IN] The ID of the thread which executed CreateMutexExA() */
    ADDRINT returnIP,
    /**< [IN] The IP to which CreateMutexExA() returns to */
    WINDOWS::LPCTSTR mutexName,
    /**< [IN] The name of the mutex */
    WINDOWS::HANDLE *mutexReturned
    /**< [IN] The handle to the created mutex */);
  /*! This method is called after CreateMutexW() is executed. If the mainSeen
   * variable is set to true, then the mutexReturned is added to the
   * syncPrimitiveVectorMap.
   * \return void
   */
  friend void afterCreateMutexW(PinToolAnalyzeWin32ThreadLibrary *threadLibraryAnalysisObject,
                                /**< [IN] Pointer to the PinToolAnalyzeWin32ThreadLibrary object */
                                THREADID threadID,
                                /**< [IN] The ID of the thread which executed CreateMutexW() */
                                ADDRINT returnIP,
                                /**< [IN] The IP to which CreateMutexW() returns to */
                                WINDOWS::BOOL initialOwner,
                                /**< [IN] The initial owner of the mutex */
                                WINDOWS::LPCTSTR mutexName,
                                /**< [IN] The name of the mutex */
                                WINDOWS::HANDLE *mutexReturned
                                /**< [IN] The handle to the created mutex */);
  /*! This method is called after CreateMutexExW() is executed. If the mainSeen
   * variable is set to true, then the mutexReturned is added to the
   * syncPrimitiveVectorMap.
   * \return void
   */
  friend void afterCreateMutexExW(
    PinToolAnalyzeWin32ThreadLibrary *threadLibraryAnalysisObject,
    /**< [IN] Pointer to the PinToolAnalyzeWin32ThreadLibrary object */
    THREADID threadID,
    /**< [IN] The ID of the thread which executed CreateMutexExW() */
    ADDRINT returnIP,
    /**< [IN] The IP to which CreateMutexExW() returns to */
    WINDOWS::LPCTSTR mutexName,
    /**< [IN] The name of the mutex */
    WINDOWS::HANDLE *mutexReturned
    /**< [IN] The handle to the created mutex */);
  /*! This method is called before WaitForMultipleObjects() is executed.
   * \return void
   */
  friend void beforeWaitForMultipleObjects(
    PinToolAnalyzeWin32ThreadLibrary *threadLibraryAnalysisObject,
    /**< [IN] Pointer to the PinToolAnalyzeWin32ThreadLibrary object */
    THREADID threadID,
    /**< [IN] The ID of the thread which executed WaitForMultipleObjects() */
    unsigned int handleCount,
    /**< [IN] The number of handles */
    WINDOWS::HANDLE **handles,
    /**< [IN] Pointer to the array of handles */
    WINDOWS::BOOL bWaitAll,
    /**< [IN] Boolean variable which indicates, whether the thread
    which invokes WaitForMultipleObjects waits for all the objects to be
    signalled or not */
    ADDRINT returnIP
    /**< [IN] The IP to which WaitForSingleObject() returns to */);
  /*! This method is called after WaitForMultipleObjects() is executed.
   * \return void
   */
  friend void afterWaitForMultipleObjects(
    PinToolAnalyzeWin32ThreadLibrary *threadLibraryAnalysisObject,
    /**< [IN] Pointer to the PinToolAnalyzeWin32ThreadLibrary object */
    THREADID threadID,
    /**< [IN] The ID of the thread which executed WaitForMultipleObjects() */
    WINDOWS::DWORD retValue,
    /**< [IN] The return value of WaitForMultipleObjects() */
    ADDRINT returnIP
    /**< [IN] The IP to which WaitForMultipleObjects() returns to */);
  /*! This method is called before ReleaseMutex() is executed. The handle of
   * the mutex, which the thread is trying to release is stored in the TLS.
   * \return void
   */
  friend void beforeReleaseMutex(PinToolAnalyzeWin32ThreadLibrary *threadLibraryAnalysisObject,
                                 /**< [IN] Pointer to the PinToolAnalyzeWin32ThreadLibrary object */
                                 THREADID threadID,
                                 /**< [IN] The ID of the thread which executed ReleaseMutex() */
                                 WINDOWS::HANDLE *mutexName,
                                 /**< [IN] Pointer to the handle of the mutex */
                                 ADDRINT returnIP
                                 /**< [IN] The IP to which ReleaseMutex() returns to */);
  /*! This method is called after ReleaseMutex() is executed.
   * If retValue is false, then this function returns. If the retValue is
   * true, an entry is made into the backend that the mutex was released.
   * The mutex which was released is retrieved from the TLS.
   * \return void
   */
  friend void afterReleaseMutex(PinToolAnalyzeWin32ThreadLibrary *threadLibraryAnalysisObject,
                                /**< [IN] Pointer to the PinToolAnalyzeWin32ThreadLibrary object */
                                THREADID threadID,
                                /**< [IN] The ID of the thread which executed ReleaseMutex() */
                                WINDOWS::BOOL retValue,
                                /**< [IN] The return value of ReleaseMutex() */
                                ADDRINT returnIP
                                /**< [IN] The IP to which ReleaseMutex() returns to */);
  /*! This method is called before CloseHandle() is executed. The handle of
   * the mutex, which the thread is trying to close is stored in the TLS.
   * \return void
   */
  friend void beforeCloseHandle(PinToolAnalyzeWin32ThreadLibrary *threadLibraryAnalysisObject,
                                /**< [IN] Pointer to the PinToolAnalyzeWin32ThreadLibrary object */
                                THREADID threadID,
                                /**< [IN] The ID of the thread which executed CloseHandle() */
                                WINDOWS::HANDLE *mutexName,
                                /**< [IN] Pointer to the handle of the mutex */
                                ADDRINT returnIP
                                /**< [IN] The IP to which CloseHandle() returns to */);
  /*! This method is called after CloseHandle() is executed.
   * If retValue is NULL, then this function returns. If the retValue is not
   * NULL, the mutex is removed from the syncPrimitiveVectorMap. The mutex which
   * was closed is retrieved from the TLS.
   * \return void
   */
  friend void afterCloseHandle(PinToolAnalyzeWin32ThreadLibrary *threadLibraryAnalysisObject,
                               /**< [IN] Pointer to the PinToolAnalyzeWin32ThreadLibrary object */
                               THREADID threadID,
                               /**< [IN] The ID of the thread which executed CloseHandle() */
                               WINDOWS::BOOL retValue,
                               /**< [IN] The return value of CloseHandle() */
                               ADDRINT returnIP
                               /**< [IN] The IP to which CloseHandle() returns to */);

  /*! This method is called before InitializeCriticalSection() is executed.
   * The pointer to the critical section is stored in the TLS.
   * \return void
   */
  friend void beforeInitializeCriticalSection(
    PinToolAnalyzeWin32ThreadLibrary *threadLibraryAnalysisObject,
    /**< [IN] Pointer to the PinToolAnalyzeWin32ThreadLibrary object */
    THREADID threadID,
    /**< [IN] The ID of the thread which executed InitializeCriticalSection()
     */
    void **csName,
    /**< [IN] Pointer to the critical section */
    ADDRINT returnIP
    /**< [IN] The IP to which InitializeCriticalSection() returns to */);
  /*! This method is called after InitializeCriticalSection() is executed.
   * If the check of the returnIP through checkReturnIP() is true, the csName is
   * added to the syncPrimitiveVectorMap.
   * \return void
   */
  friend void afterInitializeCriticalSection(
    PinToolAnalyzeWin32ThreadLibrary *threadLibraryAnalysisObject,
    /**< [IN] Pointer to the PinToolAnalyzeWin32ThreadLibrary object */
    THREADID threadID,
    /**< [IN] The ID of the thread which executed InitializeCriticalSection()
     */
    ADDRINT returnIP
    /**< [IN] The IP to which InitializeCriticalSection() returns to */);
  /*! This method is called before InitializeCriticalSectionAndSpinCount() is
   * executed.
   * The pointer to the critical section is stored in the TLS.
   * \return void
   */
  friend void beforeInitializeCriticalSectionAndSpinCount(
    PinToolAnalyzeWin32ThreadLibrary *threadLibraryAnalysisObject,
    /**< [IN] Pointer to the PinToolAnalyzeWin32ThreadLibrary object */
    THREADID threadID,
    /**< [IN] The ID of the thread which executed
       InitializeCriticalSectionAndSpinCount() */
    void **csName,
    /**< [IN] Pointer to the critical section */
    ADDRINT returnIP
    /**< [IN] The IP to which InitializeCriticalSectionAndSpinCount() 
       * returns to */);
  /*! This method is called after InitializeCriticalSectionAndSpinCount() is
   * executed.
   * If the check of the returnIP through checkReturnIP() is true, the csName is
   * added to the syncPrimitiveVectorMap.
   * \return void
   */
  friend void afterInitializeCriticalSectionAndSpinCount(
    PinToolAnalyzeWin32ThreadLibrary *threadLibraryAnalysisObject,
    /**< [IN] Pointer to the PinToolAnalyzeWin32ThreadLibrary object */
    THREADID threadID,
    /**< [IN] The ID of the thread which executed
       InitializeCriticalSectionAndSpinCount() */
    ADDRINT returnIP
    /**< [IN] The IP to which InitializeCriticalSectionAndSpinCount() 
       * returns to */);
  /*! This method is called before EnterCriticalSection() is executed.
   * The pointer to the critical section is stored in the TLS.
   * \return void
   */
  friend void beforeEnterCriticalSection(
    PinToolAnalyzeWin32ThreadLibrary *threadLibraryAnalysisObject,
    /**< [IN] Pointer to the PinToolAnalyzeWin32ThreadLibrary object */
    THREADID threadID,
    /**< [IN] The ID of the thread which executed EnterCriticalSection() */
    void **csName,
    /**< [IN] Pointer to the critical section */
    ADDRINT returnIP
    /**< [IN] The IP to which EnterCriticalSection() returns to */);
  /*! This method is called after EnterCriticalSection() is executed.
   * Based on the check of the returnIP through checkReturnIP(), an entry is
   * made into the backend that the critical section was entered. The critical
   * section which was entered is retrieved from the TLS
   * \return void
   */
  friend void afterEnterCriticalSection(
    PinToolAnalyzeWin32ThreadLibrary *threadLibraryAnalysisObject,
    /**< [IN] Pointer to the PinToolAnalyzeWin32ThreadLibrary object */
    THREADID threadID,
    /**< [IN] The ID of the thread which executed EnterCriticalSection() */
    ADDRINT returnIP
    /**< [IN] The IP to which EnterCriticalSection() returns to */);
  /*! This method is called before TryEnterCriticalSection() is executed.
   * The pointer to the critical section is stored in the TLS.
   * \return void
   */
  friend void beforeTryEnterCriticalSection(
    PinToolAnalyzeWin32ThreadLibrary *threadLibraryAnalysisObject,
    /**< [IN] Pointer to the PinToolAnalyzeWin32ThreadLibrary object */
    THREADID threadID,
    /**< [IN] The ID of the thread which executed TryEnterCriticalSection() */
    void **csName,
    /**< [IN] Pointer to the critical section */
    ADDRINT returnIP
    /**< [IN] The IP to which TryEnterCriticalSection() returns to */);
  /*! This method is called after TryEnterCriticalSection() is executed.
   * Based on the check of the returnIP through checkReturnIP() and if
   * TryEnterCriticalSection() succeeds, an entry is made into the backend
   * that the critical section was entered. The critical section which
   * was entered is retrieved from the TLS.
   * \return void
   */
  friend void afterTryEnterCriticalSection(
    PinToolAnalyzeWin32ThreadLibrary *threadLibraryAnalysisObject,
    /**< [IN] Pointer to the PinToolAnalyzeWin32ThreadLibrary object */
    THREADID threadID,
    /**< [IN] The ID of the thread which executed TryEnterCriticalSection() */
    WINDOWS::BOOL retValue,
    /**< [IN] The return value of TryEnterCriticalSection() */
    ADDRINT returnIP
    /**< [IN] The IP to which TryEnterCriticalSection() returns to */);
  /*! This method is called before LeaveCriticalSection() is executed.
   * The pointer to the critical section is stored in the TLS.
   * \return void
   */
  friend void beforeLeaveCriticalSection(
    PinToolAnalyzeWin32ThreadLibrary *threadLibraryAnalysisObject,
    /**< [IN] Pointer to the PinToolAnalyzeWin32ThreadLibrary object */
    THREADID threadID,
    /**< [IN] The ID of the thread which executed LeaveCriticalSection() */
    void **csName,
    /**< [IN] Pointer to the critical section */
    ADDRINT returnIP
    /**< [IN] The IP to which LeaveCriticalSection() returns to */);
  /*! This method is called after LeaveCriticalSection() is executed.
   * Based on the check of the returnIP through checkReturnIP(), an entry is
   * made into the backend that the critical section was left. The critical
   * section which was left is retrieved from the TLS.
   * \return void
   */
  friend void afterLeaveCriticalSection(
    PinToolAnalyzeWin32ThreadLibrary *threadLibraryAnalysisObject,
    /**< [IN] Pointer to the PinToolAnalyzeWin32ThreadLibrary object */
    THREADID threadID,
    /**< [IN] The ID of the thread which executed LeaveCriticalSection() */
    ADDRINT returnIP
    /**< [IN] The IP to which LeaveCriticalSection() returns to */);
  /*! This method is called before DeleteCriticalSection() is executed.
   * The pointer to the critical section is stored in the TLS.
   * \return void
   */
  friend void beforeDeleteCriticalSection(
    PinToolAnalyzeWin32ThreadLibrary *threadLibraryAnalysisObject,
    /**< [IN] Pointer to the PinToolAnalyzeWin32ThreadLibrary object */
    THREADID threadID,
    /**< [IN] The ID of the thread which executed DeleteCriticalSection() */
    void **csName,
    /**< [IN] Pointer to the critical section */
    ADDRINT returnIP
    /**< [IN] The IP to which DeleteCriticalSection() returns to */);
  /*! This method is called after DeleteCriticalSection() is executed.
   * Based on the check of the returnIP through checkReturnIP(), the critical
   * section is removed from the syncPrimitiveVectorMap. The critical
   * section which was deleted is retrieved from the TLS.
   * \return void
   */
  friend void afterDeleteCriticalSection(
    PinToolAnalyzeWin32ThreadLibrary *threadLibraryAnalysisObject,
    /**< [IN] Pointer to the PinToolAnalyzeWin32ThreadLibrary object */
    THREADID threadID,
    /**< [IN] The ID of the thread which executed DeleteCriticalSection() */
    ADDRINT returnIP
    /**< [IN] The IP to which DeleteCriticalSection() returns to */);

  /*! This method is called after CreateThread() is executed. The parent thread
   * ID is got from WINDOWS::GetCurrentThreadId(), the child thread ID is got
   * from the created thread handle using WINDOWS::GetThreadId(threadHandle). An
   * entry is made into the backend that the thread was created.
   * \return void
   */
  friend void afterCreateThread(PinToolAnalyzeWin32ThreadLibrary *threadLibraryAnalysisObject,
                                /**< [IN] Pointer to the PinToolAnalyzeWin32ThreadLibrary object */
                                THREADID threadID,
                                /**< [IN] The ID of the thread which executed CreateThread() */
                                WINDOWS::HANDLE threadHandle
                                /**< [IN] The handle of the thread created by CreateThread() */);

  /*! This method is a wrapper to WaitForSingleObject(). It internally calls
   * WaitForSingleObject. This wrapper was introduced, because, we are not able
   * to capture routine leave for WaitForSingleObject in 64-bit applications.
   * This function replaces calls to WaitForSingleObject. Any call to
   * WaitForSingleObject in the application is passed to this wrapper.
   */
  friend WINDOWS::DWORD waitForSingleObjectWrapper(
    PinToolAnalyzeWin32ThreadLibrary *threadLibraryAnalysisObject,
    /**< [IN] Pointer to the PinToolAnalyzeWin32ThreadLibrary object */
    CONTEXT *ctxt,
    /**< [IN] Pointer to the register context */
    AFUNPTR pf_waitForSingleObject,
    /**< [IN] Function pointer to the original WaitForSingleObject routine */
    THREADID threadID,
    /**< [IN] The ID of the thread invoking WaitForSingleObject */
    WINDOWS::HANDLE handle,
    /**< [IN] The handle passed as parameter to WaitForSingleObject */
    WINDOWS::DWORD time,
    /**< [IN] The timeout period */
    S_ADDRINT returnIP
    /**< [IN] The IP to which the function returns to */);

  /*! This method is called from PinToolInstrumentWin32ThreadLibrary's
   * instlibMainEnter(). This sets the mainSeen variable to true.
   * \return void
   */
  friend void tlibMainEnter(PinToolAnalyzeWin32ThreadLibrary *threadLibraryAnalysisObject
                            /**< [IN] Pointer to the PinToolAnalyzeWin32ThreadLibrary object */);
  /*! This method is called from PinToolInstrumentWin32ThreadLibrary's
   * instlibMainLeave(). This sets the mainSeen variable to false.
   * \return void
   */
  friend void tlibMainLeave(PinToolAnalyzeWin32ThreadLibrary *threadLibraryAnalysisObject
                            /**< [IN] Pointer to the PinToolAnalyzeWin32ThreadLibrary object */);
};

#endif

}  // namespace pcv

#endif  // SRC_BACKEND_INCLUDE_PINTOOLANALYZEWIN32THREADLIBRARY_H_
