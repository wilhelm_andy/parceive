/*! \file loopstate.h
 *  \brief This file contains the structure for Loop State.
 *
 * Copyright 2015 Siemens Technology and Services
 */
#ifndef SRC_BACKEND_INCLUDE_LOOPSTATE_H_
#define SRC_BACKEND_INCLUDE_LOOPSTATE_H_
#include "Timer.h"
#include "model/Loop.h"
#include "model/LoopExecution.h"
#include "model/LoopIteration.h"

namespace pcv {

using model::Loop;
using model::LoopExecution;
using model::LoopIteration;

/*!
 * \brief The LoopState structure.
 */
struct LoopState {
  Loop::Id_t loopId;
  LoopExecution::Id_t executionId;
  LoopIteration::Id_t iterationId;
  unsigned int executionNo;
  unsigned int iterationNo;
  R_TIMERTYPE startTime;
  bool touched;

  explicit LoopState(Loop::Id_t loopId,
                     LoopExecution::Id_t executionId,
                     LoopIteration::Id_t iterationId,
                     unsigned int executionNo,
                     unsigned int iterationNo,
                     R_TIMERTYPE startTime)
    : loopId(loopId),
      executionId(executionId),
      iterationId(iterationId),
      executionNo(executionNo),
      iterationNo(iterationNo),
      startTime(startTime),
      touched(false)
  {}

 private:
  LoopState() {}
};

}  // namespace pcv

#endif  // SRC_BACKEND_INCLUDE_LOOPSTATE_H_
