#ifndef SQLITE_H
#define SQLITE_H

#include <pin.H>
#include <memory>
#include <string>
#include <utility>
#include "Timer.h"

struct sqlite3;
struct sqlite3_stmt;

namespace pcv {
namespace SQLite {

class Connection;
class Statement;
class StatementBinder;

class Connection : public std::enable_shared_from_this<Connection>
{
 public:
  Connection(const char *location, bool forceCreate = false);
  virtual ~Connection();

  const char *getErrorMessage();
  std::shared_ptr<Statement> makeStatement(const char *sql);

  int64_t lastInsertedROWID();

  void execute(const char *sql);

  void lock();
  void unlock();

 private:
  sqlite3 *db{};

  PIN_MUTEX mutex{};

  friend class Statement;
};

class NullClass
{
};
extern NullClass SQLNULL;

class Statement : public std::enable_shared_from_this<Statement>
{
 public:
  Statement(std::shared_ptr<Connection> connection, const char *sql);
  virtual ~Statement();

  void bind(int col, uint64_t val) { this->bind(col, (int64_t)val); }
  void bind(int, int64_t);
  void bind(int, uint32_t);
  void bind(int, int);
  void bind(int, const std::string &);
  void bind(int, const char *);
  void bind(int, const unsigned long long&);
  void bindNULL(int);

  void checkColumn(int);
  int columnInt(int);
  unsigned long long columnULong(int);
  std::string columnString(int);

  template <typename T>
  T column(int);

  void execute();
  int64_t executeInsert();

  void reset();
  void clearBindings();
  void step();
  bool stepRow();

  void resetUnlocked();
  void clearBindingsUnlocked();
  void stepUnlocked();

  bool stepRowUnlocked();

 private:
  sqlite3_stmt *stmt{};
  std::shared_ptr<Connection> connection;
  int columnCount;
  int lastBound;
};

template <>
inline int Statement::column<int>(int col)
{
  return columnInt(col);
}
template <>
inline unsigned long long Statement::column<unsigned long long>(int col)
{
  return columnULong(col);
}
template <>
inline std::string Statement::column<std::string>(int col)
{
  return columnString(col);
}

class StatementBinder
{
 public:
  StatementBinder(std::shared_ptr<Statement> stmt) : at(1), stmt(stmt) {}
  template <typename T>
  StatementBinder &operator<<(const T &obj)
  {
    stmt->bind(at++, obj);

    return *this;
  }

 private:
  int at;
  std::shared_ptr<Statement> stmt;
};

template <>
inline StatementBinder &StatementBinder::operator<<<NullClass>(const NullClass &obj)
{
  stmt->bindNULL(at++);
  return *this;
}

template <typename T>
StatementBinder operator<<(Statement &stmt, const T &obj)
{
  StatementBinder binder(stmt.shared_from_this());

  binder << obj;

  return binder;
}

template <typename T>
StatementBinder operator<<(std::shared_ptr<Statement> &stmt, const T &obj)
{
  StatementBinder binder(stmt);

  binder << obj;

  return binder;
}

class StatementReader
{
 public:
  StatementReader(std::shared_ptr<Statement> stmt) : at(0), stmt(stmt) {}
  template <typename T>
  StatementReader &operator>>(T &obj)
  {
    obj = stmt->column<T>(at++);

    return *this;
  }

 private:
  int at;
  std::shared_ptr<Statement> stmt;
};

template <typename T>
StatementReader operator>>(Statement &stmt, T &obj)
{
  StatementReader reader(stmt.shared_from_this());

  reader >> obj;

  return reader;
}

template <typename T>
StatementReader operator>>(std::shared_ptr<Statement> &stmt, T &obj)
{
  StatementReader reader(stmt);

  reader >> obj;

  return reader;
}

}  // namespace SQLite
}  // namespace pcv
#endif  // SQLITE_H
