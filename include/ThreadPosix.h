/*! \file PinToolInstrumentPThreadLibrary.h
 *  \brief This file contains the PinToolInstrumentPThreadLibrary class
 * which contains instrumenting functions for PThread library
 */

/* Copyright 2014 Siemens Technology and Services*/

#ifndef SRC_BACKEND_INCLUDE_PINTOOLINSTRUMENTPTHREADLIBRARY_H_
#define SRC_BACKEND_INCLUDE_PINTOOLINSTRUMENTPTHREADLIBRARY_H_

#pragma once
#include <map>

#include <Thread.h>
#include <set>

#include "model/Call.h"
#include "model/Function.h"
#include "model/Instruction.h"
#include "model/Thread.h"

namespace pcv {

/**
 * @brief This class extends the InstrumentThreadLibrary class and provides
 * instrumentation routines for the PThread library
 */
class ThreadPosix : public pcv::Thread
{
 public:
  ThreadPosix(Parceive* parceive);
  ~ThreadPosix() override;

  void initialize() override;
  void beforeInstrumentImage(IMG) override;
  void instrumentImage(IMG) override;
  void instrumentTrace(IMG, TRACE, bool) override;

 private:
  /** @brief Instrument pthread_create   */
  void instrumentCreate(IMG img /**< [IN] The PIN image. */);

  /** @brief Instrument pthread_join   */
  void instrumentJoin(IMG img /**< [IN] The PIN image. */);

  /** @brief Instrument pthread_mutex_lock   */
  void instrumentLock(IMG img /**< [IN] The PIN image. */);

  /** @brief Instrument pthread_mutex_unlock   */
  void instrumentUnlock(IMG img /**< [IN] The PIN image. */);

  /*! \brief the replacement function for pthread_create */
  static int w_pthreadCreate(ThreadPosix* pThis,
                             CONTEXT* context,
                             pthread_t* thread,
                             pthread_attr_t* attr,
                             void* (*start_routine)(void*),
                             void* arg,
                             AFUNPTR orig_funptr);

  /*! \brief the replacement function for pthread_join */
  static int w_pthreadJoin(
    ThreadPosix* pThis, CONTEXT* context, pthread_t thread, void** result, AFUNPTR orig_funptr);

  /*! \brief analysis routine for thread start */
  static void an_threadStart(
    ThreadPosix* pThis, THREADID threadId, ADDRINT* arg, S_ADDRUINT returnIP, UINT32 rtnId);

  /*! \brief analysis routine for mutex_lock */
  static void an_mutexLock(ThreadPosix* pThis, THREADID threadId, ADDRINT* arg);

  /*! \brief analysis routine for mutex_unlock */
  static void an_mutexUnlock(ThreadPosix* pThis, THREADID threadId, ADDRINT* arg);

  /**
   * @brief Returns the id of the (possibly created) mutex reference;
   */
  model::Reference::Id_t getReferenceId(
    THREADID threadId, /**< [IN] The thread id that called the function. */
    ADDRINT* mutex     /**< [IN] The address of the aquired mutex. */
  );

  inline void lock() { PIN_RWMutexWriteLock(&_lock); }
  inline void unlock() { PIN_RWMutexUnlock(&_lock); }

  std::set<ADDRINT> threadStartAddresses;
  std::map<model::Instruction::Id_t, pthread_t*> insPtMap;
  std::map<pthread_t, THREADID> ptTidMap;

  PIN_RWMUTEX _lock;
};

}  // namespace pcv

#endif  // SRC_BACKEND_INCLUDE_PINTOOLINSTRUMENTPTHREADLIBRARY_H_
