/*! \file Writer.h
 *  \brief This file contains the abstract Writer class, which contains virtual
 * functions for logging purposes
 */

/* Copyright 2014 Siemens Technology and Services*/
#ifndef SRC_BACKEND_INCLUDE_WRITER_H_
#define SRC_BACKEND_INCLUDE_WRITER_H_

#pragma once

#include "DataModel.h"

namespace pcv {

/*! \brief Writer class provides virtual functions for setting up the
 * environment for the Writer object, for logging purposes
 *
 * Writer is implemented as a buffer, to avoid writing logs everytime
 * When a log is generated, it is written into the buffer which is
 * flushed periodically, or when the buffer becomes full
 */
class Writer
{
 public:
  Writer()          = default;
  virtual ~Writer() = default;

  /*! \brief Functions to add an entry
   *
   * \return True, if success, else False
   */
  virtual void insert(const model::File &&)          = 0;
  virtual void insert(const model::Image &&)         = 0;
  virtual void insert(const model::Function &&)      = 0;
  virtual void insert(const model::Thread &&)        = 0;
  virtual void insert(const model::Call &&)          = 0;
  virtual void insert(const model::Segment &&)       = 0;
  virtual void insert(const model::Instruction &&)   = 0;
  virtual void insert(const model::Loop &&)          = 0;
  virtual void insert(const model::LoopExecution &&) = 0;
  virtual void insert(const model::LoopIteration &&) = 0;
  virtual void insert(const model::Member &&)        = 0;
  virtual void insert(const model::Access &&)        = 0;
  virtual void insert(const model::Reference &&)     = 0;

  virtual void remove(const model::LoopIteration &&) = 0;
};

}  // namespace pcv

#endif  // SRC_BACKEND_INCLUDE_WRITER_H_
