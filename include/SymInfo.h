/*! \file SymInfo.h
 *  \brief This file contains the abstract class for Symbol resolution
 */

/* Copyright 2014 Siemens Technology and Services*/

#ifndef SRC_BACKEND_INCLUDE_SYMINFO_H_
#define SRC_BACKEND_INCLUDE_SYMINFO_H_

#include <entities/File.h>
#include <cstdint>
#include <memory>
#include <set>
#include <string>
#include <unordered_map>
#include <vector>

#include "Common.h"
#include "Context.h"
#include "entities/Class.h"
#include "entities/ImageScopedId.h"
#include "entities/Routine.h"
#include "entities/SoftwareEntity.h"
#include "entities/Variable.h"

namespace pcv {

/*!
 * \brief The SymInfo class is an abstract class which helps in
 * resolving symbols - function, variable(data) and instruction.
 * It can be extended to support different symbol libraries like
 * DbgHlp.lib which reads COFF based debug information, and Dwarf
 * which reads ELF based debug information
 */
class SymInfo
{
 public:
  using className_t = std::string;
  using classId_t   = uint64_t;
  using line_t      = uint64_t;
  using offset_t    = uint64_t;

  SymInfo()          = default;
  virtual ~SymInfo() = default;

  virtual const pcv::Context &getContext() const = 0;

  /*!
   * @returns A vector of symbol information regarding the found global
   * variables of an image.
   */
  virtual std::unique_ptr<std::vector<const entity::Variable *>> getGlobalVariables(
    Id_t imageId) const = 0;

  /*!
   * @returns A vector of symbol information regarding the found static
   * variables of an image.
   */
  virtual std::unique_ptr<std::vector<const entity::Variable *>> getStaticVariables(
    Id_t imageId) const = 0;

  virtual const entity::Routine *getRoutine(const Id_t imageId,
                                            const std::string rtnName) const = 0;

  virtual std::vector<const entity::File *> getFiles(Id_t imageId) = 0;

  /*!
   * @returns The symbol information for a member variable on a specific
   * class-id/offset if found, otherwise nullptr.
   */
  virtual const entity::Variable *getMemberVariable(
    const entity::ImageScopedId &clsId, /**< [IN] The class id.*/
    entity::Class::offset_t offset /**< [IN] The offset to look for the member var.*/) const = 0;

  /*!
   * @brief Adds # of namespaces in routine names accordingly to
   * <#namespaces><origRtnName>.
   * @return The refined routine name.
   */
  virtual std::string refineRtnName(
    const Id_t imageId, /**< [IN] The image id*/
    const std::string &rtnName /**< [IN] Undecorated routine name in [scope::]name form*/) = 0;

  /*!
   * @brief Initializes the symbol information handler with information
   * contained in the file.
   * @return true if successful else false
   */
  virtual bool readSymInfo(
    const std::string &fileName, /**< [IN] The file that contains debug information*/
    const Id_t imageId,          /**< [IN] The corresponding image id*/
    const offset_t
      offset /**< [IN] Offset from the image's link time to its load time address.*/) = 0;

  /*!
   * @returns true if binary is compiled with optimizations (that may obfuscate
   * debug symbols).
   */
  virtual bool isBinaryOptimized(
    const std::string &fileName /**< [IN] The file that contains debug information*/) = 0;

  /*!
   * @brief Clears the cache needed during instrumentation.
   */
  virtual void clearSymInfo() = 0;

  /*!
   * @brief Returns the class id for a respective constructor.
   * @return The class id.
   */
  virtual entity::ImageScopedId getUniqueClassId(
    Id_t imageId, /**< [IN] The corresponding image id*/
    const std::string &rtnName /**< [IN] Routine name of the constructor.*/) const = 0;

  /*!
   * @brief Fills the line number and filename information (if not nullptr) for
   * a sourelocation at a given address.
   * @returns true if the information is found, otherwise false.
   */
  virtual bool getSourceLocation(const S_ADDRUINT &ip, /**< [in] instruction address */
                                 int32_t *line,        /**< [out] source line */
                                 std::string *fileName /**< [out] file name */) const = 0;
};

}  // namespace pcv

#endif  // SRC_BACKEND_INCLUDE_SYMINFO_H_
