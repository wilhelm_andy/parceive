namespace pcv {

#ifdef _WIN32
/*! \brief ThreadDataThreadTLS class maintains the Thread-specific information
 * related to the thread data like Windows-assigned Thread ID, thread ID
 * assigned by Pin. This data is stored in Pin's TLS
 *
 * The class currently contains the following members
 *     The threadID assigned by WINDOWS
 *     The threadID assigned by Pin framework
 *     The number of handles passed to WaitForMultipleObjects()
 *     The value of bWaitAll parameter passed to WaitForMultipleObjects()
 * Note: The _pad specifies the padding to be done, so that the cache line are
 * not shared among different threads. If, you add members to this class, check
 * the size of the class in 32-bit and 64-bit applications, and update the pad
 * size.
 */
class ThreadDataThreadTLS
{
 public:
  ThreadDataThreadTLS() : osThreadID(0), pinThreadID(0), handleCount(0), bWaitAll(false) {}
  /*! The thread ID assigned by Windows */
  S_DWORD osThreadID;
  /*! The thread ID assigned by Pin */
  S_UINT pinThreadID;
  /*! Stores the handleCount passed to WaitForMultipleObjects() */
  S_UINT handleCount;
  /*! Stores the bWaitAll variable of WaitForMultipleObjects() */
  bool bWaitAll;
  /*! The padding to be added so that two threads will not access the same
   * cache line. This is suggested in the Intel Pin documentation
   */
  UINT8 _pad[64 - (3 * sizeof(S_UINT)) - sizeof(bool)];
};
class ThreadTLS
{
 public:
  ThreadTLS()
    : reAllocAddress(0), size(0), lineNo(0), csName(NULL), mutexName(NULL), lastReferenceId(0)
  {}
  /*! The address passed to realloc */
  S_ADDRUINT reAllocAddress;
  /*! The size parameter passed to functions handling dynamic memory allocation
   */
  S_UINT size;
  /*! The value of the base register during the function call */
  S_ADDRUINT baseRegValue{};
  /*! The routine ID of the caller function */
  Function::Id_t callerRoutineID{};
  /*! The line number of the callee, where the call was generated */
  S_UINT lineNo;
  /*! The pointer to the critical section */
  void *csName;
  /*! The pointer to the mutex handle */
  void *mutexName;
  /*! Variable which holds the reference id of the currently allocated memory */
  S_ADDRUINT lastReferenceId;
  /*! The padding to be added so that two threads will not access the same
   * cache line. This is suggested in the Intel Pin documentation
   */
  UINT8
  _pad[64 - sizeof(S_ADDRUINT) - (2 * sizeof(S_UINT)) - sizeof(bool) - (4 * (sizeof(void *)))]{};
};
#endif

#ifdef _WIN32
/*! \brief ThreadDataThreadTLS class maintains the Thread-specific information
 * related to the thread data like Windows-assigned Thread ID, thread ID
 * assigned by Pin. This data is stored in Pin's TLS
 *
 * The class currently contains the following members
 *     The threadID assigned by WINDOWS
 *     The threadID assigned by Pin framework
 *     The number of handles passed to WaitForMultipleObjects()
 *     The value of bWaitAll parameter passed to WaitForMultipleObjects()
 * Note: The _pad specifies the padding to be done, so that the cache line are
 * not shared among different threads. If, you add members to this class, check
 * the size of the class in 32-bit and 64-bit applications, and update the pad
 * size.
 */
class ThreadDataThreadTLS
{
 public:
  ThreadDataThreadTLS() : osThreadID(0), pinThreadID(0), handleCount(0), bWaitAll(false) {}
  /*! The thread ID assigned by Windows */
  S_DWORD osThreadID;
  /*! The thread ID assigned by Pin */
  S_UINT pinThreadID;
  /*! Stores the handleCount passed to WaitForMultipleObjects() */
  S_UINT handleCount;
  /*! Stores the bWaitAll variable of WaitForMultipleObjects() */
  bool bWaitAll;
  /*! The padding to be added so that two threads will not access the same
   * cache line. This is suggested in the Intel Pin documentation
   */
  UINT8 _pad[64 - (3 * sizeof(S_UINT)) - sizeof(bool)];
};
#endif

}  // namespace pcv
