/*! \file loopmanager.h
 *  \brief This file contains the Loop Manager class.
 *
 * Copyright 2015 Siemens Technology and Services
 */
#ifndef SRC_BACKEND_INCLUDE_LOOPMANAGER_H_
#define SRC_BACKEND_INCLUDE_LOOPMANAGER_H_
#include <map>
#include <set>
#include <stack>

#include "LoopState.h"
#include "model/Call.h"
#include "model/Loop.h"
#include "model/LoopExecution.h"
#include "model/LoopIteration.h"

namespace pcv {

/*!\brief The LoopManager class.*/
class LoopManager
{
 public:
  /*!\brief get the singleton instance */
  static LoopManager *getInstance();

  /*!\brief
   *	enterLoop needs to be called whenever a new loop (iteration) is entered.
   * The method assigns id's to the loop, the execution, and the iteration.
   *
   * \return true if a new loop has entered, otherwise false.
   */
  bool enterLoop(model::Call::Id_t callId, /**< [IN] The call id */
                 model::Loop::Id_t loopId /**< [IN] The loop id */);

  /*!\brief
   *	exitLoop needs to be called whenever a new loop (execution) is left.
   */
  void exitLoop(model::Call::Id_t callId, /**< [IN] The call id */
                model::Loop::Id_t loopId /**< [IN] The loop id */);

  /*!\brief
   * returns the top of the stack.
   */
  const LoopState &top(model::Call::Id_t /**< [IN] The call id */) const;

  /*!\brief
   * returns true if the stack is empty, otherwise false.
   */
  bool empty(model::Call::Id_t callId /**< [IN] The call id */) const;

  void touch(model::Call::Id_t callId /**< [IN] The call id */);

 private:
  // member variables
  std::map<model::Call::Id_t, std::stack<LoopState>> loopStack_;  // loop stack
  std::set<model::Loop::Id_t> loops_;                             // set of visitedloops
  std::map<model::Loop::Id_t, unsigned int> executionCount_;      // execution counter

  model::Loop *loopI_;
  model::LoopExecution *loopExecutionI_;
  model::LoopIteration *loopIterationI_;

  // prevent construction
  LoopManager()
  {
    loopI_          = model::Loop::getInstance();
    loopExecutionI_ = model::LoopExecution::getInstance();
    loopIterationI_ = model::LoopIteration::getInstance();
  }

  // prevent generated functions
  LoopManager(const LoopManager &);
  LoopManager &operator=(const LoopManager &);
};

}  // namespace pcv

#endif  // SRC_BACKEND_INCLUDE_LOOPMANAGER_H_
