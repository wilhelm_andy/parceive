//
// Created by wilhelma on 1/23/17.
//

#pragma once

#include <cstdint>
#include <regex>

namespace pcv {

class FileFilter
{
 public:
  explicit FileFilter(const std::string &includePattern, const std::string &excludePattern);

  bool isIncluded(const std::string &fileName) const;
  bool isIncluded(const std::string &fileName, uint32_t lineNo) const;
  bool isExcluded(const std::string &fileName) const;

  void assign(const std::string &include, const std::string &exclude);

  /* disable copy/move construction and assignment operators */
  FileFilter(const FileFilter &) = delete;
  FileFilter(FileFilter &&)      = delete;
  FileFilter &operator=(const FileFilter &) = delete;
  FileFilter &operator=(FileFilter &&) = delete;

 private:
  std::regex include_;
  std::regex exclude_;
};

}  // namespace pcv
