//
// Created by wilhelma on 12/22/16.
//

#pragma once

#include <string>
#include <utility>

#ifdef _WIN32
#define Dwarf_Off int32_t
#elif defined _WIN64
#define Dwarf_Off int64_t
#else
#include <libdwarf.h>
#endif

#include "Common.h"
#include "entities/ImageScopedId.h"

namespace pcv {
namespace entity {

struct Namespace;

/// @brief The type of a software entity.
enum class EntityType : int {
  None      = 0,
  Class     = 1 << 0,
  Routine   = 1 << 1,
  Variable  = 1 << 2,
  Namespace = 1 << 3,
  Image     = 1 << 4,
  File      = 1 << 5,
  All       = Class | Routine | Variable | Namespace | Image
};

inline EntityType operator&(EntityType __x, EntityType __y)
{
  return static_cast<EntityType>(static_cast<int>(__x) & static_cast<int>(__y));
}

inline EntityType operator|(EntityType __x, EntityType __y)
{
  return static_cast<EntityType>(static_cast<int>(__x) | static_cast<int>(__y));
}

using Id_t = ::pcv::Id_t;

struct Artifact_t;
struct File;
struct Class;
struct Image;

/// @brief The base structure of a software entitiy.
struct SoftwareEntity {
  using name_t   = std::string;
  using size_t   = uint64_t;
  using file_t   = std::string;
  using line_t   = uint32_t;
  using offset_t = int64_t;

  Id_t id{};
  name_t name;
  name_t full_name;
  size_t size{};
  Image *img{};
  Namespace *nmsp{};
  Class *cls{};
  File *file{};
  line_t line{};

  SoftwareEntity() = default;
  /// @brief Destructor.
  virtual ~SoftwareEntity() = default;

  /// @brief Constructor.
  /// @param id The id of the symbol information.
  /// @param name The name of the entity.
  /// @param img The containing image.
  /// @param nmsp The containing namespace.
  /// @param cls The containing class.
  /// @param file The name of the containing file.
  /// @param line The line of the declaration.
  explicit SoftwareEntity(const Id_t id,
                          const name_t &name,
                          Image *img,
                          Namespace *nmsp,
                          Class *cls,
                          File *file,
                          const line_t line,
                          const name_t &full_name = "");

  /*!
   * @brief Returns the type of the concrete software entity.
   * @return The EntityType.
   */
  virtual EntityType getEntityType() const { return EntityType::None; }

  ImageScopedId getUniqueId() const;
};

}  // namespace entity
}  // namespace pcv
