//
// Created by wilhelma on 12/22/16.
//

#pragma once

#include <climits>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include "Class.h"
#include "Image.h"
#include "Namespace.h"
#include "SoftwareEntity.h"

namespace pcv {
namespace entity {

struct Routine;
struct Variable;

struct Class : public SoftwareEntity {
  std::vector<Routine *> methods;
  std::vector<Variable *> members;
  std::vector<Class *> baseClasses;
  std::vector<Class *> inheritClasses;
  std::vector<Class *> nestedClasses;
  std::vector<Class *> composites;
  Class *nestingParent{};

  explicit Class(const Id_t id,
                 const name_t name,
                 Image *img,
                 Namespace *nmsp,
                 Class *cls,
                 File *file,
                 const line_t line,
                 const size_t size)
    : SoftwareEntity(id, name, img, nmsp, cls, file, line)
  {
    this->size = size;
    img->entities.push_back(this);
  }
  Class() = default;

  EntityType getEntityType() const override { return EntityType::Class; }

  std::string getFullName()
  {
    if (full_name.empty()) {
      full_name = cls->nmsp == nullptr ? "" : cls->getFullName();
      if (!full_name.empty()) {
        full_name += "::";
      }
      auto nestingScopes = getNestingClassScopes(nestingParent);
      if (!nestingScopes.empty()) {
        full_name += nestingScopes + "::";
      }
      full_name += cls->name;
    }
    return full_name;
  }

  std::string getNestingClassScopes() const { return getNestingClassScopes(nestingParent); }

 private:
  static std::string getNestingClassScopes(Class *parent)
  {
    std::string scopes;
    while (parent != nullptr) {
      if (!scopes.empty()) {
        scopes.insert(0, "::");
      }
      scopes = parent->name + scopes;
      parent = parent->nestingParent;
    }
    return scopes;
  }
};

struct UniqueClass {
  Id_t imageId;
  Id_t classId;
  UniqueClass(Id_t imageId, Id_t classId) : imageId(imageId), classId(classId) {}
};

}  // namespace entity
}  // namespace pcv
