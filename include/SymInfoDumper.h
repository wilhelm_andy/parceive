#ifndef SRC_BACKEND_INCLUDE_SYMINFODUMPER_H_
#define SRC_BACKEND_INCLUDE_SYMINFODUMPER_H_

namespace pcv {
class SymInfo;

void dumpSymInfo(pcv::SymInfo* symInfo);
}  // namespace pcv

#endif