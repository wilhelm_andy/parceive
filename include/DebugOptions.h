/*! \file DebugOptions.h
 *  \brief This file contains various debug options for the tool like
 *   only normal mode, variable analysis only, routine analysis only,
 *   instrumentation only, analysis empty, no database write etc.
 *   Also two kinds of tracing levels can be enabled. Tracing Level 1 is less
 *	 detailed and Tracing Level 2 is for higher details.
 */

/* Copyright 2014 Siemens Technology and Services*/

#ifndef SRC_BACKEND_INCLUDE_DEBUGOPTIONS_H_
#define SRC_BACKEND_INCLUDE_DEBUGOPTIONS_H_

/* This mode enables tracing level 1. This logs more details during the
 * PinTool run.
 */
// #define TRACING_LEVEL1

/* This mode enables the highest tracing level. This logs most details during
 * the PinTool run.
 */
// #define TRACING_LEVEL2

#endif  // SRC_BACKEND_INCLUDE_DEBUGOPTIONS_H_
