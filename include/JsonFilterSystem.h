/*! \file JsonFilterSystem.h
 *  \brief This file contains an implementation of FilterSystem based on JSON data.
 *
 * Copyright 2017 Siemens Technology and Services
 */

#ifndef SRC_BACKEND_INCLUDE_JSONFILTERSYSTEM_H_
#define SRC_BACKEND_INCLUDE_JSONFILTERSYSTEM_H_

#pragma once
#include <string>

#include "FilterRuleCollection.h"
#include "FilterSystem.h"
#include "JsonFilterRuleDeserializer.h"

namespace pcv {

class JsonFilterSystem final : public FilterSystem
{
 public:
  JsonFilterSystem(const std::string& jsonFilepath);

 private:
  void fillFilterRuleCollections(const JsonFilterRules& rules);
  void addRule(FilterRuleCollection* target,
               const Filter::Action action,
               const JsonFilterData& data,
               const JsonFilterScope& scope);
};

}  // namespace pcv

#endif  // SRC_BACKEND_INCLUDE_JSONFILTERSYSTEM_H_