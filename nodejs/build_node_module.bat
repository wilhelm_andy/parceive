@echo off
SETLOCAL

REM Builds the native module used in parceive on windows
REM Requires VS2013, node+npm+nody-gyp to be installed
REM https://github.com/nodejs/node-gyp suggests that we could use certain commandline
REM parameters such as --target in our call. Experimentation suggests this setup:

set npm_config_arch=x64
set npm_config_disturl=https://atom.io/download/electron
set npm_config_runtime=electron
set npm_config_build_from_source=true
set npm_config_target=1.6.11
set JOBS=8

CALL node-gyp configure --msvs_version=2013
CALL node-gyp build
IF ERRORLEVEL 1 EXIT /B 1

copy /Y %~dp0build\Release\symbols.node %~dp0..\..\ui\assets\
