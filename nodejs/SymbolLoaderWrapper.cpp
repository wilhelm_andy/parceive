#include "SymbolLoaderWrapper.h"
#include <v8.h>
#include <string>
#include "ArchException.h"
#include "ast/ast-from-json/CreateAstFromJson.h"
#include "ast/visitor/ASTVisitor.h"
#include "ast/visitor/VisitorContext.h"

using std::cerr;
using std::string;
using std::stringstream;

namespace pcv {

v8::Persistent<v8::Function> SymbolLoaderWrapper::constructor;

SymbolLoaderWrapper::SymbolLoaderWrapper(const string& fileName, const string& filterPattern)
  : symbolLoader_(fileName, filterPattern)
{}

SymbolLoaderWrapper::~SymbolLoaderWrapper() {}

void SymbolLoaderWrapper::Init(v8::Local<v8::Object> exports)
{
  const char* const className = "loader";

  v8::Isolate* isolate                = exports->GetIsolate();
  v8::Local<v8::FunctionTemplate> tpl = v8::FunctionTemplate::New(isolate, New);
  tpl->SetClassName(v8::String::NewFromUtf8(isolate, className));
  tpl->InstanceTemplate()->SetInternalFieldCount(2);

  NODE_SET_PROTOTYPE_METHOD(tpl, "start", start);
  NODE_SET_PROTOTYPE_METHOD(tpl, "startnew", startnew);

#ifdef _MSC_VER
  NODE_SET_PROTOTYPE_METHOD(tpl, "getBinaryBits", getBinaryBits);
  NODE_SET_PROTOTYPE_METHOD(tpl, "isBinaryOptimized", isBinaryOptimized);
#endif

  constructor.Reset(isolate, tpl->GetFunction());
  exports->Set(v8::String::NewFromUtf8(isolate, className), tpl->GetFunction());
}

void SymbolLoaderWrapper::New(const v8::FunctionCallbackInfo<v8::Value>& args)
{
  v8::Isolate* isolate = args.GetIsolate();
  if (args.IsConstructCall()) {
    v8::String::Utf8Value fileName(args[0]);
    v8::String::Utf8Value filterPattern(args[1]);

    auto obj = new SymbolLoaderWrapper(*fileName, *filterPattern);
    obj->Wrap(args.This());
    args.GetReturnValue().Set(args.This());
  } else {
    const int argc                  = 1;
    v8::Local<v8::Value> argv[argc] = {args[0]};
    v8::Local<v8::Context> context  = isolate->GetCurrentContext();
    v8::Local<v8::Function> cons    = v8::Local<v8::Function>::New(isolate, constructor);
    v8::Local<v8::Object> result    = cons->NewInstance(context, argc, argv).ToLocalChecked();
    args.GetReturnValue().Set(result);
  }
}

void SymbolLoaderWrapper::start(const v8::FunctionCallbackInfo<v8::Value>& args)
{
  auto isolate = args.GetIsolate();
  auto wrapper = ObjectWrap::Unwrap<SymbolLoaderWrapper>(args.Holder());
  v8::String::Utf8Value ast(args[0]);

  try {
    const string returnValue(wrapper->symbolLoader_.start(*ast));

    args.GetReturnValue().Set(v8::String::NewFromUtf8(isolate, returnValue.c_str()));
  } catch (ArchError& e) {
    isolate->ThrowException(v8::Exception::Error(v8::String::NewFromUtf8(isolate, e.what())));
  }
}

void SymbolLoaderWrapper::startnew(const v8::FunctionCallbackInfo<v8::Value>& args)
{
  auto isolate = args.GetIsolate();
  auto wrapper = ObjectWrap::Unwrap<SymbolLoaderWrapper>(args.Holder());
  v8::String::Utf8Value ast(args[0]);

  try {
    const string returnValue(wrapper->symbolLoader_.start(*ast));

    args.GetReturnValue().Set(v8::String::NewFromUtf8(isolate, returnValue.c_str()));
  } catch (ArchError& e) {
    isolate->ThrowException(v8::Exception::Error(v8::String::NewFromUtf8(isolate, e.what())));
  }
}

#ifdef _MSC_VER

void SymbolLoaderWrapper::getBinaryBits(const v8::FunctionCallbackInfo<v8::Value>& args)
{
  auto isolate = args.GetIsolate();
  auto wrapper = ObjectWrap::Unwrap<SymbolLoaderWrapper>(args.Holder());
  args.GetReturnValue().Set(v8::Number::New(isolate, wrapper->symbolLoader_.getBinaryBits()));
}

void SymbolLoaderWrapper::isBinaryOptimized(const v8::FunctionCallbackInfo<v8::Value>& args)
{
  auto isolate = args.GetIsolate();
  auto wrapper = ObjectWrap::Unwrap<SymbolLoaderWrapper>(args.Holder());
  args.GetReturnValue().Set(v8::Boolean::New(isolate, wrapper->symbolLoader_.isBinaryOptimized()));
}

#endif

}  // namespace pcv
