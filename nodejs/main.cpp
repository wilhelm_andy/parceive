#include <FunctionRule.h>
#include <stdlib.h>
#include <cstdlib>
#include <fstream>
#include <string>

#include "SymbolLoader.h"

using pcv::SymbolLoader;
using std::cerr;
using std::cout;
using std::endl;
using std::ifstream;
using std::istreambuf_iterator;
using std::string;

static string getFileContents(const char* fileName);
static string getExampleJsonAst();

int main(int argc, char** argv)
{
  if (argc != 3 && argc != 4) {
    cerr << "Usage: console_reader <path to binary> <filter regex pattern> [path to JSON AST]"
         << endl;
    abort();
  }

  try {
    const string fileName(argv[1]);
    const string filterPattern(argv[2]);
    SymbolLoader loader(fileName, filterPattern);

    string ast;
    if (argc == 4) {
      const auto astPath = argv[3];
      ast                = getFileContents(astPath);
    } else {
      ast = getExampleJsonAst();
    }

    cout << loader.start(ast);
  } catch (std::exception& e) {
    cerr << e.what();
    return -1;
  }

  return 0;
}

static string getFileContents(const char* fileName)
{
  ifstream stream(fileName);
  return string(istreambuf_iterator<char>(stream), istreambuf_iterator<char>());
}

static string getExampleJsonAst()
{
  return "[{\"type\":\"AssignmentExpression\",\"operator\":\":=\",\"left\":{\"type\":\"Artifact\","
         "\"name\":\"rules\"},\"right\":{\"type\":\"AtomExpression\",\"rule\":\"infile\",\"regex\":"
         "\".*\"}}]";
}