#include "SymbolLoaderWrapper.h"

using v8::Local;
using v8::Object;

void InitAll(Local<Object> exports) { pcv::SymbolLoaderWrapper::Init(exports); }

NODE_MODULE(dwarfLoader, InitAll)
