#pragma once

#include <node.h>
#include <node_object_wrap.h>
#include <v8.h>
#include <string>

#include "SymbolLoader.h"

using pcv::SymbolLoader;

namespace pcv {

class SymbolLoaderWrapper : public node::ObjectWrap
{
 public:
  static void Init(v8::Local<v8::Object> exports);

 private:
  explicit SymbolLoaderWrapper(const std::string& fileName, const std::string& filterStr);
  ~SymbolLoaderWrapper();

  static v8::Persistent<v8::Function> constructor;
  static void New(const v8::FunctionCallbackInfo<v8::Value>& args);
  static void start(const v8::FunctionCallbackInfo<v8::Value>& args);
  static void startnew(const v8::FunctionCallbackInfo<v8::Value>& args);

#ifdef _MSC_VER
  static void getBinaryBits(const v8::FunctionCallbackInfo<v8::Value>& args);
  static void isBinaryOptimized(const v8::FunctionCallbackInfo<v8::Value>& args);
#endif

  SymbolLoader symbolLoader_;
};

}  // namespace pcv
