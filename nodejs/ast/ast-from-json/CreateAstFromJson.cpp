//
// Created by Faris Cakaric on 30.05.17.
//

#include "CreateAstFromJson.h"
#include "../nodes/AssignmentExpression.h"
#include "../nodes/FilterExpression.h"

namespace {

using pcv::FilterType;
using pcv::StatementType;

StatementType getStatementType(const Json &json)
{
  if (json.string_value().compare("DefinitionExpression") == 0) return StatementType::Definition;
  if (json.string_value().compare("AssignmentExpression") == 0) return StatementType::Assignment;
  if (json.string_value().compare("FilterStatement") == 0) return StatementType::Filter;
  return StatementType::Filter;
}

FilterType getFilterType(const Json &json)
{
  if (json.string_value().compare("include") == 0) return FilterType::include;
  if (json.string_value().compare("exclude") == 0) return FilterType::exclude;
  if (json.string_value().compare("enable") == 0) return FilterType::enable;
  if (json.string_value().compare("disable") == 0) return FilterType::disable;
  if (json.string_value().compare("start") == 0) return FilterType::start;
  if (json.string_value().compare("stop") == 0) return FilterType::stop;
  if (json.string_value().compare("partial") == 0) return FilterType::partial;
  return FilterType::stop;
}

}  // namespace

namespace pcv {

Program CreateAstFromJson::generateAst(Json json)
{
  std::vector<std::unique_ptr<Expression>> expressions;
  Json::array statements = json.array_items();

  for (const auto &statement : statements) {
    switch (getStatementType(statement["type"].string_value())) {
      case StatementType::Filter: {
        expressions.push_back(std::unique_ptr<Expression>(generateFilterExpression(statement)));
        break;
      }
      default:
        expressions.push_back(
          std::unique_ptr<Expression>(generateDefinitionExpressionFromJson(statement)));
    }
  }

  return Program{expressions};
}

Expression *CreateAstFromJson::generateGeneralExpression(const Json &json)
{
  if (json["type"].string_value().compare("OrExpression") == 0)
    return generateBinaryExpressionFromJson<OrExpression>(json);
  if (json["type"].string_value().compare("AndExpression") == 0)
    return generateBinaryExpressionFromJson<AndExpression>(json);
  if (json["type"].string_value().compare("NotExpression") == 0)
    return generateNotExpressionFromJson(json);
  if (json["type"].string_value().compare("AtomExpression") == 0)
    return generateAtomExpressionFromJson(json);
  if (json["type"].string_value().compare("Artifact") == 0) return generateArtifactFromJson(json);
  if (json["type"].string_value().compare("SetExpression") == 0)
    return generateSetTermFromJson(json);
  return nullptr;
}

Expression *CreateAstFromJson::generateFilterExpression(Json json)
{
  std::unique_ptr<Expression> right(generateGeneralExpression(json["right"]));

  return new FilterExpression(getFilterType(json["left"]), right);
}

Expression *CreateAstFromJson::generateDefinitionExpressionFromJson(Json json)
{
  std::unique_ptr<Artifact> left(generateArtifactFromJson(json["left"]));
  Json rightSide = json["right"];
  std::unique_ptr<Expression> right(generateGeneralExpression(rightSide));

  if (json["operator"] == "=")
    return new DefinitionExpression(left, right);
  else
    return new AssignmentExpression(left, right);
}

template <typename T>
T *CreateAstFromJson::generateBinaryExpressionFromJson(Json json)
{
  Json leftSide = json["left"];
  std::unique_ptr<Expression> left;
  if (leftSide["type"] == "OrExpression") {
    left = std::unique_ptr<OrExpression>(generateBinaryExpressionFromJson<OrExpression>(leftSide));
  } else if (leftSide["type"] == "AndExpression") {
    left =
      std::unique_ptr<AndExpression>(generateBinaryExpressionFromJson<AndExpression>(leftSide));
  } else if (leftSide["type"] == "NotExpression") {
    left = std::unique_ptr<NotExpression>(generateNotExpressionFromJson(leftSide));
  } else if (leftSide["type"] == "AtomExpression") {
    left = std::unique_ptr<AtomExpression>(generateAtomExpressionFromJson(leftSide));
  } else if (leftSide["type"] == "Artifact") {
    left = std::unique_ptr<Artifact>(generateArtifactFromJson(leftSide));
  } else if (leftSide["type"] == "SetExpression") {
    left = std::unique_ptr<SetExpression>(generateSetTermFromJson(leftSide));
  }

  Json rightSide = json["right"];
  std::unique_ptr<Expression> right;
  if (rightSide["type"] == "OrExpression") {
    right =
      std::unique_ptr<OrExpression>(generateBinaryExpressionFromJson<OrExpression>(rightSide));
  } else if (rightSide["type"] == "AndExpression") {
    right =
      std::unique_ptr<AndExpression>(generateBinaryExpressionFromJson<AndExpression>(rightSide));
  } else if (rightSide["type"] == "NotExpression") {
    right = std::unique_ptr<NotExpression>(generateNotExpressionFromJson(rightSide));
  } else if (rightSide["type"] == "AtomExpression") {
    right = std::unique_ptr<AtomExpression>(generateAtomExpressionFromJson(rightSide));
  } else if (rightSide["type"] == "Artifact") {
    right = std::unique_ptr<Artifact>(generateArtifactFromJson(rightSide));
  } else if (rightSide["type"] == "SetExpression") {
    right = std::unique_ptr<SetExpression>(generateSetTermFromJson(rightSide));
  }

  return new T(left, right);
}

NotExpression *CreateAstFromJson::generateNotExpressionFromJson(Json json)
{
  Json rightSide = json["operand"];
  std::unique_ptr<Expression> right;
  if (rightSide["type"] == "OrExpression") {
    right =
      std::unique_ptr<OrExpression>(generateBinaryExpressionFromJson<OrExpression>(rightSide));
  } else if (rightSide["type"] == "AndExpression") {
    right =
      std::unique_ptr<AndExpression>(generateBinaryExpressionFromJson<AndExpression>(rightSide));
  } else if (rightSide["type"] == "NotExpression") {
    right = std::unique_ptr<NotExpression>(generateNotExpressionFromJson(rightSide));
  } else if (rightSide["type"] == "AtomExpression") {
    right = std::unique_ptr<AtomExpression>(generateAtomExpressionFromJson(rightSide));
  } else if (rightSide["type"] == "Artifact") {
    right = std::unique_ptr<Artifact>(generateArtifactFromJson(rightSide));
  } else if (rightSide["type"] == "SetExpression") {
    right = std::unique_ptr<SetExpression>(generateSetTermFromJson(rightSide));
  }

  return new NotExpression{right};
}

AtomExpression *CreateAstFromJson::generateAtomExpressionFromJson(Json json)
{
  return new AtomExpression{json["rule"].string_value(), json["regex"].string_value()};
}

SetExpression *CreateAstFromJson::generateSetTermFromJson(Json json)
{
  std::vector<std::unique_ptr<Artifact>> elements;
  Json::array jsonElements = json["elements"].array_items();

  for (size_t i = 0; i < jsonElements.size(); i++)
    elements.push_back(std::unique_ptr<Artifact>(generateArtifactFromJson(jsonElements[i])));

  return new SetExpression{elements};
}

Artifact *CreateAstFromJson::generateArtifactFromJson(Json json)
{
  return new Artifact{json["name"].string_value()};
}

}  // namespace pcv
