//
// Created by Faris Cakaric on 30.05.17.
//

#pragma once

#include "../nodes/AndExpression.h"
#include "../nodes/Artifact.h"
#include "../nodes/AtomExpression.h"
#include "../nodes/DefinitionExpression.h"
#include "../nodes/NotExpression.h"
#include "../nodes/OrExpression.h"
#include "../nodes/Program.h"
#include "../nodes/SetExpression.h"

#include <json11.hpp>

using json11::Json;

namespace pcv {

/// ast types
enum class StatementType { Definition, Assignment, Filter };

class CreateAstFromJson
{
 public:
  static Program generateAst(Json json);

 private:
  static Expression* generateDefinitionExpressionFromJson(Json json);

  static Expression* generateFilterExpression(Json json);

  static Expression* generateGeneralExpression(const Json& json);

  template <typename T>
  static T* generateBinaryExpressionFromJson(Json json);

  static NotExpression* generateNotExpressionFromJson(Json json);

  static AtomExpression* generateAtomExpressionFromJson(Json json);

  static SetExpression* generateSetTermFromJson(Json json);

  static Artifact* generateArtifactFromJson(Json json);
};

}  // namespace pcv
