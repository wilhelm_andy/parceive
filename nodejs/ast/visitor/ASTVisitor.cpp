//
// Created by Faris Cakaric on 25.05.17.
//

#include "ASTVisitor.h"
#include "AndOperatorRule.h"
#include "ArchException.h"
#include "ClassHierarchyRule.h"
#include "FunctionRule.h"
#include "ImageRule.h"
#include "NamespaceRule.h"
#include "NotOperatorRule.h"
#include "OrOperatorRule.h"
#include "RegexFileRule.h"
#include "SetOperatorRule.h"
#include "VariableRule.h"

namespace pcv {

using pcv::AndOperatorRule;
using pcv::ClassHierarchyRule;
using pcv::ClassRule;
using pcv::EntityType;
using pcv::FunctionRule;
using pcv::NamespaceRule;
using pcv::NotOperatorRule;
using pcv::OrOperatorRule;
using pcv::RegexFileRule;
using pcv::SetOperatorRule;
using pcv::VariableRule;

ASTVisitor::ASTVisitor(VisitorContext *visitorContext) : visitorContext_(visitorContext) {}

void ASTVisitor::visit(AndExpression &el)
{
  el.getLeft()->accept(*this);
  el.getRight()->accept(*this);
  ArchRule *archRule = new AndOperatorRule(
    "", visitorContext_->popFromArchRulesStack(), visitorContext_->popFromArchRulesStack());
  visitorContext_->pushToArchRulesStack(archRule);
  visitorContext_->applyRuleToBuilder(archRule);
}

void ASTVisitor::visit(AssignmentExpression &el)
{
  el.getExpression()->accept(*this);
  ArchRule *archRule = visitorContext_->popFromArchRulesStack();
  visitorContext_->addArtifactToArchBuilder(el.getArtifact().get()->getName(), archRule);
  expressions[el.getArtifact().get()->getName()] = archRule;
}

void ASTVisitor::visit(AtomExpression &el)
{
  if (el.getRule() == "classHierarchy") {
    ArchRule *archRule = new ClassHierarchyRule("", el.getRegex());
    visitorContext_->pushToArchRulesStack(archRule);
    visitorContext_->applyRuleToBuilder(archRule);
  } else if (el.getRule() == "class") {
    ArchRule *archRule = new ClassRule("", el.getRegex());
    visitorContext_->pushToArchRulesStack(archRule);
    visitorContext_->applyRuleToBuilder(archRule);
  } else if (el.getRule() == "namespace") {
    ArchRule *archRule = new NamespaceRule("", el.getRegex());
    visitorContext_->pushToArchRulesStack(archRule);
    visitorContext_->applyRuleToBuilder(archRule);
  } else if (el.getRule() == "variable") {
    ArchRule *archRule = new VariableRule("", el.getRegex());
    visitorContext_->pushToArchRulesStack(archRule);
    visitorContext_->applyRuleToBuilder(archRule);
  } else if (el.getRule() == "function") {
    ArchRule *archRule = new FunctionRule("", el.getRegex());
    visitorContext_->pushToArchRulesStack(archRule);
    visitorContext_->applyRuleToBuilder(archRule);
  } else if (el.getRule() == "infile") {
    ArchRule *archRule = new RegexFileRule("", EntityType::All, el.getRegex());
    visitorContext_->pushToArchRulesStack(archRule);
    visitorContext_->applyRuleToBuilder(archRule);
  } else if (el.getRule() == "image") {
    ArchRule *archRule = new ImageRule("", el.getRegex());
    visitorContext_->pushToArchRulesStack(archRule);
    visitorContext_->applyRuleToBuilder(archRule);
  }
}

void ASTVisitor::visit(Artifact &el)
{
  auto rule = expressions.find(el.getName());
  if (rule == std::end(expressions))
    throw ArchError(std::string("no expression for ") + el.getName() + std::string(" found!"));

  visitorContext_->pushToArchRulesStack(expressions[el.getName()]);
}

void ASTVisitor::visit(DefinitionExpression &el)
{
  el.getExpression()->accept(*this);
  ArchRule *archRule = visitorContext_->popFromArchRulesStack();
  archRule->setArtifactName(el.getArtifact().get()->getName());
  expressions[el.getArtifact().get()->getName()] = archRule;
}

void ASTVisitor::visit(Expression &el) {}

void ASTVisitor::visit(NotExpression &el)
{
  ArchRule *archRule;
  if (dynamic_cast<Artifact *>(el.getOperand().get())) {
    archRule = new NotOperatorRule("",
                                   visitorContext_->getArtifactFromArchBuilder(
                                     dynamic_cast<Artifact *>(el.getOperand().get())->getName()));
  } else {
    el.getOperand()->accept(*this);
    archRule = new NotOperatorRule("", visitorContext_->popFromArchRulesStack()->getArtifact());
  }
  visitorContext_->pushToArchRulesStack(archRule);
  visitorContext_->applyRuleToBuilder(archRule);
}

void ASTVisitor::visit(OrExpression &el)
{
  el.getLeft()->accept(*this);
  el.getRight()->accept(*this);
  ArchRule *archRule = new OrOperatorRule(
    "", visitorContext_->popFromArchRulesStack(), visitorContext_->popFromArchRulesStack());
  visitorContext_->pushToArchRulesStack(archRule);
  visitorContext_->applyRuleToBuilder(archRule);
}

void ASTVisitor::visit(Program &el)
{
  for (const auto &expression : el.getExpressions()) {
    expression->accept(*this);
  }

  visitorContext_->outputBuilderJson();
}

void ASTVisitor::visit(SetExpression &el)
{
  std::vector<Artifact_t *> artifactsFromStack;
  for (size_t i = 0; i < el.getTerms().size(); i++) {
    artifactsFromStack.push_back(expressions[el.getTerms()[i].get()->getName()]->getArtifact());
  }

  ArchRule *archRule = new SetOperatorRule("", artifactsFromStack);
  visitorContext_->pushToArchRulesStack(archRule);
  visitorContext_->applyRuleToBuilder(archRule);
}

void ASTVisitor::visit(FilterExpression &el)
{
  visitorContext_->switchToFilterArtifact();

  if (el.getExpression()) {
    el.getExpression()->accept(*this);
    ArchRule *archRule = visitorContext_->popFromArchRulesStack();
    visitorContext_->addCloneForFilter(el.getFilterTypeName(), archRule);
  }

  visitorContext_->switchToDefaultArtifact();
}

}  // namespace pcv
