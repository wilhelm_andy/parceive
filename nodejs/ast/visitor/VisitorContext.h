//
// Created by Faris Cakaric on 26.06.17.
//

#pragma once

#include "ArchBuilder.h"
#include "ArchRule.h"

#include <algorithm>
#include <stack>

namespace pcv {

using pcv::ArchBuilder;
using pcv::ArchRule;
using pcv::Artifact_t;
using pcv::Context;

class VisitorContext
{
 public:
  explicit VisitorContext(const Context& ctxt, std::ostream& out);

  void pushToArchRulesStack(ArchRule* archRule);

  ArchRule* popFromArchRulesStack();

  void addArtifactToArchBuilder(std::string artifactName, ArchRule* archRule);
  void addCloneForFilter(std::string artifactName, ArchRule* archRule);

  Artifact_t* getArtifactFromArchBuilder(std::string artifactName);

  void outputBuilderJson();

  void applyRuleToBuilder(ArchRule* archRule);

  inline void switchToDefaultArtifact() { currentArtifact_ = &artifact_; }
  inline void switchToFilterArtifact() { currentArtifact_ = &filterArtifact_; }

  /// @brief The overloaded << operator that writes Json output.
  /// @param [out] os The ostream ref to write to.
  /// @param [in] obj The internal VisitorContext reference.
  /// @return The ostream ref to write to.
  friend std::ostream& operator<<(std::ostream& os, const VisitorContext& obj);

 private:
  ArchBuilder archBuilder_;
  std::stack<ArchRule*> archRulesStack;
  std::ostream& out_;
  Artifact_t artifact_;
  Artifact_t filterArtifact_;
  Artifact_t* currentArtifact_;
};

}  // namespace pcv
