//
// Created by Faris Cakaric on 30.05.17.
//

#pragma once

namespace pcv {

class Visitor;

class ASTNode
{
 public:
  virtual void accept(Visitor& v) = 0;
  virtual ~ASTNode()              = default;
};

}  // namespace pcv
