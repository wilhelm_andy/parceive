//
// Created by wilhelma on 12/6/17.
//

#pragma once

#include "Artifact.h"
#include "Expression.h"

namespace pcv {

enum class FilterType { include, exclude, enable, disable, start, stop, partial };

class FilterExpression : public Expression
{
 public:
  explicit FilterExpression(FilterType type, std::unique_ptr<Expression>& expression);
  explicit FilterExpression(FilterType type, std::unique_ptr<Expression>&& expression);

  void accept(Visitor& v) override;

  std::string getFilterTypeName() const;
  Expression* getExpression() const;

 private:
  FilterType type;
  std::unique_ptr<Expression> expression;
};

}  // namespace pcv
