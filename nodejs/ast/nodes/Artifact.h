//
// Created by Faris Cakaric on 28/05/2017.
//

#pragma once

#include <iostream>
#include <json11.hpp>
#include <string>

#include "Expression.h"

namespace pcv {

using json11::Json;

class Artifact : public virtual Expression
{
  std::string name;

 public:
  explicit Artifact(const std::string &name);

  const std::string &getName() const;

  virtual void accept(Visitor &v) override;
};

}  // namespace pcv
