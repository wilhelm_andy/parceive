//
// Created by Faris Cakaric on 27.08.17.
//

#pragma once

#include "Artifact.h"
#include "Expression.h"

#include <json11.hpp>

namespace pcv {

using json11::Json;

class AssignmentExpression : public Expression
{
  std::unique_ptr<Artifact> artifact;
  std::unique_ptr<Expression> expression;

 public:
  AssignmentExpression(std::unique_ptr<Artifact> &artifact,
                       std::unique_ptr<Expression> &expression);

  const std::unique_ptr<Artifact> &getArtifact() const;

  const std::unique_ptr<Expression> &getExpression() const;

  virtual void accept(Visitor &v) override;
};

}  // namespace pcv
