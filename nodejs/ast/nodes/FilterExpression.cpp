//
// Created by wilhelma on 12/6/17.
//

#include "FilterExpression.h"
#include "../visitor/Visitor.h"

namespace pcv {

FilterExpression::FilterExpression(FilterType type, std::unique_ptr<Expression>&& expression)
  : type(type), expression(std::move(expression))
{}

FilterExpression::FilterExpression(FilterType type, std::unique_ptr<Expression>& expression)
  : type(type), expression(std::move(expression))
{}

void FilterExpression::accept(Visitor& v) { v.visit(*this); }

Expression* FilterExpression::getExpression() const { return expression.get(); }

std::string FilterExpression::getFilterTypeName() const
{
  switch (type) {
    case FilterType::include:
      return "include";
    case FilterType::exclude:
      return "exclude";
    case FilterType::enable:
      return "enable";
    case FilterType::disable:
      return "disable";
    case FilterType::start:
      return "start";
    case FilterType::stop:
      return "stop";
    case FilterType::partial:
      return "partial";
  }
  return "error";
}

}  // namespace pcv
