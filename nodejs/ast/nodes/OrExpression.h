//
// Created by Faris Cakaric on 28/05/2017.
//

#pragma once

#include "Expression.h"

namespace pcv {

class OrExpression : public virtual Expression
{
  std::unique_ptr<Expression> left;
  std::unique_ptr<Expression> right;

 public:
  OrExpression(OrExpression const &) = delete;

  OrExpression &operator=(OrExpression const &) = delete;

  OrExpression(std::unique_ptr<Expression> &left, std::unique_ptr<Expression> &right);

  const std::unique_ptr<Expression> &getLeft() const;

  const std::unique_ptr<Expression> &getRight() const;

  virtual void accept(Visitor &v) override;
};

}  // namespace pcv
