//
// Created by Faris Cakaric on 28/05/2017.
//

#pragma once

#include <json11.hpp>
#include <vector>

#include "Artifact.h"
#include "Expression.h"

namespace pcv {

using json11::Json;

class SetExpression : public virtual Expression
{
  std::vector<std::unique_ptr<Artifact>> terms;

 public:
  SetExpression(std::vector<std::unique_ptr<Artifact>> &terms);

  const std::vector<std::unique_ptr<Artifact>, std::allocator<std::unique_ptr<Artifact>>>
    &getTerms() const;

  virtual void accept(Visitor &v) override;
};

}  // namespace pcv
