//
// Created by Faris Cakaric on 28/05/2017.
//

#pragma once

#include <string>

#include "Expression.h"

#include <json11.hpp>

namespace pcv {

using json11::Json;

class AtomExpression : public virtual Expression
{
  std::string rule;
  std::string regex;

 public:
  AtomExpression(const std::string &rule, const std::string &regex);

  const std::string &getRule() const;

  const std::string &getRegex() const;

  virtual void accept(Visitor &v) override;
};

}  // namespace pcv
