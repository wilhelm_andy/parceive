//
// Created by Faris Cakaric on 19.07.17.
//

#ifndef DWARFLOADER_NOTOPERATOR_H
#define DWARFLOADER_NOTOPERATOR_H

#include <memory>
#include <regex>
#include "BinaryOperatorRule.h"

namespace pcv {

class NotOperatorRule : public BinaryOperatorRule
{
  std::string artifactName_;
  Artifact_t *operand_;

 public:
  NotOperatorRule(const std::string &artifactName_, Artifact_t *operand_);

  NotOperatorRule(const NotOperatorRule &);

  virtual ArchRule *clone() const { return new NotOperatorRule(*this); }

  std::unique_ptr<artifacts_t> execute(Artifact_t &artifact, const Context &ctxt) override;
  std::unique_ptr<artifacts_t> append(Artifact_t &artifact, const Context &ctxt) override;

};

}  // namespace pcv

#endif  // DWARFLOADER_NOTOPERATOR_H
