//
// Created by Faris Cakaric on 19.06.17.
//

#ifndef DWARFLOADER_ANDOPERATORRULE_H
#define DWARFLOADER_ANDOPERATORRULE_H

#include <memory>
#include <regex>
#include "BinaryOperatorRule.h"

namespace pcv {

class AndOperatorRule : public BinaryOperatorRule
{
  std::string artifactName_;
  ArchRule* firstArtifact_;
  ArchRule* secondArtifact_;

 public:
  AndOperatorRule(const std::string& artifactName_,
                  ArchRule* firstArtifact_,
                  ArchRule* secondArtifact_);

  AndOperatorRule(const AndOperatorRule&);

  virtual ArchRule* clone() const { return new AndOperatorRule(*this); }
  std::unique_ptr<artifacts_t> execute(Artifact_t& artifact, const Context& ctxt) override;
  std::unique_ptr<artifacts_t> append(Artifact_t& artifact, const Context& ctxt) override;
};

}  // namespace pcv

#endif  // DWARFLOADER_ANDOPERATORRULE_H
