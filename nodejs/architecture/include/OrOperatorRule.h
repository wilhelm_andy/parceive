//
// Created by Faris Cakaric on 17.06.17.
//

#ifndef DWARFLOADER_ANDRULE_H
#define DWARFLOADER_ANDRULE_H

#include <memory>
#include <regex>
#include "BinaryOperatorRule.h"

namespace pcv {

class OrOperatorRule : public Cloneable<OrOperatorRule, BinaryOperatorRule>
{
  std::string artifactName_;
  ArchRule *firstArtifact_;
  ArchRule *secondArtifact_;

 public:
  explicit OrOperatorRule(const std::string &artifactName_, ArchRule *first, ArchRule *second);
  OrOperatorRule(const OrOperatorRule &);

  std::unique_ptr<artifacts_t> execute(Artifact_t &artifact, const Context &ctxt) override;
  std::unique_ptr<artifacts_t> append(Artifact_t &artifact, const Context &ctxt) override;
};

}  // namespace pcv

#endif  // DWARFLOADER_ANDRULE_H
