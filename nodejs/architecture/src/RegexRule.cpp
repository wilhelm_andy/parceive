//
// Created by wilhelma on 1/10/17.
//

#include "RegexRule.h"
#include <ClassRule.h>
#include <FunctionRule.h>
#include <NamespaceRule.h>
#include "ArchBuilder.h"
#include "entities/Class.h"
#include "entities/Image.h"
#include "entities/SoftwareEntity.h"

#ifndef _MSC_VER
#include "DwarfContext.h"
#endif

namespace pcv {

RegexRule::RegexRule(const std::string &artifactName, EntityType type, const std::string &rx)
  : artifactName_(artifactName), type_(type), rx_(rx)
{}

std::unique_ptr<ArchRule::artifacts_t> RegexRule::append(Artifact_t &artifact, const Context &ctxt)
{
  return nullptr;
}

void RegexRule::fillArtifact(const Context &ctxt,
                             Artifact_t *toFill,
                             ArchRule::added_t &added) const
{
  // consider classes
  {
    std::unordered_set<const Class *> classes;
    for (auto &entities : ctxt.images) {
      for (auto &entity : entities.get()->entities) {
        if (entity->getEntityType() == EntityType::Class) {
          if (added.find(entity) == std::end(added)) {
            if (std::regex_match(getMatchString(*entity), rx_) &&
                classes.find(static_cast<const Class *>(entity)) == std::end(classes)) {
              classes.insert(static_cast<const Class *>(entity));
            }
          }
        }
      }
    }

    ClassRule cRule;
    auto tmpAdded = cRule.apply(toFill, classes);
    for (auto cls : tmpAdded) added.insert(cls.first);
  }

  // consider functions
  {
    std::unordered_set<const Routine *> routines;
    for (auto &entities : ctxt.images) {
      for (auto &entity : entities.get()->entities) {
        if (entity->getEntityType() == EntityType::Routine) {
          if (added.find(entity) == std::end(added)) {
            if (std::regex_match(getMatchString(*entity), rx_)) {
              routines.insert(static_cast<const Routine *>(entity));
            }
          }
        }
      }
    }
    FunctionRule fRule;
    auto tmpAdded = fRule.apply(toFill, routines);
    added.insert(std::begin(tmpAdded), std::end(tmpAdded));
  }

  // consider variables
  {
    for (auto &entities : ctxt.images) {
      for (auto &entity : entities.get()->entities) {
        if (entity->getEntityType() == EntityType::Variable) {
          if (std::regex_match(getMatchString(*entity), rx_)) {
            if (added.find(entity) == std::end(added)) {
              toFill->children.emplace_back(
                std::unique_ptr<Artifact_t>{new Artifact_t(entity->name, toFill)});
              toFill->children.back()->entity = entity;
            }
          }
        }
      }
    }
  }
}

std::unique_ptr<ArchRule::artifacts_t> RegexRule::execute(Artifact_t &artifact, const Context &ctxt)
{
  auto artifacts = std::unique_ptr<artifacts_t>{new artifacts_t};

  artifact_         = std::unique_ptr<Artifact_t>(new Artifact_t(artifactName_, &artifact));
  artifact_->entity = nullptr;

  artifacts->emplace_back(artifact_.get());

  added_t added;

#ifndef _MSC_VER
  NamespaceRule nRule;
  for (auto &nmsp : ctxt.namespaces) {
    if (nmsp->parent &&
        nmsp->parent == static_cast<const pcv::dwarf::DwarfContext &>(ctxt).emptyNamespace) {
      if (std::regex_match(getMatchString(*nmsp.get()), rx_)) {
        auto tmpAdded = nRule.apply(artifact_.get(), *nmsp.get());
        added.insert(std::begin(tmpAdded), std::end(tmpAdded));
      }
    }
  }
#endif

  fillArtifact(ctxt, artifact_.get(), added);

  return artifacts;
}

RegexRule::RegexRule(Artifact_t *artifact) : ArchRule(artifact) {}

}  // namespace pcv
