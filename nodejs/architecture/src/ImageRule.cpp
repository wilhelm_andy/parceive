//
// Created by wilhelma on 12/7/17.
//

#include "ImageRule.h"

#include "ClassRule.h"
#include "FunctionRule.h"
#include "NamespaceRule.h"
#include "VariableRule.h"
#include "entities/Class.h"
#include "entities/Image.h"
#include "entities/Namespace.h"

namespace pcv {

using entity::Class;
using entity::Image;
using entity::Namespace;
using entity::Routine;
using entity::Variable;

ImageRule::ImageRule(const ImageRule &rule) : artifactName_(rule.artifactName_), rx_(rule.rx_) {}

ImageRule::ImageRule(const std::string &artifactName, const std::string &regexString)
  : artifactName_(artifactName), rx_(regexString)
{}

std::unique_ptr<pcv::ArchRule::artifacts_t> ImageRule::execute(pcv::Artifact_t &artifact,
                                                               const Context &ctxt)
{
  std::vector<const Image *> images;

  artifact_         = std::unique_ptr<Artifact_t>(new Artifact_t(artifactName_, &artifact));
  artifact_->entity = nullptr;

  for (auto &img : ctxt.images) {
    if (std::regex_match(img->name, rx_)) {
      images.push_back(img.get());
    }
  }

  added_t added;
  apply(artifact_.get(), images, &added);

  return nullptr;
}

std::unique_ptr<pcv::ArchRule::artifacts_t> ImageRule::append(pcv::Artifact_t &artifact,
                                                              const pcv::Context &ctxt)
{
  return nullptr;
}

std::unordered_map<const Image *, Artifact_t *> ImageRule::apply(
  Artifact_t *artifact, const std::vector<const Image *> &images, ArchRule::added_t *added)
{
  std::unordered_map<const Image *, Artifact_t *> addedImages;

  for (auto image : images) {
    artifact->children.emplace_back(
      std::unique_ptr<Artifact_t>{new Artifact_t(image->name, artifact)});

    auto parent        = artifact->children.back().get();
    parent->entity     = image;
    addedImages[image] = parent;

    {  // consider namespaces
      std::vector<const Namespace *> namespaces;
      for (const auto entity : image->entities) {
        if (entity->getEntityType() == pcv::entity::EntityType::Namespace) {
          namespaces.push_back(static_cast<const Namespace *>(entity));
        }
      }
      NamespaceRule nRule;
      nRule.apply(parent, namespaces, added);
    }

    {  // consider classes
      std::unordered_set<const Class *> classes;
      for (auto entity : image->entities) {
        if (entity->getEntityType() == pcv::entity::EntityType::Class) {
          if (entity->cls == nullptr && added->find(entity) == std::end(*added)) {
            classes.insert(static_cast<const Class *>(entity));
          }
        }
      }
      ClassRule cRule;
      auto tmpAdded = cRule.apply(parent, classes, false);
      for (auto cls : tmpAdded) added->insert(cls.first);
    }

    {  // consider routines
      std::unordered_set<const Routine *> routines;
      for (auto entity : image->entities) {
        if (entity->getEntityType() == pcv::entity::EntityType::Routine) {
          if (added->find(entity) == std::end(*added)) {
            routines.insert(static_cast<const Routine *>(entity));
          }
        }
      }

      FunctionRule fRule;
      auto tmpAdded = fRule.apply(parent, routines);
      added->insert(std::begin(tmpAdded), std::end(tmpAdded));
    }

    {  // consider global variables
      std::unordered_set<const Variable *> variables;
      for (auto entity : image->entities) {
        if (entity->getEntityType() == pcv::entity::EntityType::Variable) {
          if (added->find(entity) == std::end(*added)) {
            variables.insert(static_cast<const Variable *>(entity));
          }
        }
      }
      VariableRule vRule;
      auto tmpAdded = vRule.apply(*parent, variables);
      added->insert(std::begin(tmpAdded), std::end(tmpAdded));
    }
  }

  return addedImages;
}

}  // namespace pcv
