//
// Created by Faris Cakaric on 17.06.17.
//

#include "../include/VariableRule.h"

namespace pcv {

using entity::Variable;

VariableRule::VariableRule(const std::string &artifactName, const std::string &regexString)
  : artifactName_(artifactName), rx_(regexString)
{}

std::unique_ptr<ArchRule::artifacts_t> VariableRule::execute(Artifact_t &artifact,
                                                             const Context &ctxt)
{
  artifact_         = std::unique_ptr<Artifact_t>(new Artifact_t(artifactName_, &artifact));
  artifact_->entity = nullptr;

  std::unordered_set<const Variable *> variables;
  for (auto &variable : ctxt.variables) {
    if (std::regex_match(variable->name, rx_) || std::regex_match(variable->full_name, rx_)) {
      variables.insert(variable.get());
    }
  }

  apply(*artifact_, variables);

  return nullptr;
}

ArchRule::added_t VariableRule::apply(Artifact_t &artifact,
                                      const std::unordered_set<const Variable *> &variables)
{
  for (auto variable : variables) {
    artifact.children.emplace_back(new Artifact_t(variable->name, &artifact));
    auto newVariable    = artifact.children.back().get();
    newVariable->entity = variable;
    added_.insert(variable);
  }

  return added_;
}

std::unique_ptr<ArchRule::artifacts_t> VariableRule::append(Artifact_t &artifact,
                                                            const Context &ctxt)
{
  return nullptr;
}

VariableRule::VariableRule(const VariableRule &rule)
  : artifactName_(rule.artifactName_), rx_(rule.rx_), added_(rule.added_)
{}

}  // namespace pcv