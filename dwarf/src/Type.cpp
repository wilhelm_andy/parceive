//
// Created by wilhelma on 12/27/16.
//

#include "../include/Type.h"

#ifdef __GNUG__
#include <cxxabi.h>
#include <memory>

namespace pcv {
namespace dwarf {

std::string demangle(const char* name)
{
  int status{};

  std::unique_ptr<char, void (*)(void*)> res{abi::__cxa_demangle(name, nullptr, 0, &status),
                                             std::free};

  return (status == 0) ? res.get() : name;
}

std::string demangleNameOnly(const char* name)
{
  std::string demangledName = demangle(name);
  unsigned long start{0}, end{demangledName.size()};
  bool considerSpace{true};

  for (unsigned long i = 0; i < demangledName.size(); ++i) {
    switch (demangledName[i]) {
      case '(':
        end           = i;
        considerSpace = false;
        break;
      case ' ':
        if (considerSpace) start = i;
        break;
      case '<':
        considerSpace = false;
        break;
      default:
        continue;
    }
  }

  return demangledName.substr(start, end);
}

std::string demangleWithoutReturnType(const char* name)
{
  std::string demangledName = demangle(name);
  unsigned long start{0}, end{demangledName.size()};
  bool considerSpace{true};

  for (unsigned long i = 0; i < demangledName.size(); ++i) {
    switch (demangledName[i]) {
      case ' ':
        if (considerSpace) start = i + 1;
        break;
      case '<':
      case '(':
        considerSpace = false;
        break;
      default:
        continue;
    }
  }

  return demangledName.substr(start, end);
}

}  // namespace dwarf
}  // namespace pcv

#else

namespace pcv {
namespace dwarf {

// does nothing if not g++
std::string demangle(const char* name) { return name; }

}  // namespace dwarf
}  // namespace pcv

#endif
