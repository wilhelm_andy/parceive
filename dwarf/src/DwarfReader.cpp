//
// Created by wilhelma on 12/23/16.
//

#include "DwarfReader.h"

#include <algorithm>
#include <memory>
#include <sstream>

#include "entities/Namespace.h"
#include "tag/TagGeneric.h"

namespace {

using pcv::dwarf::SourceLocation;
using pcv::entity::Namespace;
using pcv::entity::Variable;

Namespace *getTopNamespace(Namespace *nmsp, Namespace *emptyNmsp)
{
  if (nmsp == emptyNmsp || nmsp->parent == emptyNmsp) return nmsp;

  return getTopNamespace(nmsp->parent, emptyNmsp);
}

std::unique_ptr<std::vector<const Variable *>> getVariablesOfType(
  const Variable::Type type, const DwarfContext &ctxt, const pcv::entity::Id_t imageId = 0)
{
  std::unique_ptr<std::vector<const Variable *>> vec{new std::vector<const Variable *>};

  for (auto &v : ctxt.variables) {
    if (v->type == type && (!imageId || v->img->id == imageId)) { vec->emplace_back(v.get()); }
  }

  return vec;
}

}  // namespace

namespace pcv {
namespace dwarf {

using pcv::entity::Namespace;

void DwarfReader::errHandler(Dwarf_Error err, Dwarf_Ptr errArg)
{
  std::cerr << "err: " << dwarf_errmsg(err) << '\n';
  auto dbg = static_cast<Dwarf_Debug>(errArg);
  dwarf_dealloc(dbg, err, DW_DLA_ERROR);
}

void DwarfReader::setCurrentFile(const std::string &fileName)
{
  fileGuard_ = std::unique_ptr<FileGuard>{new FileGuard(fileName.c_str())};
  dbgGuard_  = std::unique_ptr<DbgGuard>{new DbgGuard(fileGuard_->fd, errHandler)};
  ctxt_.dbg  = dbgGuard_->dbg;
}

bool DwarfReader::readSymInfo(Id_t imgId, offset_t off)
{
  Dwarf_Unsigned size{0}, rest{0};
  dwarf_get_section_max_offsets_b(
    ctxt_.dbg, &size, &rest, &rest, &rest, &rest, &rest, &rest, &rest, &rest, &rest, &rest, &rest);
  ctxt_.reset();
  dieDuplicate_.clear();
  ctxt_.setCurrentImage(std::unique_ptr<Image>{new Image(imgId, fileGuard_->fileName)}, off, size);
  iterateCUs([this](Dwarf_Die cuDie) { iterateToIdentifyExternals(cuDie); });
  iterateCUs([this](Dwarf_Die cuDie) {
    iterate(cuDie);
    ctxt_.clearCuSrcFiles();
  });
  ctxt_.finalizeSourceLocations();
  ctxt_.clearImgSrcFiles();
  return true;
}

#ifdef NODEJS
std::vector<pcv::dwarf::ImageInfo> getEnabledImages(const json11::Json &rules)
{
  std::vector<pcv::dwarf::ImageInfo> images;

  if (rules.is_array()) {
    for (const auto item : rules.array_items()) {
      if (item["type"].string_value().compare("Image") != 0) continue;
      images.emplace_back(pcv::dwarf::ImageInfo(
        static_cast<pcv::entity::Id_t>(item["id"].int_value()), item["path"].string_value()));
    }
  }

  return images;
}

DwarfReader::DwarfReader(const std::string &fileName,
                         DieDuplicate &dieDuplicate,
                         FileFilter &filter)
  : dieDuplicate_(dieDuplicate), filter_(filter)
{
  setCurrentFile(fileName);

  ctxt_.namespaces.emplace_back(
    new Namespace(0, std::string(""), nullptr, nullptr, nullptr, 0, nullptr));
  ctxt_.emptyNamespace = ctxt_.currentNamespace = ctxt_.namespaces.back().get();
}

void DwarfReader::processContext()
{
  ctxt_.demangleRoutines();
  ctxt_.establishInheritance();
  ctxt_.establishComposition();
  ctxt_.establishTypedefs();
}

void DwarfReader::start(const json11::Json &rules)
{
  clearContext();
  auto images = getEnabledImages(rules);

  if (images.empty()) {
    readSymInfo(1);
    processContext();
  }

  for (auto &image : images) {
    setCurrentFile(image.filePath);
    readSymInfo(image.imageId);
    processContext();
  }
}

void DwarfReader::assignRegex(const std::string &include, const std::string &exclude)
{
  filter_.assign(include, exclude);
}

void DwarfReader::clearContext()
{
  ctxt_.reset();
  ctxt_.images.clear();
  ctxt_.routines.clear();
  ctxt_.classes.clear();
  ctxt_.variables.clear();
  ctxt_.files.clear();

  ctxt_.namespaces.clear();
  ctxt_.namespaces.emplace_back(
    new Namespace(0, std::string(""), nullptr, nullptr, nullptr, 0, nullptr));
  ctxt_.emptyNamespace = ctxt_.currentNamespace = ctxt_.namespaces.back().get();
}

#else  // NOT NODEJS
DwarfReader::DwarfReader(Filter *f) : filter_(f)
{
  ctxt_.namespaces.emplace_back(
    new Namespace(0, std::string(""), nullptr, nullptr, nullptr, 0, nullptr));
  ctxt_.emptyNamespace = ctxt_.currentNamespace = ctxt_.namespaces.back().get();
}
#endif

bool DwarfReader::readSymInfo(const std::string &fileName, Id_t imageId, offset_t offset)
{
  setCurrentFile(fileName);
  return readSymInfo(imageId, offset);
}

bool DwarfReader::isBinaryOptimized(const std::string &fileName)
{
  setCurrentFile(fileName);
  return hasOptimizedCUs();
}

void DwarfReader::clearSymInfo() { ctxt_.clearCache(); }

entity::ImageScopedId DwarfReader::getUniqueClassId(const Id_t imageId,
                                                    const std::string &rtnName) const
{
  auto altConstructor = ctxt_.getFixedConstructorName(rtnName);

  auto rtn = ctxt_.getRoutine(imageId, rtnName);
  if (!rtn) rtn = ctxt_.getRoutine(imageId, altConstructor);
  if (rtn && rtn->isConstructor &&
      rtn->cls) {  // check rtn->cls due to missing debug info for class
    return rtn->cls->getUniqueId();
  }

  return entity::ImageScopedId::getNone();
}

std::string DwarfReader::refineRtnName(const Id_t imageId, const std::string &rtnName)
{
  int numNmsp{0};

  {
    std::string pStr{rtnName};
    const Namespace *nmsp = ctxt_.namespaces.at(0).get();

    while (1) {
      std::size_t pos  = pStr.find("::");
      std::string left = pStr.substr(0, pos);
      pStr             = pStr.substr(pos + 2);

      auto it = std::find_if(begin(nmsp->children),
                             end(nmsp->children),
                             [&left](const Namespace *n) { return n->name == left; });
      if (it == end(nmsp->children)) break;

      nmsp = *it;
      ++numNmsp;
    }
  }

  std::string res{rtnName};
  if (numNmsp > 0) {
    std::stringstream referenceId;
    referenceId << numNmsp;
    res.insert(0, referenceId.str());
  }

  return res;
}

std::unique_ptr<std::vector<const entity::Variable *>> DwarfReader::getGlobalVariables(
  const Id_t imageId) const
{
  return getVariablesOfType(Variable::Type::GLOBAL, ctxt_, imageId);
}

std::unique_ptr<std::vector<const entity::Variable *>> DwarfReader::getStaticVariables(
  const Id_t imageId) const
{
  return getVariablesOfType(Variable::Type::STATICVAR, ctxt_, imageId);
}

const Variable *DwarfReader::getMemberVariable(const entity::ImageScopedId &clsId,
                                               const Class::offset_t offset) const
{
  auto cls = ctxt_.get<Class>(clsId.imageId, clsId.id);
  if (cls && !cls->members.empty() && static_cast<size_t>(offset) < cls->size) {
    auto it =
      std::upper_bound(begin(cls->members),
                       end(cls->members),
                       offset,
                       [](const Class::offset_t &off, Variable *var) { return off < var->offset; });
    if (it != std::begin(cls->members)) {
      --it;
      if ((*it)->declaringClass)  // nested classes
        return getMemberVariable({clsId.imageId, (*it)->declaringClass->id}, offset - (*it)->offset);
      return *it;
    }
  }

  return nullptr;
}

const entity::Routine *DwarfReader::getRoutine(const Id_t imageId, const std::string rtnName) const
{
  return ctxt_.getRoutine(imageId, rtnName);
}

void DwarfReader::iterateCUs(std::function<void(Dwarf_Die)> handleCU) noexcept
{
  Dwarf_Unsigned cu_header_length, abbrev_offset, next_cu_header, typeOffset;
  Dwarf_Half version_stamp, address_size, offset_size, extension_size;
  Dwarf_Sig8 signature{0};
  Dwarf_Die cu_die;

  int res{DW_DLV_OK};
  while (res == DW_DLV_OK) {
    res = dwarf_next_cu_header_c(ctxt_.dbg,
                                 IS_INFO,
                                 &cu_header_length,
                                 &version_stamp,
                                 &abbrev_offset,
                                 &address_size,
                                 &offset_size,
                                 &extension_size,
                                 &signature,
                                 &typeOffset,
                                 &next_cu_header,
                                 nullptr);
    if (res == DW_DLV_ERROR) throw DwarfError("Error with dwarf_next_cu_header()");

    Dwarf_Error err;
    if (dwarf_siblingof_b(ctxt_.dbg, nullptr, IS_INFO, &cu_die, &err) == DW_DLV_OK) {
      handleCU(cu_die);
      dwarf_dealloc(ctxt_.dbg, cu_die, DW_DLA_DIE);
    }
  }
}

bool DwarfReader::hasOptimizedCUs()
{
  Dwarf_Unsigned cu_header_length, abbrev_offset, next_cu_header, typeOffset;
  Dwarf_Half version_stamp, address_size, offset_size, extension_size;
  Dwarf_Sig8 signature{0};
  Dwarf_Die cu_die;

  int res{DW_DLV_OK};
  bool foundNoCU{true};
  while (res == DW_DLV_OK) {
    res = dwarf_next_cu_header_c(ctxt_.dbg,
                                 IS_INFO,
                                 &cu_header_length,
                                 &version_stamp,
                                 &abbrev_offset,
                                 &address_size,
                                 &offset_size,
                                 &extension_size,
                                 &signature,
                                 &typeOffset,
                                 &next_cu_header,
                                 nullptr);
    if (res == DW_DLV_ERROR) throw DwarfError("Error with dwarf_next_cu_header()");

    Dwarf_Error err;
    if (dwarf_siblingof_b(ctxt_.dbg, nullptr, IS_INFO, &cu_die, &err) == DW_DLV_OK) {
      foundNoCU = false;

      char *tmpText;
      getAttrText(ctxt_.dbg, cu_die, DW_AT_producer, &tmpText);
      dwarf_dealloc(ctxt_.dbg, cu_die, DW_DLA_DIE);
      std::string prodText{tmpText};

      if (prodText.find("-O") != std::string::npos && prodText.find("-O0") == std::string::npos &&
          prodText.find("-Og") == std::string::npos)
        return true;
    }
  }

  return foundNoCU;
}

void DwarfReader::iterateToIdentifyExternals(Dwarf_Die die) noexcept
{
  Dwarf_Die child_die{}, sib_die{};
  ctxt_.die = die;
  if (!handleExternal(die)) {
    if (dwarf_child(die, &child_die, nullptr) == DW_DLV_OK) {
      iterateToIdentifyExternals(child_die);
      dwarf_dealloc(ctxt_.dbg, child_die, DW_DLA_DIE);
    }
  }

  ctxt_.die = die;
  leaveExternal(die);

  if (dwarf_siblingof_b(ctxt_.dbg, die, IS_INFO, &sib_die, nullptr) == DW_DLV_OK) {
    iterateToIdentifyExternals(sib_die);
    dwarf_dealloc(ctxt_.dbg, sib_die, DW_DLA_DIE);
  }
}

void DwarfReader::iterate(Dwarf_Die die) noexcept
{
  Dwarf_Off off{};
  dwarf_dieoffset(die, &off, nullptr);

  if (!ctxt_.isSymbolExternal(off)) {  // process child
    Dwarf_Die child_die{};
    ctxt_.die      = die;
    auto duplicate = ctxt_.duplicate = dieDuplicate_.isDuplicate(ctxt_);
    if (duplicate)
      dieDuplicate_.addDuplicate(ctxt_);
    else
      dieDuplicate_.addDie(ctxt_);

    if (!handle(die)) {
      if (dwarf_child(die, &child_die, nullptr) == DW_DLV_OK) {
        iterate(child_die);
        dwarf_dealloc(ctxt_.dbg, child_die, DW_DLA_DIE);
      }
    }

    ctxt_.die       = die;
    ctxt_.duplicate = duplicate;
    leave(die);
  }

  {  // process siblings
    Dwarf_Die sib_die{};
    if (dwarf_siblingof_b(ctxt_.dbg, die, IS_INFO, &sib_die, nullptr) == DW_DLV_OK) {
      iterate(sib_die);
      dwarf_dealloc(ctxt_.dbg, sib_die, DW_DLA_DIE);
    }
  }
}

void DwarfReader::leaveExternal(Dwarf_Die die)
{
  Dwarf_Half tag{};
  if (dwarf_tag(die, &tag, nullptr) != DW_DLV_OK) throw DwarfError("dwarf_tag() failed");

  leaveExternalDwarfDie(ctxt_, tag);
}

void DwarfReader::leave(Dwarf_Die die)
{
  Dwarf_Half tag{};
  if (dwarf_tag(die, &tag, nullptr) != DW_DLV_OK) throw DwarfError("dwarf_tag() failed");

  if (isExcluded(die)) return;

  leaveDwarfDie(ctxt_, tag);
}

bool DwarfReader::isExcluded(Dwarf_Die die)
{
  Dwarf_Half tag{};
  if (dwarf_tag(die, &tag, nullptr) != DW_DLV_OK) throw DwarfError("dwarf_tag");

  if (tag != DW_TAG_namespace && hasAttr(die, DW_AT_decl_file)) {
    Dwarf_Unsigned fileNo{};
    getAttrUint(ctxt_.dbg, ctxt_.die, DW_AT_decl_file, &fileNo);
#ifdef NODEJS
    Dwarf_Unsigned lineNo{};
    if (hasAttr(die, DW_AT_decl_line))
      getAttrUint(ctxt_.dbg, ctxt_.die, DW_AT_decl_line, &lineNo);
    else
      lineNo = 1;
    if (!filter_.isIncluded(ctxt_.getCuSrcFileName(fileNo - 1))) return true;
      // if (!filter_.isIncluded(ctxt_.getCuSrcFileName(fileNo - 1), lineNo)) return true;
#else
    if (!filter_->isIncluded(ctxt_.getCuSrcFileName(fileNo - 1))) return true;
#endif
  }

  return false;
}

bool DwarfReader::handleExternal(Dwarf_Die die)
{
  Dwarf_Half tag{};
  if (dwarf_tag(die, &tag, nullptr) != DW_DLV_OK) throw DwarfError("dwarf_tag() failed");

  return handleExternalDwarfDie(ctxt_, tag);
}

bool DwarfReader::handle(Dwarf_Die die)
{
  Dwarf_Half tag{};
  if (dwarf_tag(die, &tag, nullptr) != DW_DLV_OK) throw DwarfError("dwarf_tag() failed");

  if (isExcluded(die)) return true;

  return handleDwarfDie(ctxt_, tag);
}

std::vector<const entity::File *> DwarfReader::getFiles(Id_t imageId)
{
  std::vector<const entity::File *> vec;

  const auto &imgIt =
    std::find_if(std::begin(ctxt_.images),
                 std::end(ctxt_.images),
                 [&imageId](const std::unique_ptr<Image> &img) { return img->id == imageId; });
  if (imgIt != std::end(ctxt_.images)) {
    for (const auto &file : (*imgIt)->files) {
      vec.emplace_back(static_cast<const entity::File *>(file));
    }
  }

  return vec;
}

bool DwarfReader::getSourceLocation(const S_ADDRUINT &ip, int *line, std::string *file) const
{
  const auto &loc = ctxt_.getSourceLocation(ip);

  if (loc == nullptr) {
    if (line) *line = 0;
    return false;
  }

  if (line) *line = loc->lineNo;
  if (file) *file = (loc->file) ? loc->file->name : "";
  return true;
}

}  // namespace dwarf
}  // namespace pcv
