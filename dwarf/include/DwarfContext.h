//
// Created by wilhelma on 12/27/16.
//

#ifndef DWARFLOADER_CONTEXT_H
#define DWARFLOADER_CONTEXT_H

#include <libdwarf.h>
#include <map>
#include <memory>
#include <stack>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include "Context.h"
#include "entities/Class.h"
#include "entities/File.h"
#include "entities/Image.h"
#include "entities/Namespace.h"
#include "entities/Routine.h"
#include "entities/Variable.h"

namespace pcv {

class Parceive;

namespace dwarf {

using pcv::entity::Class;
using pcv::entity::File;
using pcv::entity::Image;
using pcv::entity::Namespace;
using pcv::entity::Routine;
using pcv::entity::SoftwareEntity;
using pcv::entity::Variable;

struct ClassRelation : public pcv::Context {
  Dwarf_Off first;
  Dwarf_Off second;
  ClassRelation(Dwarf_Off baseOff, Dwarf_Off inhOff) : first(baseOff), second(inhOff) {}
};

struct SourceLocation {
  uint lineNo;
  File* file;
  explicit SourceLocation(uint lineNo, File* file) : lineNo(lineNo), file(file) {}
};

struct DwarfContext : public Context {
  using offset_t = uint64_t;

  Parceive* pParceive;

  Dwarf_Debug dbg{};
  Dwarf_Die die{};

  Image* currentImage{nullptr};
  Namespace* currentNamespace{nullptr};
  Namespace* emptyNamespace{nullptr};
  std::vector<Class*> currentClass;
  std::stack<Routine*> currentRoutine;

  Dwarf_Off duplicate{0};

  std::unordered_set<Dwarf_Die> toClean;

  /// clear the whole context
  void clearContext();

  /// add typedef with name on dwarf offset
  void addTypedef(Dwarf_Off off, const std::string& name);

  /// add inheritance with base class at offset baseOff and inherited class on offset inhOff
  void addInheritance(Dwarf_Off baseOff, Dwarf_Off inhOff);

  /// add composition with classes on offsets first and second
  void addComposition(Dwarf_Off first, Dwarf_Off second);

  /// demangle all function names
  void demangleRoutines();

  /// establish inheritances based on traced pairs of offsets
  void establishInheritance();

  /// establish composition based on traced pairs of offsets
  void establishComposition();

  /// establish typedefs based on traced typedef information
  void establishTypedefs();

  /// add a class to the context
  void addClass(Dwarf_Off off, Class* cls);

  /// return the pointer to the class entity with the given namespace and the name
  Class* getClass(const Namespace& nmsp, const Class::name_t& className) const;

  /// return an formalized class specifier
  std::string getClassSpecifier(const std::string& nmsp, const Class::name_t& className) const;

  /// add routine to the entities
  void addRoutine(Dwarf_Off off, std::unique_ptr<Routine> rtn);

  /// link the routine with a name to a dwarf routine to be used during analysis
  void linkNameToRoutine(const std::string& rtnName, Routine* rtn);

  /// get the pointer to the routine entity with the given name
  Routine* getRoutine(const pcv::entity::Id_t imageId, const Routine::name_t& name) const;

  // add a member variable at the given dwarf offset
  void addMember(Dwarf_Off off, Variable* member);

  /// source locations add the source location at the given instruction pointer
  void addSourceLocation(const Dwarf_Addr& ip,
                         std::unique_ptr<pcv::dwarf::SourceLocation> loc) noexcept;

  /// return the source location at the given instruction pointer
  SourceLocation* getSourceLocation(Dwarf_Addr ip) const;

  /// add a software entity at the given dwarf offset
  void add(Dwarf_Off off, SoftwareEntity* entity);

  /// a workaround for the following problem (C1 and C2 constructor emitted): return the other name
  const Routine::name_t getFixedConstructorName(const Routine::name_t& cstr) const;

  /// reset the context
  void reset() noexcept;

  /// clear information that is not necessary during instrumentation
  void clearCache() noexcept;

  /// sets the current image containing the image id, its offset, and the necessary debug array size
  void setCurrentImage(std::unique_ptr<Image> image, offset_t imgOffset, size_t dwarfSize);

  /// finalize the mapped source locations such that they contain a final dummy entry
  void finalizeSourceLocations();

  /// returns true if symbol at given offset is marked external
  bool isSymbolExternal(Dwarf_Off off) const;

  /// mark symbol at the given offset as being external
  void markSymbolAsExternal(Dwarf_Off off);

  /// mark symbol at the given offset as being not external
  void unmarkSymbolAsExternal(Dwarf_Off off);

  /// add the class with the given offset as being traversed
  void pushCurrentClassOffset(Dwarf_Off off);

  /// remove the class with the given offset as being traversed
  void popCurrentClassOffset();

  /// adjusts the source location according to their image's offset
  void transformSourceLocations(Dwarf_Addr low_pc,
                                Dwarf_Addr high_pc,
                                uint32_t lineNo,
                                uint32_t file);

  void addCuSrcFile(std::string fileName);
  File* getCuSrcFile(size_t pos);
  std::string getCuSrcFileName(size_t pos) const;
  void clearCuSrcFiles();
  void clearImgSrcFiles();

  /// get the entity at the given dwarf offset
  template <typename T>
  T* get(Dwarf_Off off) const;

  /// get the entity at the given dwarf offset in the image with the given id
  template <typename T>
  T* get(uint32_t imgId, Dwarf_Off off) const;

 private:
  using sourceLocations_t = std::map<Dwarf_Addr, std::unique_ptr<pcv::dwarf::SourceLocation>>;

  struct ExternalSymbol {
    bool isExternal{false};
    Dwarf_Off parentOffset;

    ExternalSymbol(Dwarf_Off parentOffset) : parentOffset(parentOffset) {}
  };

  std::unordered_map<uint32_t, size_t> vectorSize_;
  size_t currentVectorSize_{0};
  std::unordered_map<uint32_t, std::unique_ptr<std::vector<SoftwareEntity*>>> entities_;
  std::vector<SoftwareEntity*>* currentEntity_;
  sourceLocations_t sourceLocations_;
  std::unordered_map<Dwarf_Off, std::string> offTypedefName_;
  std::unordered_map<Class::name_t, Class*> nameClassMap_;
  std::unordered_map<Routine::name_t, Routine*> nameRtnMap_;
  std::vector<ClassRelation> inheritances_;
  std::vector<ClassRelation> compositions_;
  std::vector<Dwarf_Off> currentClassOffset_;
  std::unique_ptr<std::vector<ExternalSymbol>> externalSymbols_;
  offset_t imgOffset_;

  std::vector<std::string> cuSrcFiles_;
  std::unordered_map<std::string, File*> imageSrcFiles_;

  /// reserve the vector to store #<elements> pointers to software entities from debug information
  void reserveEntityVector(uint32_t imgId, size_t elements);
};

template <typename T>
T* DwarfContext::get(Dwarf_Off off) const
{
  if (off < currentVectorSize_) return static_cast<T*>(currentEntity_->at(off));
  return nullptr;
}

template <typename T>
T* DwarfContext::get(uint32_t imgId, Dwarf_Off off) const
{
  if (off < vectorSize_.at(imgId)) {
    auto entities = entities_.at(imgId).get();
    return static_cast<T*>(entities->at(off));
  }
  return nullptr;
}

}  // namespace dwarf
}  // namespace pcv

#endif  // DWARFLOADER_CONTEXT_H
