//
// Created by wilhelma on 12/23/16.
//

#ifndef DWARFLOADER_TAGCOMPILEUNIT_H
#define DWARFLOADER_TAGCOMPILEUNIT_H

#include "../include/DwarfHelper.h"
#include "./TagGeneric.h"
#include "entities/File.h"

namespace pcv {
namespace dwarf {

static std::string fixPath(const char *path)
{
  std::string pathStr(path);

  while (pathStr.find("/./") != std::string::npos) pathStr.erase(pathStr.find("/./") + 1, 2);

  while (pathStr.find("/../") != std::string::npos) {
    int lastSlash  = pathStr.find("/../");
    int firstSlash = pathStr.rfind("/", lastSlash - 1);
    pathStr.erase(firstSlash, lastSlash + 3 - firstSlash);
  }

  return pathStr;
};

void parseSourceLocationInfo(pcv::dwarf::DwarfContext &ctxt)
{
  Dwarf_Unsigned version{0};
  Dwarf_Small count{0};
  Dwarf_Line_Context context{0};

  if (dwarf_srclines_b(ctxt.die, &version, &count, &context, nullptr) != DW_DLV_OK)
    throw DwarfError("dwarf_srclines_b");

  if (count == 1) {
    Dwarf_Line *lineBuf;
    Dwarf_Signed lineCount{0};

    if (dwarf_srclines_from_linecontext(context, &lineBuf, &lineCount, nullptr) != DW_DLV_OK)
      throw DwarfError("dwarf_srclines_from_linecontext");

    for (auto i = 0; i < lineCount; ++i) {
      Dwarf_Addr lineAddr{0};
      Dwarf_Unsigned lineNo{0}, fileNo{0};

      if (dwarf_lineaddr(lineBuf[i], &lineAddr, nullptr) != DW_DLV_OK)
        throw DwarfError("dwarf_lineaddr");

      if (dwarf_lineno(lineBuf[i], &lineNo, nullptr) != DW_DLV_OK) throw DwarfError("dwarf_lineno");

      if (dwarf_line_srcfileno(lineBuf[i], &fileNo, nullptr) != DW_DLV_OK)
        throw DwarfError("dwarf_line_srcfileno");

      ctxt.addSourceLocation(
        lineAddr,
        std::unique_ptr<SourceLocation>{new SourceLocation(lineNo, ctxt.getCuSrcFile(fileNo - 1))});
    }
  }

  dwarf_srclines_dealloc_b(context);
}

template <>
struct TagHandler<DW_TAG_compile_unit> {
  static bool handle(pcv::dwarf::DwarfContext &ctxt)
  {
    {  // collect src files of cu
      char **srcfiles;
      Dwarf_Signed cnt;

      if (dwarf_srcfiles(ctxt.die, &srcfiles, &cnt, nullptr) == DW_DLV_OK) {
        for (int i = 0; i < cnt; ++i) {
          ctxt.addCuSrcFile(fixPath(srcfiles[i]));
          dwarf_dealloc(ctxt.dbg, srcfiles[i], DW_DLA_STRING);
        }
        dwarf_dealloc(ctxt.dbg, srcfiles, DW_DLA_LIST);
      }
    }

    parseSourceLocationInfo(ctxt);

    return false;  // continue
  }
  static bool handleDuplicate(DwarfContext &ctxt) { return false; }
};

}  // namespace dwarf
}  // namespace pcv

#endif  // DWARFLOADER_TAGCOMPILEUNIT_H
