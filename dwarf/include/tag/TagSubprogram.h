//
// Created by wilhelma on 12/23/16.
//

#ifndef DWARFLOADER_TAGSUBPROGRAM_H
#define DWARFLOADER_TAGSUBPROGRAM_H

#include <cstring>

#include "DwarfContext.h"
#include "DwarfReader.h"
#include "Type.h"
#include "entities/File.h"
#include "entities/Routine.h"
#include "tag/TagGeneric.h"

namespace {

static std::unordered_map<Dwarf_Off, pcv::dwarf::Routine *> handled;

bool isValid(const DwarfContext &ctxt, Dwarf_Die die)
{
#ifdef NODEJS
  return !pcv::dwarf::hasAttr(die, DW_AT_artificial);
#else
  return !(ctxt.currentClass.empty() && pcv::dwarf::hasAttr(die, DW_AT_artificial));
#endif
}

}  // namespace

namespace pcv {
namespace dwarf {

pcv::entity::Routine *getSpecRoutine(const DwarfContext &ctxt, Dwarf_Die die)
{
  Dwarf_Off specOff{};
  Dwarf_Die specDie{0};

  // specification
  if (hasAttr(die, DW_AT_declaration)) specDie = die;

  // declaration
  else if (hasAttr(die, DW_AT_specification))
    specDie = jump(ctxt.dbg, die, DW_AT_specification);

  // abstract origin
  else if (hasAttr(die, DW_AT_abstract_origin)) {
    auto declDie = jump(ctxt.dbg, die, DW_AT_abstract_origin);
    if (hasAttr(declDie, DW_AT_specification)) {
      specDie = jump(ctxt.dbg, declDie, DW_AT_specification);
    }
    dwarf_dealloc(ctxt.dbg, declDie, DW_DLA_DIE);
  }

  if (!specDie) return nullptr;

  dwarf_dieoffset(specDie, &specOff, nullptr);
  if (!hasAttr(die, DW_AT_declaration)) dwarf_dealloc(ctxt.dbg, specDie, DW_DLA_DIE);
  return ctxt.get<Routine>(specOff);
}

bool inspectConstructors(DwarfContext &ctxt, Dwarf_Die die)
{
  char *rtnName = nullptr;
  if (getDieName(ctxt.dbg, die, &rtnName)) {
    std::string uName = demangleNameOnly(rtnName);
    size_t pos2{uName.rfind("::")};
    if (pos2 != std::string::npos) {
      size_t pos1{uName.rfind("::", pos2 - 1)};
      if (pos1 != std::string::npos)
        pos1 += 2;
      else
        pos1 = 0;

      std::string className = uName.substr(pos1, pos2 - pos1);
      auto pos              = className.find_first_of('<');
      if (pos != std::string::npos) className = className.substr(0, pos);

      const std::string methodName = uName.substr(pos2 + 2);

      if (className == methodName) {
        if (hasAttr(die, DW_AT_abstract_origin)) {
          auto rtn = getSpecRoutine(ctxt, die);
          if (rtn != nullptr) {
            rtn->isConstructor = true;
            ctxt.linkNameToRoutine(rtnName, rtn);
          }

          return true;
        }
      }
    }
  }
  return false;
}

bool handleSubProgram(DwarfContext &ctxt, Dwarf_Die die, Dwarf_Off off = 0)
{
  if (isValid(ctxt, die)) {
    if (off) {
      Dwarf_Off specOff;
      if (dwarf_dieoffset(die, &specOff, nullptr) != DW_DLV_OK) throw DwarfError("offset");
      auto search = handled.find(specOff);
      if (search != handled.end()) {
        ctxt.currentRoutine.push(search->second);
        ctxt.toClean.insert(ctxt.die);
        return false;  // continue
      }
    }

    char *rtnName{nullptr};
    if (!getDieName(ctxt.dbg, die, &rtnName)) return true;  // stop (probably inlined constructor)

    // handle constructors
    bool isConstructor{!ctxt.currentClass.empty() && (ctxt.currentClass.back()->name == rtnName)};
    if (std::strstr(rtnName, "C1") != nullptr || std::strstr(rtnName, "C2") != nullptr ||
        std::strstr(rtnName, "C3") != nullptr) {
      inspectConstructors(ctxt, die);
    }

    if (!off && (dwarf_dieoffset(die, &off, nullptr) != DW_DLV_OK)) throw DwarfError("offset");

    Dwarf_Unsigned fileNo{0}, lineNo{0};
    pcv::entity::File *file{nullptr};
    if (hasAttr(die, DW_AT_decl_file)) {
      getAttrUint(ctxt.dbg, die, DW_AT_decl_file, &fileNo);
      file = ctxt.getCuSrcFile(fileNo - 1);
      getAttrUint(ctxt.dbg, die, DW_AT_decl_line, &lineNo);
    }

    if (lineNo > 0 || isConstructor) {
      auto cls = ctxt.currentClass.empty() ? nullptr : ctxt.currentClass.back();
      if (cls == nullptr) {
        auto specRtn = getSpecRoutine(ctxt, die);
        if (specRtn != nullptr) cls = specRtn->cls;
      }

      auto rtn = std::unique_ptr<Routine>{new Routine(
        off, rtnName, ctxt.currentImage, ctxt.currentNamespace, cls, file, lineNo, isConstructor)};
      ctxt.addRoutine(off, std::move(rtn));
      handled[off] = ctxt.routines.back().get();

      if (!ctxt.currentClass.empty())
        ctxt.currentClass.back()->methods.push_back(ctxt.routines.back().get());

      ctxt.currentRoutine.push(ctxt.routines.back().get());
      ctxt.toClean.insert(ctxt.die);

      return false;  // continue
    }
  }
  return true;  // stop
}

template <>
struct TagHandler<DW_TAG_subprogram> {
  static bool handle(DwarfContext &ctxt)
  {
    bool handled = false;

    if (hasAttr(ctxt.die, DW_AT_specification)) {
      Dwarf_Off off;
      auto specDie = jump(ctxt.dbg, ctxt.die, DW_AT_specification);
      if (dwarf_dieoffset(ctxt.die, &off, 0) == DW_DLV_OK)
        handled = handleSubProgram(ctxt, specDie, off);
      dwarf_dealloc(ctxt.dbg, specDie, DW_DLA_DIE);
    } else {
      handled = handleSubProgram(ctxt, ctxt.die);
    }

    return handled;
  }
  static bool handleDuplicate(DwarfContext &ctxt)
  {
    auto rtn = ctxt.get<Routine>(ctxt.duplicate);
    if (rtn) {
      Dwarf_Off off{};
      if (dwarf_dieoffset(ctxt.die, &off, nullptr) != DW_DLV_OK) throw DwarfError("dieoffset");
      handled[off] = rtn;
      ctxt.currentRoutine.emplace(rtn);
      ctxt.toClean.insert(ctxt.die);
      return false;  // continue
    }

    return true;  // original not considered => do not consider duplicate
  }

  static bool handleExternal(DwarfContext &ctxt)
  {
    Dwarf_Off off{};

    if (hasAttr(ctxt.die, DW_AT_declaration)) {
      if (dwarf_dieoffset(ctxt.die, &off, nullptr) != DW_DLV_OK) throw DwarfError("offset");
      ctxt.markSymbolAsExternal(off);
    } else if (hasAttr(ctxt.die, DW_AT_specification)) {
      auto specDie = jump(ctxt.dbg, ctxt.die, DW_AT_specification);
      if (dwarf_dieoffset(specDie, &off, nullptr) != DW_DLV_OK) throw DwarfError("offset");
      dwarf_dealloc(ctxt.dbg, specDie, DW_DLA_DIE);

      ctxt.unmarkSymbolAsExternal(off);
    } else {
      if (dwarf_dieoffset(ctxt.die, &off, nullptr) != DW_DLV_OK) throw DwarfError("offset");
      ctxt.unmarkSymbolAsExternal(off);
    }

    return true;
  }
};

void leaveRoutine(DwarfContext &ctxt)
{
  auto it = ctxt.toClean.find(ctxt.die);
  if (it != std::end(ctxt.toClean)) {
    ctxt.currentRoutine.pop();
    ctxt.toClean.erase(it);
  }
}

template <>
struct TagLeaver<DW_TAG_subprogram> {
  static void leave(DwarfContext &ctxt) { leaveRoutine(ctxt); }

  static void leaveDuplicate(DwarfContext &ctxt) { leaveRoutine(ctxt); }
};

}  // namespace dwarf
}  // namespace pcv

#endif  // DWARFLOADER_TAGSUBPROGRAM_H
