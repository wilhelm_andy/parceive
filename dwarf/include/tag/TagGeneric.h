//
// Created by wilhelma on 12/23/16.
//

#ifndef DWARFLOADER_TAGHANDLER_H
#define DWARFLOADER_TAGHANDLER_H

#include <dwarf.h>
#include <libdwarf.h>
#include <cstdlib>

#include "DwarfContext.h"
#include "DwarfHelper.h"
#include "Type.h"

#include "entities/Variable.h"

namespace pcv {
namespace dwarf {

template <Dwarf_Half tag>
struct TagHandler {
  static bool handle(DwarfContext & /* ctxt */)
  {
    std::cerr << "Tag " << tag << " declared but not defined!\n";
    abort();
  }
  static bool handleDuplicate(DwarfContext & /* ctxt */)
  {
    std::cerr << "Tag " << tag << " declared but not defined!\n";
    abort();
  }
  static void handleExternal(DwarfContext & /* ctxt */)
  {
    std::cerr << "Tag " << tag << " declared but not defined!\n";
    abort();
  }
};

template <Dwarf_Half tag>
struct TagLeaver {
  static void leave(DwarfContext & /* ctxt */)
  {
    std::cerr << "Tag " << tag << " declared but not defined!\n";
    abort();
  }
  static void leaveDuplicate(DwarfContext & /* ctxt */)
  {
    std::cerr << "Tag " << tag << " declared but not defined!\n";
    abort();
  }
  static void leaveExternal(DwarfContext & /* ctxt */)
  {
    std::cerr << "Tag " << tag << " declared but not defined!\n";
    abort();
  }
};

/// handle die -------------------------------------------------------------------------------------
template <Dwarf_Half...>
struct TagList {
};

bool handleDie(DwarfContext &, Dwarf_Half, TagList<>) { return false; }

template <Dwarf_Half I, Dwarf_Half... N>
bool handleDie(DwarfContext &ctxt, Dwarf_Half i, TagList<I, N...>)
{
  if (i != I) {
    return handleDie(ctxt, i, TagList<N...>());
  }

  if (ctxt.duplicate)
    return TagHandler<I>::handleDuplicate(ctxt);
  else
    return TagHandler<I>::handle(ctxt);
}

template <Dwarf_Half... N>
bool handleDie(DwarfContext &ctxt, Dwarf_Half tag)
{
  return handleDie(ctxt, tag, TagList<N...>());
}

using handleDwarfDie_t           = bool(DwarfContext &, Dwarf_Half);
handleDwarfDie_t *handleDwarfDie = handleDie<DW_TAG_compile_unit,
                                             DW_TAG_subprogram,
                                             DW_TAG_variable,
                                             DW_TAG_formal_parameter,
                                             DW_TAG_class_type,
                                             DW_TAG_structure_type,
                                             DW_TAG_typedef,
                                             DW_TAG_member,
                                             DW_TAG_namespace,
                                             DW_TAG_inheritance,
                                             DW_TAG_inlined_subroutine,
                                             DW_TAG_union_type>;

/// leave die --------------------------------------------------------------------------------------
void leaveDie(DwarfContext &, Dwarf_Half, TagList<>) {}

template <Dwarf_Half I, Dwarf_Half... N>
void leaveDie(DwarfContext &ctxt, Dwarf_Half i, TagList<I, N...>)
{
  if (i != I) {
    return leaveDie(ctxt, i, TagList<N...>());
  }

  if (ctxt.duplicate)
    TagLeaver<I>::leaveDuplicate(ctxt);
  else
    TagLeaver<I>::leave(ctxt);
}

template <Dwarf_Half... N>
void leaveDie(DwarfContext &ctxt, Dwarf_Half tag)
{
  return leaveDie(ctxt, tag, TagList<N...>());
}

using leaveDwarfDie_t          = void(DwarfContext &, Dwarf_Half);
leaveDwarfDie_t *leaveDwarfDie = leaveDie<DW_TAG_subprogram,
                                          DW_TAG_namespace,
                                          DW_TAG_class_type,
                                          DW_TAG_structure_type,
                                          DW_TAG_union_type>;

/// handle external die ----------------------------------------------------------------------------
bool handleExternalDie(DwarfContext &, Dwarf_Half, TagList<>) { return false; }

template <Dwarf_Half I, Dwarf_Half... N>
bool handleExternalDie(DwarfContext &ctxt, Dwarf_Half i, TagList<I, N...>)
{
  if (i != I) {
    return handleExternalDie(ctxt, i, TagList<N...>());
  }

  return TagHandler<I>::handleExternal(ctxt);
}

template <Dwarf_Half... N>
bool handleExternalDie(DwarfContext &ctxt, Dwarf_Half tag)
{
  return handleExternalDie(ctxt, tag, TagList<N...>());
}

handleDwarfDie_t *handleExternalDwarfDie =
  handleExternalDie<DW_TAG_subprogram, DW_TAG_class_type, DW_TAG_structure_type>;

/// leave external die -----------------------------------------------------------------------------
void leaveExternalDie(DwarfContext &, Dwarf_Half, TagList<>) {}

template <Dwarf_Half I, Dwarf_Half... N>
void leaveExternalDie(DwarfContext &ctxt, Dwarf_Half i, TagList<I, N...>)
{
  if (i != I) {
    return leaveExternalDie(ctxt, i, TagList<N...>());
  }

  TagLeaver<I>::leaveExternal(ctxt);
}

template <Dwarf_Half... N>
void leaveExternalDie(DwarfContext &ctxt, Dwarf_Half tag)
{
  return leaveExternalDie(ctxt, tag, TagList<N...>());
}

leaveDwarfDie_t *leaveExternalDwarfDie = leaveExternalDie<DW_TAG_class_type, DW_TAG_structure_type>;

}  // namespace dwarf
}  // namespace pcv

#include "TagClass.h"
#include "TagCompileUnit.h"
#include "TagFormalParameter.h"
#include "TagInheritance.h"
#include "TagInlinedSubRoutine.h"
#include "TagMember.h"
#include "TagNamespace.h"
#include "TagSubprogram.h"
#include "TagTypedef.h"
#include "TagUnionType.h"
#include "TagVariable.h"

#endif  // DWARFLOADER_TAGHANDLER_H
