//
// Created by wilhelma on 12/22/16.
//

#ifndef DWARFLOADER_DWARFREADER_H
#define DWARFLOADER_DWARFREADER_H

#include <dwarf.h>
#include <libdwarf.h>
#include <cstdlib>
#include <iostream>
#include <memory>
#include <string>
#include <type_traits>

#include "Duplicate.h"
#include "DwarfContext.h"
#include "SymInfo.h"
#include "guards/DbgGuard.h"
#include "guards/FileGuard.h"

#ifdef NODEJS
#include <json11.hpp>
#include "FileFilter.h"
#else
#include "Filter.h"
#endif

namespace pcv {
namespace dwarf {

using pcv::entity::Class;
using pcv::entity::Routine;

struct ImageInfo {
  Id_t imageId;
  std::string filePath;
  ImageInfo(Id_t imageId, std::string filePath) : imageId(imageId), filePath(filePath) {}
};

/*!
 * @brief DwarfReader is responsible for reading dwarf information and providing an API for
 * providing symbol information.
 */
class DwarfReader : public SymInfo
{
 public:
#ifdef NODEJS
  explicit DwarfReader(const std::string &fileName, DieDuplicate &dieDuplicate, FileFilter &filter);
#else
  explicit DwarfReader(Filter *f);
#endif
  ~DwarfReader() = default;

  /* disable copy/move construction and assignment operators */
  DwarfReader(const DwarfReader &) = delete;
  DwarfReader(DwarfReader &&)      = delete;
  DwarfReader &operator=(const DwarfReader &) = delete;
  DwarfReader &operator=(DwarfReader &&) = delete;

  std::unique_ptr<std::vector<const entity::Variable *>> getGlobalVariables(
    const Id_t imageId) const override;
  std::unique_ptr<std::vector<const entity::Variable *>> getStaticVariables(
    const Id_t imageId) const override;
  const Variable *getMemberVariable(const entity::ImageScopedId &clsId,
                              const Class::offset_t offset) const override;
  std::vector<const entity::File *> getFiles(Id_t imageId) override;
  bool readSymInfo(const std::string &fileName, Id_t imageId, SymInfo::offset_t offset) override;
  bool isBinaryOptimized(const std::string &fileName) override;
  void clearSymInfo() override;
  entity::ImageScopedId getUniqueClassId(const Id_t imageId,
                                         const std::string &rtnName) const override;
  const entity::Routine *getRoutine(const Id_t imageId, const std::string rtnName) const override;
  std::string refineRtnName(const Id_t imageId, const std::string &rtnName) override;
  bool getSourceLocation(const S_ADDRUINT &ip, int *line, std::string *file) const override;

  /*!
   * @brief The global error handler (required ot deallocate error messages)
   */
  static void errHandler(Dwarf_Error err, /**< [IN] The error handle */
                         Dwarf_Ptr errArg /**< [IN] The error argument */);

  const Context &getContext() const override { return ctxt_; }
#ifdef NODEJS
  void start(const json11::Json &rules);
  void assignRegex(const std::string &include, const std::string &exclude);
  void clearContext();
#endif

 private:
  static constexpr Dwarf_Bool IS_INFO{true};

  // processing
  void iterateCUs(std::function<void(Dwarf_Die)> handleCU) noexcept;
  void iterateToIdentifyExternals(Dwarf_Die cu_die) noexcept;
  void iterate(Dwarf_Die die) noexcept;
  bool handleExternal(Dwarf_Die die);
  bool handle(Dwarf_Die die);
  void leaveExternal(Dwarf_Die die);
  void leave(Dwarf_Die die);
  bool isExcluded(Dwarf_Die die);
  bool hasOptimizedCUs();

#ifdef NODEJS
  // post-processing
  void processContext();
#endif

  void setCurrentFile(const std::string &filenName);
  bool readSymInfo(Id_t imageId, SymInfo::offset_t offset = 0);

  // members
  DieDuplicate dieDuplicate_;
  std::unique_ptr<FileGuard> fileGuard_;
  std::unique_ptr<DbgGuard> dbgGuard_;
  DwarfContext ctxt_;
#ifdef NODEJS
  FileFilter &filter_;
#else
  Filter *filter_;
#endif
};

}  // namespace dwarf
}  // namespace pcv

#endif  // DWARFLOADER_DWARFREADER_H
