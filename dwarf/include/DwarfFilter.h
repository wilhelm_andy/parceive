//
// Created by wilhelma on 1/23/17.
//

#pragma once

#include <regex>
#include <unordered_set>

namespace pcv {
namespace dwarf {

class DwarfFilter
{
 public:
  explicit DwarfFilter(const std::string &include, const std::string &exclude);

  bool isIncluded(const std::string &fileName) const;
  bool isIncluded(const std::string &fileName, uint lineNo) const;
  bool isExcluded(const std::string &fileName) const;

  void assign(const std::string &include, const std::string &exclude);

  /* disable copy/move construction and assignment operators */
  DwarfFilter(const DwarfFilter &) = delete;
  DwarfFilter(DwarfFilter &&)      = delete;
  DwarfFilter &operator=(const DwarfFilter &) = delete;
  DwarfFilter &operator=(DwarfFilter &&) = delete;

 private:
  std::regex include_;
  std::regex exclude_;
};

}  // namespace dwarf
}  // namespace pcv
